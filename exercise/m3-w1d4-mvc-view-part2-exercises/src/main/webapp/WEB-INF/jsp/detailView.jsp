<!DOCTYPE html>
<html>
  <head>
    <title><c:out value="${pageTitle}" /></title>
    <link rel="stylesheet" href="css/site.css" />
  </head>
  
  <body>
    <nav>
        <div class="topBar">
            <h1>MVC Exercises - Views Part 2: Model</h1>
        </div>
        <div class="bottomList">
            <ul>
                <c:url var="listUrl" value="/productList" />
					<li><a href="${listUrl}">Product List</a></li>
			
				<c:url var="tableUrl" value="/productTable"/>	
					<li><a href="${tableUrl}">Product Table</a></li>
			
				<c:url var="tilesUrl" value="/productTiles" />		
					<li><a href="${tilesUrl}">Product Tiles</a></li>
            </ul>
        </div>
	</nav>
	
	<div class="main-content">
		<div class="headline">
			<h2>Toy Department</h2>
		</div>
	
	
	<div class="items">
		
		<div class="toy">
				<c:url var="imageUrl" value="/img/${product.imageName}"/>
				<img src="${imageUrl}" />
		
		</div>
		
		<c:forEach var="productVar" items="${productList}">
	
				<c:url var="productUrl" value="/detailView">
					<c:param name="productId" value="${productVar.productId}" />
				</c:url>
				<c:url var="imageUrl" value="/img/${productVar.imageName}" />
				<a class="toyImage" href="${productUrl}"><img src="${imageUrl}" />
				</a>
				
			<div class="description">
					<div class="product">
						<c:out value="${productVar.name}" />
						<c:choose>
							<c:when test="${productVar.topSeller}">
							<c:out value="BEST SELLER!" />
							</c:when>
						</c:choose>
					</div>
					
					<div class="manufacturer">
						<c:out value="by ${productVar.manufacturer}" />
					</div>
					
					<div class="price">
						<c:out value="$ ${productVar.price}" />
					</div>
					
					<div class="weight">
						<c:out value=" ${productVar.weightInLbs} lbs." />
					</div>
					
					<div class="rating">
					<c:choose>
						<c:when test="${productVar.remainingStock < 3}">
						<c:out value="Only ${productVar.remainingStock} left" />
						</c:when>
					</c:choose>
					
					<c:choose>
							<c:when test="${productVar.averageRating >4 }">
								<c:url var="imageUrl" value="/img/5-star.png" />
								<img src="${imageUrl}"/>
							</c:when>
							<c:when test="${productVar.averageRating >3 }">
								<c:url var="imageUrl" value="/img/4-star.png" />
								<img src="${imageUrl}"/>
							</c:when>
							<c:when test="${v.averageRating >2 }">
								<c:url var="imageUrl" value="/img/3-star.png" />
								<img src="${imageUrl}"/>
							</c:when>
							<c:when test="${productVar.averageRating >1 }">
								<c:url var="imageUrl" value="/img/2-star.png" />
								<img src="${imageUrl}"/>
							</c:when>
							<c:otherwise>
								<c:url var="imageUrl" value="/img/1-star.png" />
								<img src="${imageUrl}"/>
							</c:otherwise>
						</c:choose>
					</div>
			</div>
				
			
			</c:forEach>
		</div>
	</div>

	

  </body>

  </html>
		