-- The following queries utilize the "dvdstore" database.

-- 1. All of the films that Nick Stallone has appeared in
-- (30 rows)
<<<<<<< HEAD
SELECT
    f.title, (a.last_name || ', ' || a.first_name) AS Actors_Name
FROM
    film f
JOIN
    film_actor fa ON fa.film_id = f.film_id
JOIN
    actor a ON a.actor_id = fa.actor_id
WHERE a.first_name = 'NICK' AND a.last_name = 'STALLONE';


-- 2. All of the films that Rita Reynolds has appeared in
-- (20 rows)
SELECT 
    f.title, a.first_name, a.last_name
FROM
    actor a
JOIN film_actor fa ON fa.actor_id = a.actor_id
JOIN film f ON f.film_id = fa.film_id
WHERE a.first_name = 'RITA';


-- 3. All of the films that Judy Dean or River Dean have appeared in
-- (46 rows)
SELECT
    f.title, (a.last_name || ', ' || a.first_name) AS Actor_Name
FROM
    film f
JOIN film_actor fa ON fa.film_id = f.film_id
JOIN actor a ON a.actor_id = fa.actor_id
WHERE a.last_name = 'DEAN' AND (a.first_name = 'JUDY' OR a.first_name = 'RIVER');


-- 4. All of the the ‘Documentary’ films
-- (68 rows)
SELECT
    c.name
FROM
    film f
JOIN 
    film_category fc ON fc.film_id = f.film_id
JOIN 
    category c ON c.category_id = fc.category_id
WHERE 
    c.name = 'Documentary';

-- 5. All of the ‘Comedy’ films
-- (58 rows)
SELECT
    c.name
FROM
    film f
JOIN 
    film_category fc ON fc.film_id = f.film_id
JOIN 
    category c ON c.category_id = fc.category_id
WHERE 
    c.name = 'Comedy';


-- 6. All of the ‘Children’ films that are rated ‘G’
-- (10 rows)
SELECT
    name, rating 
FROM
    film f
JOIN film_category fc ON fc.film_id = f.film_id
JOIN category c ON c.category_id = fc.category_id
WHERE name = 'Children' AND rating = 'G';


-- 7. All of the ‘Family’ films that are rated ‘G’ and are less than 2 hours in length
-- (3 rows)
SELECT
    name, rating, length
FROM
    film f
JOIN film_category fc ON fc.film_id = f.film_id
JOIN category c ON c.category_id = fc.category_id
WHERE name = 'Family' AND rating = 'G' AND length < 120;


-- 8. All of the films featuring actor Matthew Leigh that are rated ‘G’
-- (9 rows)
SELECT
    title, a.last_name, rating
FROM
    film f
JOIN film_category fc ON fc.film_id = f.film_id
JOIN category c ON c.category_id = fc.category_id
JOIN film_actor fa ON fa.film_id = f.film_id
JOIN actor a ON a.actor_id = fa.actor_id
WHERE last_name = 'LEIGH' AND rating = 'G';


-- 9. All of the ‘Sci-Fi’ films released in 2006
-- (61 rows)
SELECT
    name, title, release_year
FROM
    film f
JOIN film_category fc ON fc.film_id = f.film_id
JOIN category c ON c.category_id = fc.category_id
WHERE 
    name = 'Sci-Fi' AND release_year = '2006';



-- 10. All of the ‘Action’ films starring Nick Stallone
-- (2 rows)
SELECT
    title, name, last_name
FROM
    film f
JOIN film_category fc ON fc.film_id = f.film_id
JOIN category c ON c.category_id = fc.category_id
JOIN film_actor fa ON fa.film_id = f.film_id
JOIN actor a ON a.actor_id = fa.actor_id
WHERE 
    name = 'Action' AND last_name = 'STALLONE';


-- 11. The address of all stores, including street address, city, district, and country
-- (2 rows)
SELECT
    a.address, ci.city, a.district, country 
FROM store s
JOIN address a  ON a.address_id = s.store_id
JOIN city ci ON ci.city_id = a.city_id
JOIN country co ON co.country_id = ci.country_id;



-- 12. A list of all stores by ID, the store’s street address, and the name of the store’s manager
-- (2 rows)
SELECT s.store_id, a.address, sta.last_name, sta. first_name
FROM store s
JOIN address a ON a.address_id = s.store_id
Join staff sta ON sta.staff_id = s.manager_staff_id;


-- 13. The first and last name of the top ten customers ranked by number of rentals
-- (#1 should be “ELEANOR HUNT” with 46 rentals, #10 should have 39 rentals)
SELECT first_name, last_name, COUNT(p.customer_id) AS Rental_Amount
FROM customer c
JOIN payment p ON p.customer_id = c.customer_id
GROUP BY last_name, first_name
ORDER BY Rental_Amount DESC LIMIT 10;




-- 14. The first and last name of the top ten customers ranked by dollars spent
-- (#1 should be “KARL SEAL” with 221.55 spent, #10 should be “ANA BRADLEY” with 174.66 spent)
SELECT first_name, last_name, SUM(p.amount) AS Payment_Total
FROM customer c
JOIN payment p ON p.customer_id = c.customer_id
GROUP BY last_name, first_name
ORDER BY Payment_Total DESC LIMIT 10;

-- 15. The store ID, street address, total number of rentals, total amount of sales (i.e. payments), and average sale of each store
-- (Store 1 has 7928 total rentals and Store 2 has 8121 total rentals)
SELECT
    s.store_id, a.address, COUNT(p.amount) AS total_rentals, SUM(amount), ROUND(AVG(amount), 2) 
FROM store s
JOIN inventory i ON i.store_id = s.store_id
JOIN rental r ON r.inventory_id = i.inventory_id
JOIN payment p ON p.rental_id = r.rental_id
JOIN address a ON a.address_id = s.store_id
GROUP BY s.store_id, a.address;


-- 16. The top ten film titles by number of rentals
-- (#1 should be “BUCKET BROTHERHOOD” with 34 rentals and #10 should have 31 rentals)
SELECT
    title, COUNT(title) AS Number_of_rentals
FROM
    film f
JOIN inventory i ON i.film_id = f.film_id
JOIN rental r ON r.inventory_id = i.inventory_id
GROUP BY title
ORDER BY Number_of_rentals DESC
LIMIT 10;


-- 17. The top five film categories by number of rentals
-- (#1 should be “Sports” with 1179 rentals and #5 should be “Family” with 1096 rentals)
SELECT
    c.name, COUNT(c.name) AS Number_of_rentals
FROM
    film f
JOIN film_category fc ON fc.film_id = f.film_id
Join category c ON c.category_id = fc.category_id
JOIN inventory i ON i.film_id = f.film_id
JOIN rental r ON r.inventory_id = i.inventory_id
GROUP BY c.name
ORDER BY Number_of_rentals DESC
LIMIT 5;

-- 18. The top five Action film titles by number of rentals
-- (#1 should have 30 rentals and #5 should have 28 rentals)
SELECT
    f.title, COUNT(f.title) AS Number_of_rentals
FROM
    film f
JOIN film_category fc ON fc.film_id = f.film_id
Join category c ON c.category_id = fc.category_id
JOIN inventory i ON i.film_id = f.film_id
JOIN rental r ON r.inventory_id = i.inventory_id
WHERE 
    c.name = 'Action'
GROUP BY 
    f.title
ORDER BY 
    Number_of_rentals DESC
LIMIT 5;

-- 19. The top 10 actors ranked by number of rentals of films starring that actor
-- (#1 should be “GINA DEGENERES” with 753 rentals and #10 should be “SEAN GUINESS” with 599 rentals)
SELECT
    a.first_name, a.last_name, COUNT(a.last_name) AS Number_of_rentals
FROM
    actor a
JOIN film_actor fa ON fa.actor_id = a.actor_id
JOIN film f ON f.film_id = fa.film_id
JOIN inventory i ON i.film_id = f.film_id
JOIN rental r ON r.inventory_id = i.inventory_id
GROUP BY 
    a.actor_id
ORDER BY 
    Number_of_rentals DESC
LIMIT 10;

-- 20. The top 5 “Comedy” actors ranked by number of rentals of films in the “Comedy” category starring that actor
-- (#1 should have 87 rentals and #5 should have 72 rentals)
SELECT
    a.first_name, a.last_name, COUNT(a.last_name) AS Number_of_rentals, 
FROM
    actor a
JOIN film_actor fa ON fa.actor_id = a.actor_id
JOIN film f ON f.film_id = fa.film_id
JOIN film_category fc ON fc.film_id = f.film_id
JOIN category c ON c.category_id = fc.category_id
JOIN inventory i ON i.film_id = f.film_id
JOIN rental r ON r.inventory_id = i.inventory_id
WHERE c.name = 'Comedy'
GROUP BY 
    a.actor_id
ORDER BY 
    Number_of_rentals DESC
LIMIT 5;
=======
