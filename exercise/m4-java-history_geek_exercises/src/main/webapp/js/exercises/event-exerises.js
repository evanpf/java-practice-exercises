﻿/// <reference path="../jquery-3.1.1.js" />

$(document).ready(function () {

    $("#SameShipping").click(function() {
        if(this.checked == true) {
            $("#ShippingAddress1").val($("#BillingAddress1").val());
            $("#ShippingAddress2").val($("#BillingAddress2").val());
            $("#ShippingCity").val($("#BillingCity").val());
            $("#ShippingState").val($("#BillingState").val());
            $("#ShippingPostalCode").val($("#BillingPostalCode").val());
        }
    });

    $("input[name='ShippingType']").on('click', function(){

        var shipping = $(this).attr('data-cost');
        $('#shipping > .price').text('$' + shipping);

        var subTotal = $('#subtotal > .price').text().substring(1);
        $('#grandtotal > .price').text('$' + (parseFloat(shipping) + parseFloat(subTotal)).toFixed(2));

    });
});

$(document).ready(function () {

    $("#btnRestart").click(function () {
        alert("The paragraph was clicked.");
    });


    $(document).keyup(function(e){


        switch (e.which){

            case 37:    //left arrow key

                $('.ship').prev().addClass('ship');
                $('.ship').next().removeClass('ship');



                break;

            case 38:    //up arrow key
                                                                            //No let? JS version not supported?
                var i = 3;

                $('#row_' + i + ':first-child').addClass('ship');



                break;

            case 39:    //right arrow key


                $('.ship').next().addClass('ship');
                $('.ship').prev().removeClass('ship');


                break;

            case 40:    //bottom arrow key
                




                $('#row_' + d + '_column_0').addClass('ship');


                break;
        }
    });

});

