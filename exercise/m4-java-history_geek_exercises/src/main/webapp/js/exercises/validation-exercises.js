﻿/// <reference path="../jquery-3.1.1.js" />
/// <reference path="../jquery.validate.js" />

$(document).ready(function () {

    $("#form0").validate({

        debug: true,

        rules: {

            email: {
                required: true,
            },

            password: {
                required: true,
                minLength: 8,
            },

            confirmPassword: {
                required: true,

                equalTo: "#password"
            }
        },

        messages: {

            email: {
                required: "You must enter an email."
            },

            password: {
                required: "You must enter a password.",
                minLength: "Password must be at least 8 characters."
            },

            confirmPassword: {
                required: "You must enter a matching password.",
                equalTo: "Passwords must match."
            },

        },

        errorClass: "error",
        validClass: "valid"

    })

});