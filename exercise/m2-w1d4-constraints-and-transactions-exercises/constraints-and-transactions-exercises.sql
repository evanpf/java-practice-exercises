-- Write queries to return the following:
-- The following changes are applied to the "pagila" database.**

-- 1. Add actors, Hampton Avenue, and Lisa Byway to the actor table.
SELECT * FROM actor;
INSERT INTO actor (first_name, last_name) values ('Hampton', 'Avenue');
INSERT INTO actor (first_name, last_name) values ('Lisa', 'Byway');

SELECT * FROM actor WHERE first_name = 'Lisa' OR first_name = 'Hampton';

-- 2. Add "Euclidean PI", "The epic story of Euclid as a pizza delivery boy in 
-- ancient Greece", to the film table. The movie was released in 2008 in English. 
-- Since its an epic, the run length is 3hrs and 18mins. There are no special 
-- features, the film speaks for itself, and doesn't need any gimmicks.	
INSERT INTO film (title, description, release_year, language_id, length) values ('Euclidean PI', 'The epic story of Euclid as a pizza delivery boy in ancient Greece', 2008, (SELECT language_id FROM language WHERE name = 'English'), 198);

SELECT * 
FROM film
JOIN language ON language.language_id = film.language_id
WHERE title = 'Euclidean PI';


-- 3. Hampton Avenue plays Euclid, while Lisa Byway plays his slightly 
-- overprotective mother, in the film, "Euclidean PI". Add them to the film.
INSERT INTO film_actor (actor_id, film_id) VALUES (201, 1001);
INSERT INTO film_actor (actor_id, film_id) VALUES (202, 1001);

SELECT *
FROM film
JOIN film_actor ON film_actor.film_id = film.film_id
JOIN actor ON actor.actor_id = film_actor.actor_id
WHERE title = 'Euclidean PI';



-- 4. Add Mathmagical to the category table.
INSERT INTO category(name) VALUES ('Mathmagical');

SELECT *
FROM category;

-- 5. Assign the Mathmagical category to the following films, "Euclidean PI",  //mathmagical = 17
-- "EGG IGBY", "KARATE MOON", "RANDOM GO", and "YOUNG LANGUAGE"                
SELECT *
FROM film
WHERE title = 'EGG IGBY' OR title = 'Euclidean PI' OR title = 'KARATE MOON' OR title = 'RANDOM GO' OR title = 'YOUNG LANGUAGE';
-- film_id     --274                   1001                    494                     714                         996             

UPDATE film_category
SET category_id = 17
WHERE film_id = 1001 OR film_id = 274 OR film_id = 494 OR film_id = 714 OR film_id = 996;

INSERT INTO film_category (film_id, category_id) VALUES (1001, 17);

-- 6. Mathmagical films always have a "G" rating, adjust all Mathmagical films 
-- accordingly.
-- (5 rows affected)
SELECT *
FROM film_category
JOIN category ON category.category_id = film_category.category_id
JOIN film ON film.film_id = film_category.film_id
WHERE category.name = 'Mathmagical';

UPDATE film
SET rating = 'G'
WHERE film_id = 1001 OR film_id = 274 OR film_id = 494 OR film_id = 714 OR film_id = 996; 

-- 7. Add a copy of "Euclidean PI" to all the stores.
SELECT *
FROM inventory
WHERE film_id > 1000;

INSERT INTO inventory(store_id, film_id) VALUES (1, 1001);
INSERT INTO inventory(store_id, film_id) VALUES (2, 1001);

-- 8. The Feds have stepped in and have impounded all copies of the pirated film, 
-- "Euclidean PI". The film has been seized from all stores, and needs to be 
-- deleted from the film table. Delete "Euclidean PI" from the film table. 
-- (Did it succeed? Why?)

DELETE FROM film WHERE film_id = 1001;
DELETE FROM film WHERE title = 'Euclidean PI';

-- No, it is associated with other tables still.

-- 9. Delete Mathmagical from the category table. 
-- (Did it succeed? Why?)
DELETE FROM category WHERE name = 'Mathmagical';

-- No, foreign key constraint.

-- 10. Delete all links to Mathmagical in the film_category table. 
-- (Did it succeed? Why?)

DELETE FROM film_category WHERE category_id = 17;
-- yes, it took out the primary linked key.

-- 11. Retry deleting Mathmagical from the category table, followed by retrying
-- to delete "Euclidean PI". 
-- (Did either deletes succeed? Why?)

DELETE FROM category WHERE category_id = 17;
DELETE FROM film WHERE film_id = 1001;

-- 12. Check database metadata to determine all constraints of the film id, and 
-- describe any remaining adjustments needed before the film "Euclidean PI" can 
-- be removed from the film table.

--Must delete relational keys from film_category and film_actor then film can be deleted at film_id.