package com.techelevator;

import java.util.Map;

import com.techelevator.dao.ActorDao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller 
public class ActorSearchController {

	@Autowired
	private ActorDao actorDao;

	@RequestMapping(path = "/actorListResult", method = RequestMethod.GET)
	public String showSearchActorForm(@RequestParam(defaultValue = "") String lastName, ModelMap modelHolder) {
		if (!lastName.equals("")) {
			modelHolder.put("actors", actorDao.getMatchingActors(lastName));
		}
		return "actorList";
	}

	@RequestMapping(path = "/actorList", method = RequestMethod.GET)
	public String searchActors() {
		return "actorList";
	}
}
