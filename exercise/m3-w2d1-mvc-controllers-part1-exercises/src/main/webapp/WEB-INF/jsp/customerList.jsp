<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:set var="pageTitle" value="Customers List"/>

<%@include file="common/header.jspf"%>

	<c:url var="formAction" value="/customerListResult" />
		<form method="GET" action="${formAction}">
			
			<div class="formInput">
				<label for="search">Name: </label> 
				<input type="text" name="search" id="search" />
			</div>
			
			<div class="formInput">
				<label for="sort">Sort By: </label> 
				<select name="sort" id="sort">
					<option value="first_Name">First Name</option>
					<option value="last_Name">Last Name</option>
					<option value="email">Email</option>
				</select>
			</div>
			
			<input class="formSubmitButton" type="submit"
				value="Search Customers" />
		</form>
			
		<div class="grid">
		
			<div class="titleA">
				<h3>Name</h3>
			</div>
			
			<div class="titleC">
				<h3>Email</h3>
			</div>
			
			<div class="titleD">
				<h3>Active</h3>
			</div>
		
			
			<c:forEach var="customer" items="${customerList}">
				
				<div id="border">
				</div>
				
				<div class="firstLast">
					<c:out value="${customer.firstName}" /> 
					<c:out value="${customer.lastName}" />
				</div>
				
				<div class="email">
					<c:out value=" ${customer.email}" />
				</div>
				
				<div class="active">
					<c:out value="${customer.active ? 'Yes' : 'No'}" />
				</div>
			
			</c:forEach>
	</div>
		

<%@include file="common/footer.jspf"%>