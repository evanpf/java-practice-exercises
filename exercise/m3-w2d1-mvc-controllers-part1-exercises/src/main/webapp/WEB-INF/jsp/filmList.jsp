<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:set var="pageTitle" value="Film List"/>

<%@include file="common/header.jspf"%>


	<c:url var="formAction" value="/filmListResult" />
		<form method="GET" action="${formAction}">
		
		
			<div class="formInput">
				<label for="minLength">Minimum Length: </label> 
				<input type="text" name="minLength" id="minLength" placeholder="30" />
			</div>
			
			<div class="formInput">
				<label for="maxLength">Maximum Length: </label> 
				<input type="text" name="maxLength" id="maxLength" placeholder="120" />
			</div>
			
			<div class="formInput">
				<label for="genre">Genre: </label> 
				<select name="genre" id="genre">
					<option value="Action">Action</option>
					<option value="Animation">Animation</option>
					<option value="Children">Children</option>
					<option value="Classics">Classics</option>
					<option value="Comedy">Comedy</option>
					<option value="Documentary">Documentary</option>
					<option value="Drama">Drama</option>
					<option value="Family">Family</option>
					<option value="Foreign">Foreign</option>
					<option value="Games">Games</option>
					<option value="Horror">Horror</option>
					<option value="Music">Music</option>
					<option value="New">New</option>
					<option value="Sci-Fi">Sci-Fi</option>
					<option value="Sports">Sports</option>
					<option value="Travel">Travel</option>
				</select>
			</div>

			<input class="formSearchButton" type="submit" value="Search" />
		</form>
	
	
	<div class="grid">
	
		<div class="titleA">
			<h3>Title</h3>
		</div>
		
		<div class="titleC">
			<h3>Description</h3>
		</div>
		
		<div class="titleD">
			<h3>Release Year</h3>
		</div>
		
		<div class="titleE">
			<h3>Length</h3>
		</div>
		
		<div class="titleF">
			<h3>Rating</h3>
		</div>
	
		<c:forEach items="${filmList}" var="film">
			<div id="border">
			</div>
			
			<div class="title">	
					
				<c:out value="${film.title}" />
			</div>
			
			<div class="description">
				<c:out value="${film.description}" />
			</div>
			
			<div class="year">
				<c:out value="${film.releaseYear}" />
			</div>
			
			<div class="length">
				<c:out value="${film.length}" />
			</div>
			
			<div class="rating">
				<c:out value="${film.rating}" />
			</div>
				 
		</c:forEach>
	

	</div>


<%@include file="common/footer.jspf"%>