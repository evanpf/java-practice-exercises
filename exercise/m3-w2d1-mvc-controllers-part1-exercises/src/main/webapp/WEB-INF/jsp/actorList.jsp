<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:set var="pageTitle" value="Actor List" />

<%@include file="common/header.jspf"%>

	<h2>Actor Search</h2>
		

<c:url var="formAction" value="/actorListResult" />
	<form method="GET" action="${formAction}">
		<div class="formInputGroup">
			<input type="text" name="lastName" id="name"
			placeholder="Search by last name" />
		</div>
	
		<input class="formSubmitButton" type="submit" value="Submit" />
	</form>
	
	
	<div class="grid">
	
		<div class="titleA">
			<h4>Name</h4>
		</div>
	
		<c:forEach items="${actors}" var="actor">
			<div id="border">
			</div>
		<div class="firstLast">		
			<c:out value="${actor.firstName}" />
			<c:out value="${actor.lastName}" />
		</div>
			 
		</c:forEach>


	</div>
		
	
		
		

<%@include file="common/footer.jspf"%>