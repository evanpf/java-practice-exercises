
CREATE TABLE job_title (
        id SERIAL,
        title varchar(100) NOT NULL,

        CONSTRAINT pk_job_title_id PRIMARY KEY(id)       
);

CREATE TABLE department (
        id SERIAL,
        name varchar(100) NOT NULL,

        CONSTRAINT pk_department_id PRIMARY KEY(id)
 );

CREATE TABLE employee (
        id SERIAL,
        last_name varchar(40) NOT NULL,
        first_name varchar(30) NOT NULL,
        job_title_id integer NOT NULL,
        gender char(1),
        birthday date,
        hire_date date NOT NULL,
        department_id integer NOT NULL,
    
        CONSTRAINT pk_employee_id PRIMARY KEY(id),
        CONSTRAINT fk_job_title_id FOREIGN KEY(job_title_id) REFERENCES job_title(id),
        CONSTRAINT fk_department_id FOREIGN KEY(department_id) REFERENCES department(id)
);



CREATE TABLE project (
        id SERIAL,
        name varchar(100) NOT NULL,
        employee_id integer NOT NULL,
        start_date date NOT NULL,

        CONSTRAINT pk_project_id PRIMARY KEY(id),
        CONSTRAINT fk_employee_id FOREIGN KEY(employee_id) REFERENCES employee(id)
);



-- four projects, three departments, and eight employees.

INSERT INTO department(name) VALUES ('Human Resources');
INSERT INTO department(name) VALUES ('Sales');
INSERT INTO department(name) VALUES ('Accounting');
INSERT INTO department(name) VALUES ('Customer Service');
INSERT INTO department(name) VALUES ('Engineering');

INSERT INTO job_title(title) VALUES ('Software Developer');
INSERT INTO job_title(title) VALUES ('Help Desk I');
INSERT INTO job_title(title) VALUES ('Analyst');
INSERT INTO job_title(title) VALUES ('B2B Salesman');
INSERT INTO job_title(title) VALUES ('Internal Affairs');

INSERT INTO employee(last_name, first_name, job_title_id, hire_date, department_id) 
VALUES ('Hoffman', 'Jake', 1, '2015-01-15', 5);
INSERT INTO employee(last_name, first_name, job_title_id, gender, birthday, hire_date, department_id) 
VALUES ('Ozakatane', 'Misty', 1, 'f', '1968-05-21', '2015-01-15', 5);
INSERT INTO employee(last_name, first_name, job_title_id, gender, birthday, hire_date, department_id) 
VALUES ('Harper', 'David', 2, 'm', '1968-05-21', '2015-01-15', 4);
INSERT INTO employee(last_name, first_name, job_title_id, gender, birthday, hire_date, department_id) 
VALUES ('Conway', 'Mike', 3, 'm', '1968-05-21', '2015-01-15', 3);
INSERT INTO employee(last_name, first_name, job_title_id, gender, birthday, hire_date, department_id) 
VALUES ('Mikan', 'Jessica', 4, 'f', '1968-05-21', '2015-01-15', 2);
INSERT INTO employee(last_name, first_name, job_title_id, gender, birthday, hire_date, department_id) 
VALUES ('Summersault', 'Greg', 5, 'm', '1968-05-21', '2015-01-15', 1);


SELECT *, job_title.title
FROM employee
JOIN job_title ON job_title.id = employee.job_title_id
JOIN department ON department.id = employee.department_id;