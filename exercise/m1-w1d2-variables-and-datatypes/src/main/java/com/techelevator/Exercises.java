package com.techelevator;

public class Exercises {

	public static void main(String[] args) {
        
        /*
        INSTRUCTIONS

        The primary purposes of these exercises are to practice: 
        		
        		1. declaring variables and using assignment statements
        		2. using variables, literals, and operators within arithmetic expressions
        		3. choosing meaningful variable names
        		4. choosing appropriate data types

        One of the hardest things about programming is choosing good names. Spend time
        to find good, meaningful, and descriptive names for your variables. Clarity and
        expressiveness is more important than brevity. Err on the side of longer, more 
        descriptive names over short, cryptic ones.

        Be consistent with your choice of datatypes, especially when it comes to 
        some values like money. There are many different ways to express money. Pick one, 
        and stick with it throughout these exercises.
        
        Keep your code consistent and well formatted. When code is poorly indented, 
        or lost in a flood of blank lines, it can make it difficult to read. 
        */

        /* 
        1. 4 birds are sitting on a branch. 1 flies away. How many birds are left on
        the branch? 
        */
		
		// ### EXAMPLE:  
		int initialNumberOfBirds = 4;
		int birdsThatFlewAway = 1;
		int remainingNumberOfBirds = initialNumberOfBirds - birdsThatFlewAway;

        /* 
        2. There are 6 birds and 3 nests. How many more birds are there than
        nests? 
        */
		
		// ### EXAMPLE: 
		int numberOfBirds = 6;
		int numberOfNests = 3;
		int numberOfExtraBirds = numberOfBirds - numberOfNests;

        /* 
        3. 3 raccoons are playing in the woods. 2 go home to eat dinner. How
        many raccoons are left in the woods? 
        */
		
		int raccoonsInWoods = 3;
		int raccoonsAtDinner = 2;
		int raccoonsLeftInWoods = raccoonsInWoods - raccoonsAtDinner;
		System.out.println(raccoonsLeftInWoods);
		
        /* 
        4. There are 5 flowers and 3 bees. How many less bees than flowers? 
        */
		int flowers = 5;
		int bees = 3;
		int beeDeficiency = flowers - bees;
		System.out.println(beeDeficiency);

        /* 
        5. 1 lonely pigeon was eating breadcrumbs. Another pigeon came to eat
        breadcrumbs, too. How many pigeons are eating breadcrumbs now? 
        */
		int pigeon = 1;
		pigeon += pigeon;
		System.out.println(pigeon);
		
        /* 
        6. 3 owls were sitting on the fence. 2 more owls joined them. How many
        owls are on the fence now? 
        */
		int owls = 3;
		owls += 2;
		System.out.println(owls);
		

        /* 
        7. 2 beavers were working on their home. 1 went for a swim. How many
        beavers are still working on their home? 
        */
		int beaversWorking = 2;
		int beaversSwimming = 1;
		System.out.println(beaversWorking - beaversSwimming);
		

        /* 
        8. 2 toucans are sitting on a tree limb. 1 more toucan joins them. How
        many toucans in all? 
        */
		int toucans = 2;
		toucans ++;
		System.out.println(toucans);
		
		
        /* 
        9. There are 4 squirrels in a tree with 2 nuts. How many more squirrels
        are there than nuts? 
        */
		int squirrels = 4;
		int nuts = 2;
		int squirrelsComparedToNuts = squirrels - nuts;
		System.out.println(squirrelsComparedToNuts);
		

        /* 
        10. Mrs. Hilt found a quarter, 1 dime, and 2 nickels. How much money did
        she find? 
        */
		float quarter = 0.25f;
		float dime = 0.10f;
		float nickel = 0.05f;
		int quarterTotal = 1;
		int dimeTotal = 1;
		int nickelTotal = 2;
		float moneyTotal = (quarter * quarterTotal) + (dime * dimeTotal) + (nickel * nickelTotal);
		System.out.println(moneyTotal);
		

        /* 
        11. Mrs. Hilt's favorite first grade classes are baking muffins. Mrs. Brier's
        class bakes 18 muffins, Mrs. MacAdams's class bakes 20 muffins, and
        Mrs. Flannery's class bakes 17 muffins. How many muffins does first
        grade bake in all? 
        */
		int briersMuffins = 18;
		int macadamsMuffins = 20;
		int flannerysMuffins = 17;
		int totalMuffinsFirstGrade = briersMuffins + macadamsMuffins + flannerysMuffins;
		System.out.println(totalMuffinsFirstGrade);
		
		
        /*
        12. Mrs. Hilt bought a yoyo for 24 cents and a whistle for 14 cents. How
        much did she spend in all for the two toys?
        */
        float yoyo = .24f;
        float whistle = .14f;
        float toyTotal = yoyo + whistle;
        System.out.println(toyTotal);
		
		
        /*
        13. Mrs. Hilt made 5 Rice Krispie Treats. She used 8 large marshmallows
        and 10 mini marshmallows.How many marshmallows did she use
        altogether?
        */
        int largeMarshmallows = 8;
        int miniMarshmallows = 10;
        int totalMarshmallows = largeMarshmallows + miniMarshmallows;
        System.out.println(totalMarshmallows);
        
        
        /*
        14. At Mrs. Hilt's house, there was 29 inches of snow, and Brecknock
        Elementary School received 17 inches of snow. How much more snow
        did Mrs. Hilt's house have?
        */
        int hiltSnowAmount = 29;
        int brecknockSnowAmount = 17;
        int hiltSnowComparedToBrecknockSnow = hiltSnowAmount - brecknockSnowAmount;
        System.out.println(hiltSnowComparedToBrecknockSnow);
        
        /*
        15. Mrs. Hilt has $10. She spends $3 on a toy truck and $2 on a pencil
        case. How much money does she have left?
        */
        float hiltMoneyTotal = 10.00f;
        float toyTruck = 3.00f;
        float pencilCase = 2.00f;
        float hiltMoneyAfterPurchase = hiltMoneyTotal - toyTruck - pencilCase;
        System.out.println(hiltMoneyAfterPurchase);
        
        
        /*
        16. Josh had 16 marbles in his collection. He lost 7 marbles. How many
        marbles does he have now?
        */
        int startingMarbles = 16;
        int lostMarbles = 7;
        int currentMarbles = startingMarbles - lostMarbles;
        System.out.println(currentMarbles);
        
        
        /*
        17. Megan has 19 seashells. How many more seashells does she need to
        find to have 25 seashells in her collection?
        */
        int currentSeashells = 19;
        int collectionSeashells = 25;
        int minumumNecessarySeashells = collectionSeashells - currentSeashells;
        System.out.println(minumumNecessarySeashells);
        
        /*
        18. Brad has 17 balloons. 8 balloons are red and the rest are green. How
        many green balloons does Brad have?
        */
        int currentBalloonTotal = 17;
        int redBalloons = 8;
        int greenBalloons = currentBalloonTotal - redBalloons;
        System.out.println(greenBalloons);
        
        
        /*
        19. There are 38 books on the shelf. Marta put 10 more books on the shelf.
        How many books are on the shelf now?
        */
        int totalBooks = 38;
        int addedBooks = 10;
        int currentTotalOfBooks = totalBooks + addedBooks;
        System.out.println(currentTotalOfBooks);
        
        //OR
        totalBooks += 10;
        System.out.println(totalBooks);
        
        
        /*
        20. A bee has 6 legs. How many legs do 8 bees have?
        */
        int beeLegs = 6;
        int numberOfBees = 8;
        int totalBeeLegs = beeLegs * numberOfBees;
        System.out.println(totalBeeLegs);
        
        
        /*
        21. Mrs. Hilt bought an ice cream cone for 99 cents. How much would 2 ice
        cream cones cost?
        */
        float iceCreamCost = 0.99f;
        int totalIceCreamCones = 2;
        float totalIceCreamCost = iceCreamCost * totalIceCreamCones;
        System.out.println(totalIceCreamCost);
        
        
        /*
        22. Mrs. Hilt wants to make a border around her garden. She needs 125
        rocks to complete the border. She has 64 rocks. How many more rocks
        does she need to complete the border?
        */
        int rocksToCompleteBorder = 125;
        int currentRockAmount = 64;
        int rocksNeededForBorder = rocksToCompleteBorder - currentRockAmount;
        System.out.println(rocksNeededForBorder);
        
        
        /*
        23. Mrs. Hilt had 38 marbles. She lost 15 of them. How many marbles does
        she have left?
        */
        int beginningMarbleAmount = 38;
        int marblesLost = 15;
        int remainingMarbles = beginningMarbleAmount - marblesLost;
        System.out.println(remainingMarbles);
        
        /*
        24. Mrs. Hilt and her sister drove to a concert 78 miles away. They drove 32
        miles and then stopped for gas. How many miles did they have left to drive?
        */
        int milesToConcert = 78;
        int milesDrove = 32;
        int milesRemaining = milesToConcert - milesDrove;
        System.out.println(milesRemaining);
        
        
        /*
        25. Mrs. Hilt spent 1 hour and 30 minutes shoveling snow on Saturday
        morning and 45 minutes shoveling snow on Saturday afternoon. How
        much total time did she spend shoveling snow?
        */
        int morningSpentShovelingSnowSaturday = 1;
        int afternoonSpentShovelingSnowSaturday = 45;
        int minuteConverterForSaturdayMorning = morningSpentShovelingSnowSaturday * 60;
        int TotalTimeShovelingSnow = afternoonSpentShovelingSnowSaturday + minuteConverterForSaturdayMorning;
        System.out.println(TotalTimeShovelingSnow);
       
        
        /*    
        26. Mrs. Hilt bought 6 hot dogs. Each hot dog cost 50 cents. How much
        money did she pay for all of the hot dogs?
        */
        
        int boughtHotDogs = 6;
        float hotDogCost = 0.50f;
        float totalPaidForHotDogs = boughtHotDogs * hotDogCost;
        System.out.println(totalPaidForHotDogs);
        
        
        /*
        27. Mrs. Hilt has 50 cents. A pencil costs 7 cents. How many pencils can
        she buy with the money she has?
        */
        float hiltsCurrentFunds = 0.50f;
        float pencilCost = 0.07f;
        float pencilsCapableOfBuying = hiltsCurrentFunds / pencilCost;
        System.out.println(Math.round(pencilsCapableOfBuying));
        
        
        /*    
        28. Mrs. Hilt saw 33 butterflies. Some of the butterflies were red and others
        were orange. If 20 of the butterflies were orange, how many of them
        were red?
        */
        int TotalButterfliesSeen = 33;
        int orangeButterflies = 20;
        int redButterflies = TotalButterfliesSeen - orangeButterflies;
        System.out.println(redButterflies);
        
        
        /*    
        29. Kate gave the clerk $1.00. Her candy cost 54 cents. How much change
        should Kate get back?
        */
        int moneyHandedToClerk = 100;
        int candyCost = 54;
        int changeReceived = moneyHandedToClerk - candyCost;
        System.out.println("." + changeReceived);
        
        
        /*
        30. Mark has 13 trees in his backyard. If he plants 12 more, how many trees
        will he have?
        */
        int totalTreesInBackyard = 13;
        totalTreesInBackyard += 12;
        System.out.println(totalTreesInBackyard);
        
        /*    
        31. Joy will see her grandma in two days. How many hours until she sees
        her?
        */
        int daysToSeeGrandma = 2;
        int hoursInDay = 24;
        int hoursUntilSeeingGrandma = daysToSeeGrandma * hoursInDay;
        System.out.println(hoursUntilSeeingGrandma);
        
        
        
        /*
        32. Kim has 4 cousins. She wants to give each one 5 pieces of gum. How
        much gum will she need?
        */
        int kimsCousins = 4;
        int piecesOfGumPerCousin = 5;
        int necessaryGumAmount = kimsCousins * piecesOfGumPerCousin;
        System.out.println(necessaryGumAmount);
        
        /*
        33. Dan has $3.00. He bought a candy bar for $1.00. How much money is
        left?
        */
        float dansStartingAmount = 3.00f;
        float candyBarCost = 1.00f;
        float dansCurrentMoney = dansStartingAmount - candyBarCost;
        System.out.println(dansCurrentMoney);
        
        
        /*
        34. 5 boats are in the lake. Each boat has 3 people. How many people are
        on boats in the lake?
        */
        int boatsInTheLake = 5;
        int peoplePerBoat = 3;
        int amountOfPeopleInLake = boatsInTheLake * peoplePerBoat;
        System.out.println(amountOfPeopleInLake);
        
        /*
        35. Ellen had 380 legos, but she lost 57 of them. How many legos does she
        have now?
        */
        int startingLegoAmount = 380;
        int lostLegos = 57;
        int currentLegoAmount = startingLegoAmount - lostLegos;
        System.out.println(currentLegoAmount);
        
        
        /*
        36. Arthur baked 35 muffins. How many more muffins does Arthur have to
        bake to have 83 muffins?
        */
        int alreadyBakedMuffins = 35;
        	int desiredMuffins = 83;
        	int muffinsNeeded = desiredMuffins - alreadyBakedMuffins;
        	System.out.println(muffinsNeeded);
        
        
        /*
        37. Willy has 1400 crayons. Lucy has 290 crayons. How many more
        crayons does Willy have then Lucy?
        */
        int crayonsOfWilly = 1400;
        int crayonsOfLucy = 290;
        int amountOfCrayonsWillyHasOverLucy = crayonsOfWilly - crayonsOfLucy;
        System.out.println(amountOfCrayonsWillyHasOverLucy);
        	
        	
        /*
        38. There are 10 stickers on a page. If you have 22 pages of stickers, how
        many stickers do you have?
        */
        int stickersOnPage = 10;
        int pagesOfStickers = 22;
        int totalStickers = stickersOnPage + pagesOfStickers;
        System.out.println(totalStickers);
        
       
        
        /*
        39. There are 96 cupcakes for 8 children to share. How much will each
        person get if they share the cupcakes equally?
        */
        int totalCupcakes = 96;
        int childrenSharingCupcakes = 8;
        int cupcakesPerPerson = totalCupcakes / childrenSharingCupcakes;
        System.out.println(cupcakesPerPerson);
        
        
        
        /*
        40. She made 47 gingerbread cookies which she will distribute equally in
        tiny glass jars. If each jar is to contain six cookies each, how many
        cookies will not be placed in a jar?
        */
        int totalGingerbreadCookies = 47;
        int cookiesPerJar = 6;
        int remainingGingerbreadCookies = totalGingerbreadCookies % cookiesPerJar;
        System.out.println(remainingGingerbreadCookies);
        
        /*
        41. She also prepared 59 croissants which she plans to give to her 8
        neighbors. If each neighbor received and equal number of croissants,
        how many will be left with Marian?
        */
        int totalCroissants = 59;
        int croissantPerPerson = 8;
        int remainingCroissants = totalCroissants % croissantPerPerson;
        System.out.println(remainingCroissants);
        
        
        /*
        42. Marian also baked oatmeal cookies for her classmates. If she can
        place 12 cookies on a tray at a time, how many trays will she need to
        prepare 276 oatmeal cookies at a time?
        */
        int totalOatmealCookies = 276;
        int cookiesPerTray = 12;
        int necessaryTrays = totalOatmealCookies / cookiesPerTray;
        System.out.println(necessaryTrays);
        
        
        /*
        43. Marian’s friends were coming over that afternoon so she made 480
        bite-sized pretzels. If one serving is equal to 12 pretzels, how many
        servings of bite-sized pretzels was Marian able to prepare?
        */
        int biteSizedPretzels = 480;
        int pretzelServingSize = 12;
        int necessaryPretzelServings = biteSizedPretzels / pretzelServingSize;
        System.out.println(necessaryPretzelServings);
        
        
        
        /*
        44. Lastly, she baked 53 lemon cupcakes for the children living in the city
        orphanage. If two lemon cupcakes were left at home, how many
        boxes with 3 lemon cupcakes each were given away?
        */
        int totalLemomCupcakes = 53;
        int lemonCupcakesLeftAtHome = 2;
        int availableLemonCupcakes = totalLemomCupcakes - lemonCupcakesLeftAtHome;
        int lemonCupcakesPerBox = 3;
        int donatedBoxesOfLemonCupcakes = availableLemonCupcakes / lemonCupcakesPerBox;
        System.out.println(donatedBoxesOfLemonCupcakes);
        
        
        
        /*
        45. Susie's mom prepared 74 carrot sticks for breakfast. If the carrots
        were served equally to 12 people, how many carrot sticks were left
        uneaten?
        */
        int preparedCarrotSticks = 74;
        int peopleServedWithCarrots = 12;
        int carrotSticksUneaten = preparedCarrotSticks % peopleServedWithCarrots;
        System.out.println(carrotSticksUneaten);
        
        
        
        /*
        46. Susie and her sister gathered all 98 of their teddy bears and placed
        them on the shelves in their bedroom. If every shelf can carry a
        maximum of 7 teddy bears, how many shelves will be filled?
        */
        int combinedTeddyBears = 98;
        int teddyBearsPerShelf = 7;
        int shelvesFilledWithTeddyBears = combinedTeddyBears / teddyBearsPerShelf;
        System.out.println(shelvesFilledWithTeddyBears);
        
        
        /*
        47. Susie’s mother collected all family pictures and wanted to place all of
        them in an album. If an album can contain 20 pictures, how many
        albums will she need if there are 480 pictures?
        */
        int picturesPerAlbum = 20;
        int totalPictures = 480;
        int necessaryAlbums = totalPictures / picturesPerAlbum;
        System.out.println(necessaryAlbums);
        
        
        
        /*
        48. Joe, Susie’s brother, collected all 94 trading cards scattered in his
        room and placed them in boxes. If a full box can hold a maximum of 8
        cards, how many boxes were filled and how many cards are there in
        the unfilled box?
        */
        int totalTradingCards = 94;
        int boxCapacity = 8;
        int boxesFilled = totalTradingCards / boxCapacity;
        int cardsInUnFilledBox = totalTradingCards % boxCapacity;
        System.out.println("Boxes Filled:" + " " + boxesFilled);
        System.out.println("Amount of cards in leftover box:" + " " + cardsInUnFilledBox);
        
        
        
        /*
        49. Susie’s father repaired the bookshelves in the reading room. If he has
        210 books to be distributed equally on the 10 shelves he repaired,
        how many books will each shelf contain?
        */
        int totalReadingBooks = 210;
        int totalBookShelves = 10;
        int readingBooksPerShelf = totalReadingBooks / totalBookShelves;
        System.out.println(readingBooksPerShelf);
        
        

        /*
        50. Cristina baked 17 croissants. If she planned to serve this equally to
        her seven guests, how many will each have?
        */
        int totalCristingCroissants = 17;
        int guestsOfCristing = 7;
        int equalServingOfCroissants = totalCristingCroissants / guestsOfCristing;
        System.out.println(equalServingOfCroissants);
        
        

        /*
            CHALLENGE PROBLEMS
        */
        
        /*
        Bill and Jill are house painters. Bill can paint a 12 x 14 room in 2.15 hours, while Jill averages 
        1.90 hours. How long will it take the two painter working together to paint 5 12 x 14 rooms?
        Hint: Calculate the hourly rate for each painter, combine them, and then divide the total walls in feet by the combined hourly rate of the painters.
        Challenge: How many days will it take the pair to paint 623 rooms assuming they work 8 hours a day?.
        */
        float billHourlyRate = (12 * 14) / (2.15f);
        float jillHourlyRate = (12 * 14) / (1.90f);
        float combinedHourlyRate = billHourlyRate + jillHourlyRate; //166.56
        System.out.println(combinedHourlyRate);
        float totalWallFootage = (12 * 14) * 5f;
        float timeCombinedForFiveRooms = totalWallFootage / combinedHourlyRate;
        System.out.println("It will take " + timeCombinedForFiveRooms + " hours to complete 5 walls for Bill and Jill.");
        
        float totalBigJobWallFootage = (12 * 14) * 623f;  //104664
        float combinedHourlyRatePerDay = combinedHourlyRate * 8f; // 1332.48
        float roomsCompletedPerDay = totalBigJobWallFootage / combinedHourlyRatePerDay;
        System.out.println("It will take " + roomsCompletedPerDay + " days to paint 623 rooms.");
        
        
        /*
        Create and assign variables to hold your first name, last name, and middle initial. Using concatenation,
        build an additional variable to hold your full name in the order of last name, first name, middle initial. The
        last and first names should be separated by a comma followed by a space, and the middle initial must end
        with a period.
        Example: "Hopper, Grace B." 
        */
        
        String firstName = "Evan";
        String middleInitial = "P";
        String lastName = "Frabell";
        String fullName = (lastName + ", " + firstName + " " + middleInitial + "." );
        System.out.println(fullName);

        /*
        The distance between New York and Chicago is 800 miles, and the train has already traveled 537 miles. 
        What percentage of the trip has been completed?
        Hint: The percent completed is the miles already traveled divided by the total miles.
        Challenge: Display as an integer value between 0 and 100 using casts.
        */
        float tripTotalDistance = 800f;
        float currentDistanceTravelled = 537f;
        float tripCompletion = (currentDistanceTravelled / tripTotalDistance) * 100;
        int convertToPercentage = (int)tripCompletion;
        System.out.println(convertToPercentage + "% of the trip has been traveled thus far.");
        
        
        
 
            
	}

}
