package com.techelevator;

import java.util.Scanner;

/*
 The Fibonacci numbers are the integers in the following sequence:  
	0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, ...
 By definition, the first two numbers in the Fibonacci sequence are 0 and 1, and each subsequent number is the sum of the previous two.
 
Write a command line program which prompts the user for an integer value and display the Fibonacci sequence leading up to that number.

  
 $ java Fiboncci
 
Please enter the Fibonacci number: 25
 
 0, 1, 1, 2, 3, 5, 8, 13, 21
 */
public class Fibonacci {

	public static void main(String[] args) {		//							1a + 1b = 2c
		//variables								//							1b + 2c = 3a	
		boolean done = false;					//							2c + 3a = 5b
		String userEntry;						// a + b = c					3 + 5 = 8
		int nCounter = 0;						// b + c = c					5 + 8 = 13
												//							8 + 13 = 21
		int a = 1;
		int b = 1;
		int c = 0;
					
		Scanner inputReader = new Scanner(System.in);
					
	
		
		
		System.out.print("Please enter an integer value you'd like to see for the Fibonacci sequence:  ");
		userEntry = inputReader.nextLine();
						
	
		nCounter = Integer.parseInt(userEntry);
		
		System.out.println();
			
			if(nCounter == 0) {
				System.out.print(0);
			} else if(nCounter == 1) {
				System.out.println("011");
			} else {
				for( int i = 0; i <= nCounter; i = i + 1) {
					if(c == 0) {
						c = a + b;
						i = c;
						System.out.print("0 " + "0 " + "1 " + c);
					} else if((a < c) && (a <= b)) {     
						a = b + c;
						i = a;
						System.out.print(" " + a);		// 1b + 2c = 3a
					} else if((b < a) && (b < c)) {		// 
						b = c + a;
						i = b;									// 2c + 3a = 5b
						System.out.print(" " + b);
					} else {
						c = a + b;
						i = c;
						System.out.print(" " + c);
					}
				}
			}
		
		}
		
	
		
		
					
	
	
	}


