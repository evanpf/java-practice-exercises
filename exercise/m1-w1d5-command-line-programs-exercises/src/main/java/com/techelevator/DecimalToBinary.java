package com.techelevator;

import java.util.Scanner;

/*
Write a command line program which prompts the user for a series of decimal integer values  
and displays each decimal value as itself and its binary equivalent

$ DecimalToBinary 

Please enter in a series of decimal values (separated by spaces): 460 8218 1 31313 987654321

460 in binary is 111001100
8218 in binary is 10000000011010
1 in binary is 1
31313 in binary is 111101001010001
987654321 in binary is 111010110111100110100010110001
*/
public class DecimalToBinary {

	public static void main(String[] args) {
		//variables	
		boolean done = false;
		String userEntry;
		int numberValue;
		
		Scanner inputReader = new Scanner(System.in);
		
		while(!done) {
			
			//Ask user for decimal value
			System.out.print("Please enter a decimal value you'd like to convert to binary:  ");
			userEntry = inputReader.nextLine();
			
			//parse the decimal value?
			numberValue = Integer.parseInt(userEntry);
			
			//Convert decimal value to binary... Formula value/2 quotient remainder bit# 
			String binary = Integer.toString(numberValue, 2);
			
			//return decimal and binary values
			System.out.print(binary);
			//restart program
			System.out.println();
			System.out.println();

			System.out.print("Convert another value?  Enter Y or N: ");
			String doAnother = inputReader.nextLine();
			
			if(doAnother.toLowerCase().equals("n")) {
				done = true;
			}
			
		}
		inputReader.close();
	}
	
}
