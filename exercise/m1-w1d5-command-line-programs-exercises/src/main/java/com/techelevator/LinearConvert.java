package com.techelevator;

import java.util.Scanner;

/*
 The foot to meter conversion formula is:
 	m = f * 0.3048
 	
 The meter to foot conversion formula is:
 	f = m * 3.2808399
 	
 Write a command line program which prompts a user to enter a length, and whether the measurement is in (m)eters or (f)eet.
 Convert the length to the opposite measurement, and display the old and new measurements to the console.
  
 $ java LinearConvert
 Please enter the length: 58
 Is the measurement in (m)eter, or (f)eet? f
 58f is 17m.
 */

public class LinearConvert {

	public static void main(String[] args) {
		
		boolean done = false;
		String measurementType;
		String weightString;
		double weight;
		double convertedWeight = 0;
		
		Scanner inputReader = new Scanner(System.in);
		
		while(!done) {
			
		System.out.print("What is your current measurement (m)meter or (f)feet?:  ");
		measurementType = inputReader.nextLine();
		measurementType = measurementType.toLowerCase();
		
		
		System.out.print("Enter the length:  ");
		weightString = inputReader.nextLine();
		
		weight = Double.parseDouble(weightString);
		
	
		
		if(measurementType.equals("m")) {
			convertedWeight =  weight * 3.2808399;
			System.out.print(weight + " meters is equal to " + convertedWeight + " feet.");
		}else {
			convertedWeight = weight * 0.3048;  
			System.out.print(weight + " feet is equal to " + convertedWeight + " meters.");
			
		}
			
		System.out.println();
		System.out.println();	
			
			
			
		System.out.print("Do you want to convert another weight? Y or N: ");
		String doAnother = inputReader.nextLine();
			
		if(doAnother.toLowerCase().equals("n")) {
			done = true;
			}
			
		}	
		
		inputReader.close();
	}
	

}


