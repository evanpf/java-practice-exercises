package com.techelevator;
import java.util.Scanner;

/*
 The Fahrenheit to Celsius conversion formula is:
 	Tc = (Tf - 32) / 1.8
 	where 'Tc' is the temperature in Celsius, and 'Tf' is the temperature in Fahrenheit
 	
 The Celsius to Fahrenheit conversion formula is:
 	Tf = Tc * 1.8 + 32
 	
 Write a command line program which prompts a user to enter a temperature, and whether its in degrees (C)elsius or (F)arenheit.
 Convert the temperature to the opposite degrees, and display the old and new temperatures to the console.
  
 $ java TempConvert
 Please enter the temperature: 58
 Is the temperature in (C)elcius, or (F)arenheit? F
 58F is 14C.
 
 Note why Tf - 32 above is enclosed in parentheses with a comment inside your code. You'll feel better for it, 
 and will start building some good programming habits while you're at it.
 */
public class TempConvert {

	public static void main(String[] args) {

	
	boolean done = false;		
	String tempString;			
	double temp;					
	String tempScaleString;		
	double convertedTemp = 0;
	
	
	
	Scanner inputReader = new Scanner(System.in);
	
	while(!done) {
	
		
		System.out.print("Enter the temperature: ");
		tempString = inputReader.nextLine();
		
		
		System.out.print("Enter the scale of the above temperature as (C) for Celsius or (F) for Fahrenheit: ");
		tempScaleString = inputReader.nextLine();
		tempScaleString = tempScaleString.toLowerCase();
		
		
		temp = Double.parseDouble(tempString);
		
		boolean scaleIsValid = false;
		
		if(tempScaleString.equals("c")) {
			
			convertedTemp =  ((temp - 32.0) * (5.0 / 9.0));
			scaleIsValid = true;
		} else if(tempScaleString.equals("f")){
			convertedTemp = (temp * (9.0 / 5.0) + 32.0);  
			scaleIsValid = true;
			
		} else {
			System.out.println("Invalid temperature scale.  Please enter C or F");
		}
		

		if(scaleIsValid) {
			System.out.printf("%.2f => %.2f", temp, convertedTemp);
		
		System.out.println();
		System.out.println();

		System.out.print("Convert another temperature?  Enter Y or N: ");
		String doAnother = inputReader.nextLine();
		
		if(doAnother.toLowerCase().equals("n")) {
			done = true;
		}
		
	}
	
	
	inputReader.close();
	
	}	
}
}
