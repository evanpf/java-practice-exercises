function sumDouble(x, y) {
    let sum = 0;
    
    if(x == y) {
        sum = (x + y) * 2;
    }else {
        sum = x + y;
    }
    
    return sum;
}

function hasTeen(a, b, c) {
    
    if(a >= 13 && a <= 19) {
        return true;
    }else if(b >= 13 && b <= 19) {
        return true;
    }else if(c >= 13 && c <= 19) {
        return true;
    }
    return false;
}

function lastDigit(a, b) {
    let aLast = (""+a).split("").pop();
    let bLast = (""+b).split("").pop();
    if(aLast == bLast){
        return true;
    } else {
        return false;
    } 
}

function seeColor(text) {
    if(text.indexOf("red") == 0) {
        return "red";
    }else if(text.indexOf("blue") == 0) {
        return "blue";
    }
    return "";
}

function middleThree(text) {
    let fullWord = text.length;
    let half = text.length / 2;
    text = text.slice(half - 1, half + 2);
    return text;
}

function frontAgain(text) {
    let frontEnd = text.slice(0, 2);
    let backEnd = text.slice(text.length - 2, text.length);

    if(frontEnd == backEnd) {
        return true;
    }else {
        return false;
    }
}



function alarmClock(day, vacation) {
    var dayF;

    switch (day) {
        case 0:
            dayF = "Sunday";
            break;
        case 1:
            dayF = "Monday";
            break;
        case 2:
            dayF = "Tuesday";
            break;
        case 3:
            dayF = "Wednesday";
            break;
        case 4:
            dayF = "Thursday";
            break;
        case 5:
            dayF = "Friday";
            break;
        case 6:
            dayF = "Saturday";
    }

    if((day >= 1 || day <= 5) && vacation == false) {
            return dayF + " " + "7:00"; 
    } else if (( day == 6 || day == 0 ) && (vacation == false)) {
        return dayF + " " + "10:00"
    } else if (( day == 6 || day == 0 ) && (vacation == true)) {
        return dayF + " " + "off"
    } else {
        return dayF + " " + "10:00";
    }
}

function makeMiddle(inputArray) {
    let zeroIndex;
    let oneIndex;
   
    if(inputArray.length < 2 || inputArray.length % 2 == 1) {
        return [];
    } else {
        zeroIndex = inputArray[inputArray.length / 2 - 1];
        oneIndex = inputArray[inputArray.length / 2]; 
        
        return [zeroIndex, oneIndex];
    }   
}

function oddOnly(input) {
    let oddArray = new Array();
    //let oddArray = [];
    
    for(i = 0; i < input.length; i++) {
        if(input[i] % 2 == 1) {
            oddArray.push(input[i]); 
        }
    } 
    return oddArray;
}

function weave(array1, array2) {
    let array3 = [];
    if (array1.length >= array2.length) {
        for (var i = 0; i < array1.length; i++) {
            array3.push(array1[i]);
            if (array2[i] != undefined) {
                array3.push(array2[i]);
            }
        }


    } else if (array2.length > array1.length) {
        for (var i = 0; i < array2.length; i++) {
            if (array1[i] != undefined) { 
                array3.push(array1[i]);
            }
        array3.push(array2[i]);

    }
}
return array3;   
}

function cigarParty(cigars, weekend) {
    if(weekend) {
        if(cigars >= 40) {
            return true;
        }
    } else if(!weekend) {
        if(cigars >= 40 && cigars <= 60){
            return true;
        }
    }
    return false;
}

function stringSplosion(text) {
    let fullText = "";

    for(i = 0; i <= text.length; i++) {
        fullText += text.substring(0, i);
    }
    return fullText;
}

function fizzBuzz(num) {
    
    if(num % 5 == 0 && num % 3 == 0) {
        return "FizzBuzz"
    }else if(num % 3 == 0) {
        return "Fizz";
    }else if(num % 5 == 0) {
        return "Buzz";
    }
    return num;

}

function countValues(array) {
    result = {};
    
    for (i of array) {
        if (result[i] !== undefined) {
            result[i] = result[i] + 1;
        } else {
            result[i] = 1;
        }
    } 
    return result;
}

function reverseArray(array) {

    let rArray = [];

    if(array[0] == null) {
        return rArray;
    }

    for(i = array.length - 1; i >= 0; i--) {
        rArray.push(array[i]);
    }
    return rArray;

    // return array.reverse();
}

function blackjack(a, b) { //19, 22 r 0x
    let trueA = 21 - a;
    let trueB = 21 - b;
   
    if(a > 21 && b > 21) {
        return 0;
    }

    if(a <= 21 && b > 21) {
        return a;
    }else if(a > 21 && b <= 21) {
        return b;
    }else if( trueA <= trueB) {
        return a;
    }
    return b;
     
}