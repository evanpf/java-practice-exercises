package com.techelevator;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class WordSearch {


    public static void main(String[] args) throws FileNotFoundException {

        boolean caseSensitive = true;
        String currentLine;
        int counter = 0;



        Scanner userInput = new Scanner(System.in);
        System.out.print("Enter a new search item:  ");
        String wordSearch = userInput.nextLine();
        System.out.print("Would you like your search to be case sensitive? (y or n):  ");
        String sensAnswer = userInput.nextLine();


        if (sensAnswer.toLowerCase().equals("n")) {
            caseSensitive = false;
        } else {
            System.out.println("System defaults to case sensitive search.  You should have entered a proper \"y\" or \"n\".");
            System.out.println();
            caseSensitive = true;
        }


        File inputFile = new File("alices_adventures_in_wonderland.txt");
        Scanner reader = new Scanner(inputFile);

        if (caseSensitive) {
            while (reader.hasNextLine()) {
                currentLine = reader.nextLine();
                counter = counter + 1;

                if (currentLine.contains(wordSearch)) {
                    System.out.println(counter + ") " + currentLine);

                }

            }
        } else if (!caseSensitive) {
            while (reader.hasNextLine()) {
                currentLine = reader.nextLine();
                String converter = currentLine.toLowerCase();
                String converter2 = wordSearch.toLowerCase();

                counter = counter + 1;

                if(converter.contains(converter2)) {
                    System.out.println(counter + ") " + currentLine);
                }


            }


        }


    }
}

