package com.techelevator;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class QuizMaker {

	public static void main(String[] args) throws FileNotFoundException {
		String fullLine;
		int answer;
		int counter = 0;
		boolean rating = false;


		Scanner userInput = new Scanner(System.in);
		System.out.println("Please upload a quiz file: ");
		String uploadPath = userInput.nextLine();

		File uploadFile = new File(uploadPath);

		try (Scanner uploadScanner = new Scanner(uploadFile)) {

			while (uploadScanner.hasNextLine()) {

				fullLine = uploadScanner.nextLine();

				String[] question = fullLine.split("\\|");
				System.out.println(question[0]);

					for(int i = 1; i < question.length; i++) {
						if(question[i].endsWith("*")) {
							System.out.print(i + ". ");
							System.out.println(question[i].substring(0, question[i].length()-1));
						}
						else {
							System.out.print(i + ". ");
							System.out.println(question[i]);
						}
					}
				System.out.println();
				System.out.print("What is your answer? Please enter the number corresponding to your selection:  ");
				answer = Integer.parseInt(userInput.nextLine());
					for(int i = 1; i < question.length; i++) {
						if (question[i].contains("*") && i == answer) {
							rating = true;
						}

					}
						if(rating) {
							System.out.println();
							System.out.println("You got it!");
							System.out.println();
							counter ++;
							rating = false;

						} else {
							System.out.println();
							System.out.println("FAILURE!");
							System.out.println();
						}

			}
			System.out.println();
			System.out.println("You received a " + counter + "/4");

		}
	}

}
