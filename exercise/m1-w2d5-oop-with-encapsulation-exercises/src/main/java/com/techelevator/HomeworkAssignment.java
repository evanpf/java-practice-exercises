package com.techelevator;

public class HomeworkAssignment {
    //Attribute | Data Type | Get | Set | Description |
    //|----------|-----------|-----|-----|-------------|
    //| totalMarks | int | X | X | The total number of correct marks received on the assignment. |
    //| possibleMarks | int | X | | The number of possible marks on the assignment. |
    //| submitterName | string | X | X | The submitter's name for the assignment. |
    // letterGrade *(derived)* | string | X | | The letter grade for the assignment. |
    private int totalMarks;
    private int possibleMarks;
    private String submitterName;

    public int getTotalMarks() {
        return totalMarks;
    }

    public void setTotalMarks(int totalMarks) {
        this.totalMarks = totalMarks;
    }

    public int getPossibleMarks() {
        return possibleMarks;
    }

    public String getSubmitterName() {
        return submitterName;
    }

    public void setSubmitterName(String submitterName) {
        this.submitterName = submitterName;
    }


    public HomeworkAssignment(int possibleMarks) {
      this.possibleMarks = possibleMarks;
    } //constructor

    public String getLetterGrade() {        //Why do I need this as a getter?
        double score = (double)totalMarks / possibleMarks;
        if(((score) * 100) >= 90) {
            return "A";
        }else if(((score) * 100) >= 80) {
            return "B";
        }else if(((score) * 100) >= 70) {
            return "C";
        }else if(((score) * 100) >= 60) {
            return "D";
        }else {
            return "F";
        }
    }

    //**Notes**
    //- `letterGrade` is a derived attribute that is calculated using totalMarks and possibleMarks.
    //    - For 90% or greater return "A"
    //    - 80-89% return "B"
    //    - 70-79% return "C"
    //    - 60-69% return "D"
    //    - otherwise return "F"




}
