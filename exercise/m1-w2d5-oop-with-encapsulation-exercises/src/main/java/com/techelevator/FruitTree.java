package com.techelevator;

public class FruitTree {


    private String typeOfFruit;
    private int piecesOfFruitLeft;

    public FruitTree(String typeOfFruit, int startingPiecesOfFruit) { // fruit type & number of fruits on tree
        this.typeOfFruit = typeOfFruit;
        this.piecesOfFruitLeft = startingPiecesOfFruit;

    }

    public String getTypeOfFruit() {

        return typeOfFruit;
    }

    public int getPiecesOfFruitLeft() {

        return piecesOfFruitLeft;
    }

    public boolean pickFruit(int numberOfPiecesToRemove) { //True or false? their are # of fruits to remove
        if(piecesOfFruitLeft - numberOfPiecesToRemove >= 0) { //if there's more than -1 fruits left
            piecesOfFruitLeft = piecesOfFruitLeft - numberOfPiecesToRemove;
            return true;

        } else {
            return false;
        }
    }
}
