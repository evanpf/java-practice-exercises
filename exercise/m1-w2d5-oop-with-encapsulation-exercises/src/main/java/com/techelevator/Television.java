package com.techelevator;

public class Television {


    private boolean isOn;       //Whether or not the TV is turned on.
    private int currentChannel = 3; //The value for the current channel. Channel levels go between 3 and 18.
    private int currentVolume = 2;  //The current volume level.

    public boolean isOn() {
        return isOn;
    }

    public int getCurrentChannel() {
        return currentChannel;
    }

    public int  getCurrentVolume() {
        return currentVolume;
    }


    public void turnOff() {
        isOn = false;
    }

    public void turnOn() {
        isOn = true;
        currentChannel = 3;
        currentVolume = 2;
    }

    public void changeChannel(int newChannel) {
        if(isOn && (newChannel > 3 && newChannel < 18)) {
           currentChannel = newChannel;
        }
    }

    public void channelUp() {
        if(isOn && currentChannel < 18) {
            currentChannel  = currentChannel + 1;
        } else {
            currentChannel = 3;
        }
    }

    public void channelDown() {
        if(isOn) {
            if(currentChannel > 3) {
            currentChannel  = currentChannel - 1;
            }else {
            currentChannel = 18;
            }
        }
    }

    public void raiseVolume() {
        if(isOn && currentVolume <= 10) {
            currentVolume += 1;
        }
    }

    public void lowerVolume() {
        if(isOn && currentVolume > 0) {
            currentVolume -= 1;
        }
    }


}
