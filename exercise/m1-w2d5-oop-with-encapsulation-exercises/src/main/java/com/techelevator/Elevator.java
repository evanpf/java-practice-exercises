package com.techelevator;

public class Elevator {

private int currentFloor = 1; //New elevators start on floor 1.
private int numberOfFloors;
private boolean doorOpen;

public int getCurrentFloor() {
    return currentFloor;
} //The current floor that the elevator is on.

public int getNumberOfFloors() {
    return numberOfFloors;
} //The number of floors available to the elevator. |

public boolean getDoorOpen() {
    return doorOpen;
} //Whether the elevator door is open or not.


public Elevator(int totalNumberOfFloors) {
    this.numberOfFloors = totalNumberOfFloors;
}

public boolean isDoorOpen() {
    return this.doorOpen;
}

public void openDoor() {
    doorOpen = true;
}
public void closeDoor() {
    doorOpen = false;
}
public void goUp(int desiredFloor) {
    if(!(doorOpen) && desiredFloor <= numberOfFloors && desiredFloor > currentFloor) {
        currentFloor = desiredFloor;
    }

}
public void goDown(int desiredFloor) {
    if(!(doorOpen) && desiredFloor > 0 && desiredFloor < currentFloor) {
        currentFloor = desiredFloor;
    }
}
//
//**Notes**
//- `openDoor()` opens the elevator door.
//- `closeDoor()` closes the elevator door.
//- `goUp(int desiredFloor)` sends the elevator upward to the desired floor as long as the door is not open. Cannot go past last floor.
//- `goDown(int desiredFloor)` sends the elevator downward to the desired floor as long as the door is not open. Cannot go past floor 1.


}
