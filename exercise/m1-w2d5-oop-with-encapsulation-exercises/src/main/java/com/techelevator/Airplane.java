package com.techelevator;

public class Airplane {

    private String planeNumber;
    private int bookedFirstClassSeats = 0;
    private int totalFirstClassSeats;
    private int bookedCoachSeats = 0;
    private int totalCoachSeats;


    public String getPlaneNumber() {
        return planeNumber;
    }

    public int getBookedFirstClassSeats() {
        return bookedFirstClassSeats;
    }

    public int getAvailableFirstClassSeats() {          //derived
        return totalFirstClassSeats - bookedFirstClassSeats;
    }

    public int getTotalFirstClassSeats() {
        return totalFirstClassSeats;
    }

    public int getBookedCoachSeats() {
        return bookedCoachSeats;
    }

    public int getAvailableCoachSeats() {              //derived
        return totalCoachSeats - bookedCoachSeats;
    }

    public int getTotalCoachSeats() {
        return totalCoachSeats;
    }



    public Airplane(String planeNumber, int totalFirstClassSeats, int totalCoachSeats) {       //Constructor
        this.planeNumber = planeNumber;
        this.totalFirstClassSeats = totalFirstClassSeats;
        this.totalCoachSeats = totalCoachSeats;
    }



    public boolean reserveSeats(boolean forFirstClass, int totalNumberOfSeats) {        //Method
        if(forFirstClass) {
            if(totalNumberOfSeats > getAvailableFirstClassSeats()) {
                return false;
            }
            bookedFirstClassSeats = bookedFirstClassSeats + totalNumberOfSeats;
        }else {
            if (totalNumberOfSeats > getAvailableCoachSeats()) {
                return false;
            }
            bookedCoachSeats = bookedCoachSeats + totalNumberOfSeats;
        }
        return true;
        }

}
