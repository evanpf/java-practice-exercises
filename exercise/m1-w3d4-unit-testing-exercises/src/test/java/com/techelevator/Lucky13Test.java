package com.techelevator;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class Lucky13Test {
    private Lucky13 test;

    @Before
    public void setUp() throws Exception {
        test = new Lucky13();
    }

    @After
    public void tearDown() throws Exception {
        test = null;
    }

    @Test
    public void getLucky() {
        assertEquals("Input: Lucky13([0, 2, 4])", true, test.getLucky(new int[] {0, 2, 4}));
        assertEquals("Input: Lucky13([1, 2, 3])", false, test.getLucky(new int[] {1, 2, 3}));
        assertEquals("Input: Lucky13([1, 2, 4])", false, test.getLucky(new int[] {1, 2, 4}));
        assertEquals("Input: Lucky13([10, 30, 32])", true, test.getLucky(new int[] {10, 30, 32}));
        assertEquals("Input: Lucky13([7, 1, 6])", false, test.getLucky(new int[] {7, 1, 6}));
        assertEquals("Input: Lucky13([3, 1, 4])", false, test.getLucky(new int[] {3, 1, 4}));

    }
}

//  /*
//     Given an array of ints, return true if the array contains no 1's and no 3's.
//     GetLucky([0, 2, 4]) → true
//     GetLucky([1, 2, 3]) → false
//     GetLucky([1, 2, 4]) → false
//     */
//    public boolean getLucky(int[] nums) {
//        for (int i = 0; i < nums.length; i++) {
//            if (nums[i] == 1 || nums[i] == 3) {
//                return false;
//            }
//        }
//        return true;
//    }
//
//}