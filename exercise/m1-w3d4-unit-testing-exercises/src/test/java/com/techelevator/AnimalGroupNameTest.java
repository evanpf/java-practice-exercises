package com.techelevator;


import org.junit.Test;
import org.junit.Before;



import static org.junit.Assert.assertEquals;


public class AnimalGroupNameTest {

    private AnimalGroupName name;

    @Before
    public void init() {
        name = new AnimalGroupName();
    }

    @Test
    public void getHerd() {


        assertEquals("Input: animalGroupName(\"giraffe\")", "Tower", name.getHerd("giraffe"));
        assertEquals("Input: animalGroupName(\"elephant\")", "Herd", name.getHerd("ELEPHANT"));
        assertEquals("Input: animalGroupName(\"lion\")", "Pride", name.getHerd("LION"));
        assertEquals("Input: animalGroupName(\"\")", "unknown", name.getHerd(""));
        assertEquals("Input: animalGroupName(\"crow\")", "Murder", name.getHerd("Crow"));
        assertEquals("Input: animalGroupName(\"walrus\")", "unknown", name.getHerd("walrus"));
        assertEquals("Input: animalGroupName(\"elephant\")", "Herd", name.getHerd("elepHANT"));
    }

}





























