package com.techelevator;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.Arrays;
import static org.junit.Assert.assertArrayEquals;

public class MaxEnd3Test {
    private MaxEnd3 test;

    @Before
    public void setUp() throws Exception {
        test = new MaxEnd3();
    }

    @After
    public void tearDown() throws Exception {
        test = null;
    }

    @Test
    public void makeArray() {
        assertArrayEquals("Input: makeArray(new int[] {11, 5, 9})", new int[]{11, 11, 11}, test.makeArray(new int[]{11, 5, 9}));
        assertArrayEquals("Input: makeArray(new int[] {1, 2, 3})", new int[]{3, 3, 3}, test.makeArray(new int[]{1, 2, 3}));
        assertArrayEquals("Input: makeArray(new int[] {4, 4, 4})", new int[]{4, 4, 4}, test.makeArray(new int[]{4, 4, 4}));
        assertArrayEquals("Input: makeArray(new int[] {53, 566, 8})", new int[]{530, 530, 530}, test.makeArray(new int[]{530, 56, 8}));
        assertArrayEquals("Input: makeArray(new int[] {0, 0, 0})", new int[]{0, 0, 0}, test.makeArray(new int[]{0, 0, 0}));
        assertArrayEquals("Input: makeArray(new int[] {1, 8, 9})", new int[]{9, 9, 9}, test.makeArray(new int[]{1, 8, 9}));
        assertArrayEquals("Input: makeArray(new int[] {1, 15, 9})", new int[]{9, 9, 9}, test.makeArray(new int[]{1, 15, 9}));
    }
}

// /*
//     Given an array of ints length 3, figure out which is larger between the first and last elements
//     in the array, and set all the other elements to be that value. Return the changed array.
//     MakeArray([1, 2, 3]) → [3, 3, 3]
//     MakeArray([11, 5, 9]) → [11, 11, 11]
//     MakeArray([2, 11, 3]) → [3, 3, 3]
//     */
//    public int[] makeArray(int[] nums) {
//        int largerNum = (nums[0] > nums[nums.length - 1]) ? nums[0] : nums[nums.length - 1];
//
//        for (int i = 0; i < nums.length; i++){
//            nums[i] = largerNum;
//        }
//
//        return nums;
//    }
//
//}