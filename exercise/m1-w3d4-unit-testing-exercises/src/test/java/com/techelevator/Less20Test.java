package com.techelevator;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class Less20Test {

    private Less20 test;

    @Before
    public void setUp() throws Exception {
        test = new Less20();
    }

    @After
    public void tearDown() throws Exception {
        test = null;
    }

    @Test
    public void isLessThanMultipleOf20() {
        assertEquals("Input: less20(18)", true, test.isLessThanMultipleOf20(18));
        assertEquals("Input: less20(19)", true, test.isLessThanMultipleOf20(19));
        assertEquals("Input: less20(20)", false, test.isLessThanMultipleOf20(20));
        assertEquals("Input: less20(38)", true, test.isLessThanMultipleOf20(38));
        assertEquals("Input: less20(39)", true, test.isLessThanMultipleOf20(39));
        assertEquals("Input: less20(37)", false, test.isLessThanMultipleOf20(37));
        assertEquals("Input: less20(240)", false, test.isLessThanMultipleOf20(240));
        assertEquals("Input: less20(238)", true, test.isLessThanMultipleOf20(238));
        assertEquals("Input: less20(237)", false, test.isLessThanMultipleOf20(237));
        assertEquals("Input: less20(353)", false, test.isLessThanMultipleOf20(353));
        assertEquals("Input: less20(10)", false, test.isLessThanMultipleOf20(10));

    }
}

// /*
//	     Return true if the given non-negative number is 1 or 2 less than a multiple of 20. So for example 38
//	     and 39 return true, but 40 returns false.
//	     (Hint: Think "mod".)
//	     less20(18) → true
//	     less20(19) → true
//	     less20(20) → false
//     */
//    public boolean isLessThanMultipleOf20(int n) {
//        boolean oneLessThanMultipleOf20 = (n % 20 == 19);
//        boolean twoLessThanMultipleOf20 = (n % 20 == 18);
//
//        return oneLessThanMultipleOf20 || twoLessThanMultipleOf20;