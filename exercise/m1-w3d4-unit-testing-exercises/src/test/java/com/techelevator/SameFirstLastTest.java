package com.techelevator;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class SameFirstLastTest {

    private SameFirstLast test;

    @Before
    public void setUp() throws Exception {
        test = new SameFirstLast();
    }

    @After
    public void tearDown() throws Exception {
        test = null;
    }

    @Test
    public void isItTheSame() {
        assertEquals("Input: IsItTheSame(new int[] {1, 2, 3}", false, test.isItTheSame(new int[] {1, 2, 3}));
        assertEquals("Input: IsItTheSame(new int[] {1, 2, 3, 1}", true, test.isItTheSame(new int[] {1, 2, 3, 1}));
        assertEquals("Input: IsItTheSame(new int[] {1, 2, 1}", true, test.isItTheSame(new int[] {1, 2, 1}));
        assertEquals("Input: IsItTheSame(new int[] {1, 2, 3, 5, 7, 7, 1}", true, test.isItTheSame(new int[] {1, 2, 3, 5, 7, 7, 1}));
        assertEquals("Input: IsItTheSame(new int[] {0, 2, 0}", true, test.isItTheSame(new int[] {0, 2, 0}));
        assertEquals("Input: IsItTheSame(new int[] {1}", true, test.isItTheSame(new int[] {1}));
        assertEquals("Input: IsItTheSame(new int[] {1, 18, 72, 8}", false, test.isItTheSame(new int[] {1, 18, 72, 8}));
    }
}

//   /*
//     Given an array of ints, return true if the array is length 1 or more, and the first element and
//     the last element are equal.
//     IsItTheSame([1, 2, 3]) → false
//     IsItTheSame([1, 2, 3, 1]) → true
//     IsItTheSame([1, 2, 1]) → true
//     */
//    public boolean isItTheSame(int[] nums) {
//        return (nums.length > 0 && nums[0] == nums[nums.length - 1]);
//    }