package com.techelevator;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class CigarPartyTest {

        private CigarParty party;

        @Before
        public void init() {
            party = new CigarParty();
        }

                                //String        expected        passed in
        @Test
    public void haveParty() {
        assertEquals("Input: cigarParty(30, false)", false, party.haveParty(30, false));
        assertEquals("Input: cigarParty(50, false)", true, party.haveParty(50, false));
        assertEquals("Input: cigarParty(70, true)", true, party.haveParty(70, true));
        assertEquals("Input: cigarParty(25, true)", false, party.haveParty(25, true));
        assertEquals("Input: cigarParty(70, false)", false, party.haveParty(70, false));
    }
}

/*
     When squirrels get together for a party, they like to have cigars. A squirrel party is successful
     when the number of cigars is between 40 and 60, inclusive. Unless it is the weekend, in which case
     there is no upper bound on the number of cigars. Return true if the party with the given values is
     successful, or false otherwise.
     haveParty(30, false) → false
     haveParty(50, false) → true
     haveParty(70, true) → true
     */