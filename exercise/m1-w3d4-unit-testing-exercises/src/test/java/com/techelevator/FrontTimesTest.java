package com.techelevator;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class FrontTimesTest {

    private FrontTimes test;

    @Before
    public void setUp() throws Exception {
        test = new FrontTimes();
    }

    @After
    public void tearDown() throws Exception {
        test = null;
    }

    @Test
    public void generateString() {
        assertEquals("Input: frontTimes(\"Chocolate\", 2)", "ChoCho", test.generateString("Chocolate", 2));
        assertEquals("Input: frontTimes(\"Chocolate\", 3)", "ChoChoCho", test.generateString("Chocolate", 3));
        assertEquals("Input: frontTimes(\"Abc\", 2)", "AbcAbcAbc", test.generateString("AbcAbcAbc", 3));
        assertEquals("Input: frontTimes(\"M\", 5)", "MMMMM", test.generateString("M", 5));
        assertEquals("Input: frontTimes(\"\", 4)", "", test.generateString("", 4));
        assertEquals("Input: frontTimes(\"FRank\", 10)", "FRaFRaFRaFRaFRaFRaFRaFRaFRaFRa", test.generateString("FRank", 10));
        assertEquals("Input: frontTimes(\"NotMe\", 0)", "", test.generateString("NotMe", 0));
    }
}

///*
//	     Given a string and a non-negative int n, we'll say that the front of the string is the first 3 chars, or
//	     whatever is there if the string is less than length 3. Return n copies of the front;
//	     frontTimes("Chocolate", 2) → "ChoCho"
//	     frontTimes("Chocolate", 3) → "ChoChoCho"
//	     frontTimes("Abc", 3) → "AbcAbcAbc"
//     */
//    public String generateString(String str, int n) {
//        String result = "";
//        if (str.length() <= 3) {
//            for (int i = 0; i < n; i++) {
//                result += str;
//            }
//        }
//        else {
//            for (int i = 0; i < n; i++) {
//                result += str.substring(0, 3);
//            }
//        }
//        return result;
//    }
//
//}