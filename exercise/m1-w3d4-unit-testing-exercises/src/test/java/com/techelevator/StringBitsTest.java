package com.techelevator;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class StringBitsTest {
    private StringBits test;

    @Before
    public void setUp() throws Exception {
        test = new StringBits();
    }

    @After
    public void tearDown() throws Exception {
        test = null;
    }

    @Test
    public void getBits() {
        assertEquals("Input: GetBits(\"Hello\")", "Hlo", test.getBits("Hello"));
        assertEquals("Input: GetBits(\"Hi\")", "H", test.getBits("Hi"));
        assertEquals("Input: GetBits(\"Heeololeo\")", "Hello", test.getBits("Heeololeo"));
        assertEquals("Input: GetBits(\"YdOkUnRlEaGnOcNeNuAcWwItN\")", "YOUREGONNAWIN", test.getBits("YdOkUnRlEaGnOcNeNuAcWwItN"));
        assertEquals("Input: GetBits(\"\")", "", test.getBits(""));
    }
}

//  /*
//     Given a string, return a new string made of every other char starting with the first, so "Hello" yields "Hlo".
//     GetBits("Hello") → "Hlo"
//     GetBits("Hi") → "H"
//     GetBits("Heeololeo") → "Hello"
//     */
//    public String getBits(String str) {
//        String result = "";
//        for (int i = 0; i < str.length(); i++) {
//            if (i % 2 == 0) {
//                result += str.charAt(i);
//            }
//        }
//        return result;
//    }
//}