package com.techelevator;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class NonStartTest {

    private NonStart test;


    @Before
    public void setUp() throws Exception {
        test = new NonStart();
    }

    @After
    public void tearDown() throws Exception {
        test = null;
    }

    @Test
    public void getPartialString() {
        assertEquals("Input: GetPartialString(\"Hello\", \"There\")", "ellohere", test.getPartialString("Hello", "There"));
        assertEquals("Input: GetPartialString(\"java\", \"code\")", "avaode", test.getPartialString("java", "code"));
        assertEquals("Input: GetPartialString(\"shotl\", \"java\")", "hotlava", test.getPartialString("shotl", "java"));
        assertEquals("Input: GetPartialString(\"E\", \"F\")", "", test.getPartialString("E", "F"));
        assertEquals("Input: GetPartialString(\"Superciliatedpsuedostratisfied\", \"columnarepithilealtissue\")", "uperciliatedpsuedostratisfiedolumnarepithilealtissue", test.getPartialString("Superciliatedpsuedostratisfied", "columnarepithilealtissue"));
        assertEquals("Input: GetPartialString(\"TRecipe\", \"PBuilder\")", "RecipeBuilder", test.getPartialString("TRecipe", "PBuilder"));
    }
}
//  /*
//     Given 2 strings, return their concatenation, except omit the first char of each. The strings will
//     be at least length 1.
//     GetPartialString("Hello", "There") → "ellohere"
//     GetPartialString("java", "code") → "avaode"
//     GetPartialString("shotl", "java") → "hotlava"
//     */
//    public String getPartialString(String a, String b) {
//        if (a.length() == 0) {
//            return b.substring(1);
//        }
//        else if (b.length() == 0) {
//            return a.substring(1);
//        }
//        else {
//            return a.substring(1) + b.substring(1);
//        }
//    }