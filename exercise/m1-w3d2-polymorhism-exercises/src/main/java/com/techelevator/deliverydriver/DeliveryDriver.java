package com.techelevator.deliverydriver;

public interface DeliveryDriver {

   double calculateRate(double distance, double weight);
}
