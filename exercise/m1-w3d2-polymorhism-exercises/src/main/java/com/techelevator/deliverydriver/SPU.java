package com.techelevator.deliverydriver;

import com.techelevator.deliverydriver.DeliveryDriver;

public class SPU implements DeliveryDriver {
    private String shippingType;

    public SPU(String shippingType) {
        this.shippingType = shippingType;
    }




    @Override
    public double calculateRate(double distance, double weight) {
        if("four-day".equals(this.shippingType)) {
            return (weight * 0.0050) * distance;
        }
        if("two-day".equals(this.shippingType)) {
            return (weight * 0.050) * distance;
        }
        if("next day".equals(this.shippingType)) {
            return (weight * 0.075) * distance;
        }
        return 0.0;

    }
}
