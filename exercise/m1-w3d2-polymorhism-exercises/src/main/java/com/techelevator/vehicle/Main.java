package com.techelevator.vehicle;

public class Main {
    public static void main(String[] argsArray) {

//if used bigDecimal printF for two decimal places.

        Vehicle car = new Car(false);
        System.out.println(car.calculateToll(100));
        Vehicle car2 = new Car(true);
        System.out.println(car2.calculateToll(75));
        Vehicle tank = new Tank();
        System.out.println(tank.calculateToll(240));
        Vehicle truck = new Truck(6);
        System.out.println(truck.calculateToll(150));
    }
}
