package com.techelevator.vehicle;

public class Car implements Vehicle{
   private boolean hasTrailer;


   public Car(boolean hasTrailer) {
       this.hasTrailer = hasTrailer;
   }



    @Override
    public double calculateToll(double distance) {
       double toll = distance * 0.020;
       if(this.hasTrailer) {
           toll = toll + 1.00;
       }
       return toll;
    }
}
