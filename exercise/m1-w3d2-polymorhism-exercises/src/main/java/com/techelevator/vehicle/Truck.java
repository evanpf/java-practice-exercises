package com.techelevator.vehicle;

import java.util.HashMap;

public class Truck implements Vehicle {
    private double perMileRate;
    private HashMap<Integer, Double> rates = new HashMap<Integer, Double>(){
        {
            put(4, 0.040);
            put(6, 0.045);
            put(8, 0.048);
        }
    };

   public Truck(int axles) {
       if(axles >= 8) {
           this.perMileRate = this.rates.get(8);
       } else {
           this.perMileRate = this.rates.get(axles);
       }
   }



    @Override
    public double calculateToll(double distance) {
        return this.perMileRate * distance;
    }
}
