package com.techelevator;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class KataFizzBuzzTest {

    private KataFizzBuzz test;


    @Before
    public void setUp() throws Exception {
        test = new KataFizzBuzz();
    }

    @Test
    public void name() {
        assertEquals("Input: fizzBuzz(1)", "1", test.fizzBuzz(1));
        assertEquals("Input: fizzBuzz(2)", "2", test.fizzBuzz(2));
        assertEquals("Input: fizzBuzz(3)", "Fizz", test.fizzBuzz(3));
        assertEquals("Input: fizzBuzz(4)", "4", test.fizzBuzz(4));
        assertEquals("Input: fizzBuzz(5)", "Buzz", test.fizzBuzz(5));
        assertEquals("Input: fizzBuzz(15)", "FizzBuzz", test.fizzBuzz(15));
        assertEquals("Input: fizzBuzz(100)", "Buzz", test.fizzBuzz(100));
        assertEquals("Input: fizzBuzz(101)", "", test.fizzBuzz(101));
        assertEquals("Input: fizzBuzz(60)", "FizzBuzz", test.fizzBuzz(60));
    }
}
