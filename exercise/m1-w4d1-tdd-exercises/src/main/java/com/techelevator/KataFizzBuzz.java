package com.techelevator;

public class KataFizzBuzz {

    public String fizzBuzz(int number) {

        if(number < 1 || number > 100) {
            return "";
        }else if ((number % 15 == 0) || (Integer.toString(number).contains("3") && (Integer.toString(number).contains("5")))) {
            return "FizzBuzz";
        }else if(number % 5 == 0 || Integer.toString(number).contains("5")) {
            return "Buzz";
        }else if(number % 3 == 0 || Integer.toString(number).contains("3")) {
            return "Fizz";
        }else {
        return Integer.toString(number);
            }
        }

    }

