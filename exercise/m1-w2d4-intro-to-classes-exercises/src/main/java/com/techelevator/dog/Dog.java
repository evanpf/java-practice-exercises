package com.techelevator.dog;

public class Dog {

    //Attribute	DataType	Get	    Set	    Description
    //sleeping	boolean	    X		TRUE    if the dog is asleep. FALSE if not.

    private boolean sleeping;

    public boolean isSleeping() {
        return sleeping;
    }

    public String makeSound() {
        if(sleeping) {
            return "Zzzzz...";
        } else {
            return "Woof!";
        }
    }
    public void sleep(){
        sleeping = true;
    }
    public void wakeUp() {
        sleeping = false;
    }


    //makeSound() returns "Zzzzz..." if the dog is asleep. Returns "Woof!" if the dog is awake.
    //sleep() sets sleeping to true.
    //wakeUp() sets sleeping to false.

}



