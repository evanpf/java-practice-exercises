package com.techelevator.calculator;

public class Calculator {


    private int currentValue = 0;

    public int getCurrentValue() {
        return currentValue;
    }


    /**add(int addend) returns the new currentValue after performing the addition.
     *
     * @param addend
     * @return
     */
    public int add(int addend) {
        currentValue = currentValue + addend;
        return currentValue;
    }

    /**multiply(int multiplier) returns the new currentValue after performing the multiplication.
     *
     * @param multiplier
     * @return
     */
    public int multiply(int multiplier) {
        currentValue = currentValue * multiplier;
        return currentValue;
    }

    /**subtract(int subtrahend) returns the new currentValue after performing the subtraction.
     *
     * @param subtrahend
     * @return
     */
    public int subtract(int subtrahend) {
        currentValue = currentValue - subtrahend;
        return currentValue;
    }

    /**power(int exponent) returns the new currentValue after raising the currentValue by the exponent.
     *
     * @param exponent
     * @return
     */
    public int power(int exponent) {
        currentValue = (int) Math.pow(currentValue, exponent);
        return currentValue;
    }

    /**void reset() resets the currentValue to 0
     *
     */
    public void reset() {
        currentValue = 0;
    }
}
