
-- https://www.postgresql.org/docs/current/static/functions-math.html


SELECT name AS CountryName from country;

SELECT name, round(surfacearea) FROM country WHERE surfacearea < 10;


SELECT name, population FROM country ORDER BY population DESC;


SELECT name, continent FROM country ORDER BY continent ASC, name ASC;



--Cities in USA with population sorted by city name in ascending order and population in descending order
SELECT name, district, population FROM city WHERE countrycode = 'USA' ORDER BY name ASC, population DESC;


SELECT name, lifeexpectancy
FROM country
WHERE lifeexpectancy IS NOT NULL
ORDER BY lifeexpectancy DESC
LIMIT 10 OFFSET 11;



SELECT (name || ', ' || district) as name_and_state 
FROM city 
WHERE district='California' OR district='Oregon' OR district='Washington' 
ORDER BY district, name;


SELECT (language || ' is spoken in ' || countrycode) FROM countrylanguage;


SELECT SUM(population) FROM city WHERE district = 'Ohio';

SELECT MIN(surfacearea) FROM country;
SELECT MAX(surfacearea) FROM country;

SELECT round(cast(AVG(lifeexpectancy) as numeric), 2) FROM country;

SELECT count(name) FROM country;
SELECT count(*) FROM country;

---Smallest city population labeled "smallest_population" and largest as "largest_population"
SELECT MIN(population) AS smallest_population, MAX(population) AS largest_population FROM city;

--Total population, Number of Cities and Average population
SELECT SUM(population) AS total_city_population, COUNT(population) as number_of_cities, AVG(population) AS avergage_population FROM city;


SELECT DISTINCT countrycode from city;
SELECT countrycode from city GROUP BY countrycode;


SELECT district, SUM(population) FROM city WHERE countrycode = 'USA' GROUP BY district ORDER BY district;
SELECT district, AVG(population) FROM city WHERE countrycode='USA' GROUP BY district ORDER BY district;

-- Average life expectancy of each continent ordered from highest to lowest
SELECT continent, avg(lifeexpectancy) AS avg_lifeexpectancy FROM country WHERE continent <> 'Antarctica' GROUP BY continent ORDER BY avg_lifeexpectancy DESC;

SELECT countrycode, MIN(population), MAX(population) FROM city GROUP BY countrycode;

-- Count the number of countries where each language is spoken, order them from most countries to least
SELECT language, count(countrycode) AS countryCount
FROM countrylanguage
WHERE isofficial = TRUE
GROUP BY language
ORDER BY countryCount DESC;

-- Total surface area by form of government
SELECT governmentform, SUM(surfacearea) as TotalArea FROM country GROUP BY governmentform ORDER BY TotalArea DESC;

-- Find the names of cities under a given government leader
SELECT name FROM city WHERE countrycode IN (SELECT code FROM country WHERE headofstate = 'Beatrix');

-- Find the names of cities who's country they belong to has not declared independence yet
SELECT name FROM city WHERE countrycode IN (SELECT code FROM country WHERE indepyear IS NULL) ORDER BY name;
