package com.techelevator;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.Assert;

public class ExercisesTest {

	private Exercises ex;
	
	@Before
	public void setup() {
		ex = new Exercises();
	}
	
	@After
	public void teardown() {
		ex = null;
	}
	
	
	/*
	 arrayCount9([1, 2, 9]) → 1
	 arrayCount9([1, 9, 9]) → 2
	 arrayCount9([1, 9, 9, 3, 9]) → 3
	*/
	@Test
	public void arrayCount9Test_emptyArray() {
		
		Exercises exercises = new Exercises();
		
		int result = exercises.arrayCount9(new int[] {});
		Assert.assertEquals(0, result);
	}
	
	@Test
	public void arrayCount9Test_ArrayWithNoNines() {
		int result = ex.arrayCount9(new int[] {1, 0, 5, 8, 10, 11});
		Assert.assertEquals(0, result);		
		
		result = ex.arrayCount9(new int[] {Integer.MIN_VALUE, 0, Integer.MAX_VALUE});
		Assert.assertEquals("{Integer.MIN_VALUE, 0, Integer.MAX_VALUE}", 0, result);		
		
		result = ex.arrayCount9(new int[] {-1000, 1000, 5000});
		Assert.assertEquals("{-1000, 1000, 5000}", 0, result);		
		
		result = ex.arrayCount9(new int[] {1000, 0, -1000});
		Assert.assertEquals("{1000, 0, -1000}", 0, result);		
	}
	
	@Test
	public void arrayCount9Test_ArrayWithNines() {
		int result = ex.arrayCount9(new int[] {8,6,2,5,4,8,3,9});
		Assert.assertEquals("arrayCount9(new int[] {8,6,2,5,4,8,3,9});", 1, result);		

		result = ex.arrayCount9(new int[] {9, 8,6,2,5,4,8,3});
		Assert.assertEquals("{9, 8,6,2,5,4,8,3}", 1, result);
		
		result = ex.arrayCount9(new int[] {9});
		Assert.assertEquals("{9}", 1, result);
		
		result = ex.arrayCount9(new int[] {9,9,9,9,9,9,9,9,9});
		Assert.assertEquals("{9,9,9,9,9,9,9,9,9}", 9, result);
		
		result = ex.arrayCount9(new int[] {1,9,1});
		Assert.assertEquals("{1,9,1}", 1, result);
		
		result = ex.arrayCount9(new int[] {2,9});
		Assert.assertEquals("{2,9}", 1, result);		

		result = ex.arrayCount9(new int[] {9,1});
		Assert.assertEquals("{9,1}", 1, result);		

		result = ex.arrayCount9(new int[] {9,1,9});
		Assert.assertEquals("{9,1,9}", 2, result);		

		result = ex.arrayCount9(new int[] {1,9,9,1});
		Assert.assertEquals("{1,9,9,1}", 2, result);		
	}
	
	@Test
	public void arrayCount9Test_ParamIsNull() {
		int result = ex.arrayCount9(null);
		
		Assert.assertEquals("null", 0, result);
		
	}
	
	
	
	/*
	 * 22. Given three ints, a b c, return true if one of them is 10 or more less
	 * than one of the others. 
	 * lessBy10(1, 7, 11) → true 
	 * lessBy10(1, 7, 10) → false
	 * lessBy10(11, 1, 7) → true
	 */
	@Test
	public void lessBy10_true() {
		
		boolean result = ex.lessBy10(1, 7, 11);
		Assert.assertTrue("lessBy10(1, 7, 11)", result);
		
		result = ex.lessBy10(11, 1, 7);
		Assert.assertTrue("lessBy10(11, 1, 7)", result);

	
		result = ex.lessBy10(12, 1, 7);
		Assert.assertTrue("lessBy10(12, 1, 7)", result);

		result = ex.lessBy10(Integer.MAX_VALUE, 1, Integer.MIN_VALUE);
		Assert.assertTrue("lessBy10(Integer.MAX_VALUE, 1, Integer.MIN_VALUE)", result);
		
		result = ex.lessBy10(11, 1, 5);
		Assert.assertTrue("lessBy10(11, 1, 5)", result);
		result = ex.lessBy10(12, 1, 5);
		Assert.assertTrue("lessBy10(12, 1, 5)", result);

		result = ex.lessBy10(5, 1, 11);
		Assert.assertTrue("lessBy10(5, 1, 11)", result);

		result = ex.lessBy10(5, 1, 12);
		Assert.assertTrue("lessBy10(5, 1, 12)", result);

		result = ex.lessBy10(-5, 1, 12);
		Assert.assertTrue("lessBy10(-5, 1, 12)", result);

		result = ex.lessBy10(0, 1, 10);
		Assert.assertTrue("lessBy10(0, 1, 10)", result);
	}
	
	@Test
	public void lessBy10_false() {
		
		boolean result = ex.lessBy10(1, 7, 10);
		Assert.assertFalse("lessBy10(1, 7, 10)", result);
		
		result = ex.lessBy10(10, 1, 7);
		Assert.assertFalse("lessBy10(10, 7, 1)", result);
	}

	
	// An (arguably) cleaner way to test multiple inputs
	@Test
	public void lessBy10_true_cleaner_test() {
		
		//More inputs can be added here.  All inputs should be expected to return true.
		int[][] inputs = new int[][] {
			{Integer.MAX_VALUE, 1, Integer.MIN_VALUE},
			{Integer.MIN_VALUE, 1, Integer.MAX_VALUE},
			{Integer.MAX_VALUE, Integer.MIN_VALUE, 1},
			{Integer.MIN_VALUE, Integer.MAX_VALUE, 1},
			{1, Integer.MAX_VALUE, Integer.MIN_VALUE},
			{1, Integer.MIN_VALUE, Integer.MAX_VALUE},
			{1, 5, 11},
			{1, 5, 12},
			{11, 1, 5},
			{12, 1, 5},
			{5, 11, 1},
			{5, 12, 1},
			{11, 5, 1},
			{12, 5, 1},
			{1, 11, 5},
			{1, 12, 5},
			{5, 1, 11},
			{5, 1, 12},
			{-10, 0, 0},
			{0, -10, 0},
			{0, 0, -10},
			{-11, 0, 0},
			{0, -11, 0},
			{0, 0, -11},
			{10, 0, 0},
			{0, 10, 0},
			{0, 0, 10},
			{11, 0, 0},
			{0, 11, 0},
			{0, 0, 11}
		};
		
		for(int[] input : inputs) {
			int input1 = input[0];
			int input2 = input[1];
			int input3 = input[2];
			
			boolean result = ex.lessBy10(input1, input2, input3);
			Assert.assertTrue(String.format("lessBy10(%d, %d, %d) expected to be true but was false", input1, input2, input3), result);
		}
		
		
	}
	
	
	// An (arguably) cleaner way to test multiple inputs
	@Test
	public void lessBy10_false_cleaner_test() {
		
		//More inputs can be added here.  All inputs should be expected to return true.
		int[][] inputs = new int[][] {
			{1, 5, 10},
			{1, 5, 9},
			{10, 1, 5},
			{9, 1, 5},
			{5, 10, 1},
			{5, 9, 1},
			{10, 5, 1},
			{9, 5, 1},
			{1, 10, 5},
			{1, 9, 5},
			{5, 1, 10},
			{5, 1, 9},
			{-9, 0, 0},
			{0, -9, 0},
			{0, 0, -9},
			{-8, 0, 0},
			{0, -8, 0},
			{0, 0, -8},
			{9, 0, 0},
			{0, 9, 0},
			{0, 0, 9},
			{8, 0, 0},
			{0, 8, 0},
			{0, 0, 8}
		};
		
		for(int[] input : inputs) {
			int input1 = input[0];
			int input2 = input[1];
			int input3 = input[2];
			
			boolean result = ex.lessBy10(input1, input2, input3);
			Assert.assertFalse(String.format("lessBy10(%d, %d, %d) expected to be false but was true", input1, input2, input3), result);
		}
		
		
	}

	
}
