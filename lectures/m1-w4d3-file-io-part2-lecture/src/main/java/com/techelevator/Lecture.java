package com.techelevator;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Scanner;

public class Lecture {

	public static void main(String[] args) throws IOException {
		
		Scanner userInput = new Scanner(System.in);
		String path = "";
		
		/* 
		 * The java.io.File class is a representation of file and directory path names.  It provides methods to inspect and 
		 * modify file system objects.
		 * 
		 * One benefit is that it compensates for differences in Windows and Unix use of '/' and '\' as directory delimiters.
		 * 
		 * A new instance of File can be created from a String that contains a file system path 
		 */

		System.out.print("Enter the path of a file or directory: ");
		path = userInput.nextLine();
		File f = new File(path);		//Creates a new File object that represents a File System object (Directory or File)
		
		/* 
		 * The File class allows us to inspect various attributes of a file system object 
		 */
		 
		/* ***************************
		 * INSPECTING THE FILESYSTEM 
		 * ***************************/
			
		System.out.println();
		
		if(f.exists()) {		//Returns whether the file exists

			System.out.println("name: " + f.getName());						//Gets the file's name, without the path.
			System.out.println("absolute path: " + f.getAbsolutePath());		//Gets the absolute path of the file, including the filename
		
			if(f.isDirectory()) {						//Returns whether the File object represents a Directory
				System.out.println("type: directory");
				
				//Write out the contents of the directory
				String[] filenames = f.list();		//Get files and directories in the given directory
				for(String fname : filenames) {
					System.out.println("\t" + fname);
				}
				
				
			} else if(f.isFile()) {						//Returns whether the File object represents a File
				System.out.println("type: file");
			}
			
			System.out.println("size: " + f.length());	//Gets the size of the file
			
			
		} else {
			System.out.println(f.getAbsolutePath() + " does not exist.");
		}
		
		/* 
		 * The File class also allows us to manipulate file system objects 
		 * */
		 
		/* ************************
		 * CREATING A DIRECTORY 
		 * ************************/
		
		System.out.println();
		System.out.println("Let's create a new directory.");
		System.out.print("Enter the path of the new directory >>> ");
		path = userInput.nextLine();
		File newDirectory = new File(path);
		
		if(newDirectory.exists()) {
			System.out.println("Sorry, " + newDirectory.getAbsolutePath() + " already exists.");
//			System.exit(1);
		} else {
			if(newDirectory.mkdir()) {		//Creates a new directory on the filesystem with the absolute path passed to the File object's constructor
				System.out.println("New directory created at " + newDirectory.getAbsolutePath());
			} else {
				System.out.println("Could not create directory.");
				System.exit(1);
			}
		}
		
		
		/* ************************
		 * CREATING A FILE 
		 * ************************/
		
		System.out.println();
		System.out.println("Now let's put a file in the directory.");
		System.out.print("Enter a name for the new file >>> ");
		String fileName = userInput.nextLine();

		File newFile = new File(newDirectory, fileName);

		newFile.createNewFile();		//Physically creates a file on the filesystem 
		
		System.out.println();
		System.out.println("name: " + newFile.getName());
		System.out.println("absolutePath: " + newFile.getAbsolutePath());
		System.out.println("size : " + newFile.length());

		/* ************************
		 * WRITING TO A FILE 
		 * ************************/
		
		System.out.println();
		System.out.println("Now let's write something in the new file.");
		System.out.print("Enter a message to be stored in the new file >>> ");
		String message = userInput.nextLine();


		try(PrintWriter writer = new PrintWriter(newFile)) {
		
			writer.println(message);			//Write data to the file buffer.  It will be written to the file when the writer is closed.
		
		}	// When we exit the try block, this cause the file to be closed and an automatic flush of the buffer to trigger
		//writer.close();				//We don't need to close the writer manually if using the try pattern above

		
		System.out.println();
		System.out.println("name: "+newFile.getName());
		System.out.println("absolutePath: "+newFile.getAbsolutePath());
		System.out.println("size : "+newFile.length());

		
		/* ************************
		 * APPENDING TEXT TO A FILE 
		 * ************************/

		//These examples are from here: https://stackoverflow.com/questions/1625234/how-to-append-text-to-an-existing-file-in-java
				
		//This shouldn't be NEEDED to do today's exercises.

		System.out.println();
		System.out.println("Now let's add something to the edited file.");
		System.out.print("Enter a message to be stored in the new file >>> ");
		message = userInput.nextLine();
		
//		//METHOD 1:
//		try { 
//			Files.write(Paths.get(newFile.getAbsolutePath()), message.getBytes(), StandardOpenOption.APPEND);
//		} catch(IOException ex) {
//			//Do something in case the write fails
//		}
		
		//METHOD 2:
		try(FileWriter fw = new FileWriter(newFile, true);
			BufferedWriter bw = new BufferedWriter(fw);
			PrintWriter pw = new PrintWriter(bw);) {
			
			pw.println(message);
						
		} catch(IOException ex) {
			//Do something in case there's an error
		}


		
	}

}
