//1. Getting an HTML element by id
let $box1 = $("#box1");

//2. Getting an HTML element by class name
let $redElements = $(".red");

//3. Getting HTML elements by tag name
let $divs = $("div");

//4. Filtering to only include items with the blue class.
let $blueDivs = $divs.filter(".blue");

//5. Use not to exclude items with the blue class.
let $nonblueDivs = $divs.not(".blue");

//6. Find parent of box1
let $box1Parent = $box1.parent();

//7. Find children of the section
let $sectionChildren = $("section").children();

//8. Find last child of the section
let $lastChild = $sectionChildren.last();


//9. Add the grow class to box1
function growBox1() {
    $box1.addClass("grow");
}

//Find and shrink the blue box
function shrinkBlueBox() {
    $(".blue.box").addClass("shrink");
}

//Remove all elements inside divs
function removeStuff() {
    $divs.empty();
}

//Detach and reattach divs
let $detachedDivs;
function detachDivs() {
    $detachedDivs = $("div").detach();
}
function reattachDivs() {
    $("section").append($detachedDivs);
}

//Change text of paragraphs
function changeText(message) {
    $(".box p").text(message);
}

//Make list items bold
function makeBold() {
    $("li").css("font-weight", "bold");
}

//Add shadow to last box
function addShadowToLastBox() {
    $(".box").last().addClass("shadow");
}

function toggleShadow(selector) {
    $(selector).toggleClass("shadow");
}

function makeYellowTextboxes () {
    $("input[type='text']").addClass("yellow");
}

function setTextboxValues () {
    $("input[type='text']").val("Hello there");
}

//Appending a list item
function addNewListItem() {
    //Find the list in box 3
    let $ul = $("#box3 ul");
    //Get the count of children of that list
    let count = $ul.children().length;
    //Create a new list item
    let $li = $("<li>").text(`Item ${count + 1}`);
    //Append the new list item to the list
    $ul.append($li);
}

//Add a new checkbox and textbox
function addNewItems() {
    let htmlToCopy = $("#templateItem").html();
    let newDiv = $(htmlToCopy);
    $("#box6").append(newDiv);
}


function disappear() {
    $(".box").fadeOut(1000);
}
