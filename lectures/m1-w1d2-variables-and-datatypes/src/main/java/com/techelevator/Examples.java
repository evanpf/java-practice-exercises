package com.techelevator;

public class Examples {

	public static void main(String[] args) {
		
		// Variable declaration
		int x;
		
		// Assignment statement
		x = 3;
		
		// We can also declare and assign in one statement
		int y = 5;		
		
		// variable names should start with a lower case letter and use camelCase
		int ageOfAdulthood = 18;

	
		// literal values of type char appear between single quotes
		char theFirstLetter = 'a';
		
		
		// use floating point types like double to represent decimal values
		double averageNumberOfChildren = 2.5;
		
		// three different types of assignment statements
		// 1) assigning a literal
		x = 2;		
		
		// 2) assigning from another variable
		x = y;
		
		// 3) assigning the result of an expression
		y = x + ((3 * 4) / 6);
	
		
		
		// ******************
		// LITERALS
		// ******************
		
		int myInt = 400;
		byte myByte = 3;
		
		byte something = (byte)myInt;	//Overflows to -112
		
		//myByte = myInt; //couses a compile-time error because thi int variable is capable of holding values that won't fit in a byte.

		myInt = myByte;
		
		// Hexadecimal literals are preceded by 0x
		int twentySix = 0x1A;
		
		// String literals appear between double quotes
		String firstName = "Bob";
		String lastName = "Jones";
		
		String fullName = firstName + " " + lastName;
//		System.out.println(firstName);
//		System.out.println(lastName);
//		System.out.println(fullName);

		
		int numberOfDotNetStudents = 16;
		int numberOfJavaStudents = 15;
		int totalNumberOfStudents = numberOfDotNetStudents + numberOfJavaStudents;
		
//		System.out.println("Total number of students: " + totalNumberOfStudents);
		
		
		
		// **********************
		// TRUNCATION AND CASTING
		// **********************
	
		int cookiesEaten = 10;
		int numberOfChildrenEatingCookies = 6;
		float averageCookiesEaten = cookiesEaten / numberOfChildrenEatingCookies;
		
		System.out.println("Average cookies eaten: " + averageCookiesEaten);
		
		averageCookiesEaten = (float)cookiesEaten / numberOfChildrenEatingCookies;
		
		System.out.println("Average cookies eaten (with casting): " + averageCookiesEaten);
	
		double aDouble = 7.89;
//		int anInteger = aDouble;		//This is a compiler error
		int anInteger = (int)aDouble;
		System.out.println(aDouble + " cast as an int is equal to: " + anInteger);
		
		// **********************
		// ARITHMETIC OPERATORS
		// **********************
		
		int a = 12;
		System.out.println("What happens to b?");
		int b = a + 3;
		System.out.println(b);
		b = a - 5;
		System.out.println(b);
		b = a * 2;
		System.out.println(b);
		b = a / 2;
		System.out.println(b);

		int remainder = 10 % 3;
		System.out.println("The remainder of 10 / 3: " + remainder);
		
		remainder = 8 % 4;
		System.out.println("The remainder of 8 / 4: " + remainder);
		
		
		
		// ***********************
		// ESCAPE CHARACTERS
		// ***********************

		String bobQuote = "Bob said, \"Bam!\"";
		System.out.println(bobQuote);
		
		System.out.println("To print \\ I need two backslashes.");
		
		System.out.println("Hello!\n\nGoodbye!");	// Use \n to print a new line
		
		System.out.println("Hello\tGoodbye");		// Use \t to print a tab character
		
	}

}