package com.techelevator;

public class Lecture {

	public static void main(String[] args) {
		
		/*
		1. Create a variable to hold an int and call it numberOfExercises.
			Then set it to 26.
		*/

		int numberOfExercises = 26;
		
//		numberOfExercises -= 1;
//		numberOfExercises++;
		
		
		/*
		int numberOfExercises;
		numberOfExercises = 26;
		 */
		System.out.println(numberOfExercises);

		/*
		2. Create a variable to hold a double and call it half.
			Set it to 0.5.
		*/

		double half = 0.5;
		System.out.println(half);

		/*
		3. Create a variable to hold a String and call it name.
			Set it to "TechElevator".
		*/

		String name = "TechElevator";
		
		System.out.println(name);

		/*
		4. Create a variable called seasonsOfFirefly and set it to 1.
		*/

		int seasonsOfFirefly = 1;
		System.out.println(seasonsOfFirefly);

		/*
		5. Create a variable called myFavoriteLanguage and set it to "Java".
		*/

		String myFavoriteLanguage = "Java";
		System.out.println(myFavoriteLanguage);

		/*
		6. Create a variable called pi and set it to 3.1416.
		*/

		float pi = 3.14159265358f;
		System.out.println(pi);

		/*
		7. Create and set a variable that holds your name.
		*/

		String myName = "Aaron";
		
		/*
		8. Create and set a variable that holds the number of buttons on your mouse.
		*/

		byte buttons = 0;
		
		/*
		9. Create and set a variable that holds the percentage of battery left on
		your phone.
		*/
		
		double batteryPercent = 0.89;

		/*
		10. Create an int variable that holds the difference between 121 and 27.
		*/
		
		int difference = 121 - 27;

		/*
		11. Create a double that holds the addition of 12.3 and 32.1.
		*/
		
		double sum = 12.3 + 32.1;

		/*
		12. Create a String that holds your full name.
		*/
		
		String fullName = "Aaron Tyler";

		/*
		13. Create a String that holds the word "Hello, " concatenated onto your
		name from above.
		*/
		
		String hello = "Hello, " + fullName;

		/*
		14. Add a " Esquire" onto the end of your full name and save it back to
		the same variable.
		*/
		
//		fullName = fullName + " Esquire";

		/*
		15. Now do the same as exercise 14, but use the += operator.
		*/

		fullName += " Esquire";
		
		/*
		16. Create a variable to hold "Saw" and add a 2 onto the end of it.
		*/

		String movieTitle = "Saw " + 2;
		
		
		/*
		17. Add a 0 onto the end of the variable from exercise 16.
		*/

		movieTitle += 0;
		
		/*
		18. What is 4.4 divided by 2.2?
		*/

		double quotient1 = 4.4 / 2.2; 
		System.out.println(quotient1);
		
		/*
		19. What is 5.4 divided by 2?
		*/
		
		float quotient2 = 5.4f / 2f;
		System.out.println(quotient2);

		/*
		20. What is 5 divided by 2?
		*/

		float quotient3 = 5f / 2;
		System.out.println(quotient3);
		
		/*
		21. What is 5.0 divided by 2?
		*/
		
		double quotient4 = 5.0 / 2;
		System.out.println(quotient4);
		
		/*
		22. What is 66.6 divided by 100? Is the answer you get right or wrong?
		*/
		
		double quotient5 = 66.6 / 100.0;
		quotient5 *= 10000000;
		System.out.println(quotient5);
		

		
		/*
		23. If I divide 5 by 2, what's my remainder?
		*/

		int remainder = 5 % 2;
		System.out.println("Remainder of 5 / 2: " + remainder);
		
		
		/*
		24. What is 1,000,000,000 * 3?
		*/

		long bigNumber = 1000000000L * 3;
		System.out.println("Big Number: " + bigNumber);
		
		
		/*
		25. Create a variable that holds a boolean called doneWithExercises and
		set it to false.
		*/

		boolean doneWithExercises = false;
		System.out.println("Done with Exercises: " + doneWithExercises);
		
		
		/*
		26. Now set doneWithExercise to true.
		*/

		doneWithExercises = true;
		System.out.println("Done with Exercises 2: " + doneWithExercises);

	}

}
