

//Use a function to create a constructor that creates a new object
function Person(firstName, lastName, age, children) {

    //Create the new Object
    let newPerson = new Object();

    //Set the paramaters
    newPerson.firstName = firstName;
    newPerson.lastName = lastName;
    newPerson.age = age;
    newPerson.children = children;

    //Create the methods
    newPerson.toString = function() {
        return `${this.firstName} ${this.lastName} (${this.age})`;
    }

    //return the newly-created Object
    return newPerson;

}

//This is exactly the same as the `Person` function above
function PersonLiteral(firstName, lastName, age, children) {
    return {
        firstName: firstName,
        lastName: lastName,
        age: age,
        children: children,
        toString: function() {
            return `${this.firstName} ${this.lastName} (${this.age})`;
        }
    }
}






//We're going to use these variables in the example
let george;
let newPerson;

    //Create a new Object
    george = new Object();              // george = {}

    //Set the properties
    george.firstName = "George";
    george.lastName = "Frederickson";
    //Can use `new` with Array to create a new array, or just use `[]`
    george.children = new Array();      //george.children = [];
    //Set values of the array
    george.children[0] = "Fred";
    george.children[1] = "Ginger";

    //Can also do it this way:
    //george.children = ["Fred", "Ginger"];

    //Properties/methods can also be accessed by key name
    george["age"] = 21;

    //Values can also be objects
    george["address"] = new Object();
    george["address"].line1 = "123 Main Street";
    george["address"].city = "Anytown";
    george["address"].state = "OH";
    george["address"].zip = "12215";

    //Use anonymous functions to create methods
    george.toString = function() {
        return `${this.firstName} ${this.lastName} (${this.age})`;
    }


    let fred = new Object();
    fred.firstName = "Fred";
    fred.toString = function() {
        return `${this.firstName} ${this.lastName} (${this.age})`;
    }


    //You can create a "class"-like construct by defining a function to create the object
    //Call the function-constructor to create a new object.
    let aaron = new Person("Aaron", "Tyler", 25, ["Caspian", "Zepnan"]);
    //Could also do: let aaron = Person(...);

    console.log(aaron.toString());


    //Use for..in to loop through all keys in an object
    for(let key in george) {

        console.log("Key: " + key);

        if(typeof george[key] !== "function") {
            if(typeof george[key] === "object") {

                for (let innerKey in george[key])
                {
                    console.log(`${key} : ${typeof george[key][innerKey]} : ${george[key][innerKey]}`);
                }


            } else {
                console.log("Value: ", george[key]);
            }
        } else {
            console.log("Value is a function");
        }

    }


    //Use `typeof` to get the type of an object
    let numberString = "5";
    let numberNum = 10;
    let numberCalc = numberString * numberNum;

    console.log("numberString", typeof numberString, numberString);
    console.log("numberNum", typeof numberNum, numberNum);
    console.log("numberCalc", typeof numberCalc, numberCalc);
    

//Usually, objects are created using object literals
newPerson = {
    firstName: "George",
    lastName: "Frederickson",
    age: 21,
    children: [
        "Fred",
        "Ginger"
    ],
    address: {
        line1: "123 Main Street",
        city: "Anytown",
        state: "OH",
        zip: "12255"
    },
    toString: function() {
        return `${this.firstName} ${this.lastName} (${this.age})`;
    }
}
    







