﻿

$(function () {




    //Validation can happen when the form is submitted:
    $("#applicationForm").submit(function(e) {

        $(".error").removeClass("error");

        let isValid = true;

        //check at each field for required
        isValid = validateRequired("fName") && isValid;
        isValid = validateRequired("lName") && isValid;
        isValid = validateRequired("emailAddr") && isValid;
        isValid = validateRequired("password") && isValid;
        isValid = validateRequired("verifyPassword") && isValid;

        if(!isValid) {
            console.log("Form NOT Submitted.");
            e.preventDefault();
            return false;

        } else {
            console.log("Form Submitted.");
        }
    });


    //Validation can happen on blur
    $("input[name='lName']").on("blur", function(e) {
        const elem = $(this);
        if(elem.val() == null || elem.val().trim() == "") {
            $("label[for='lName']").addClass("error");
        }
    });
    $("input[name='lName']").on("focus", function(e) {
        $("label[for='lName']").removeClass("error");
    });



});
 
function validateRequired(fieldName) {
    const inputSelector = "input[name='" + fieldName + "']";
    const labelSelector = "label[for='" + fieldName + "']";
    if($(inputSelector).val() == null || $(inputSelector).val().trim() == "") {
        $(labelSelector).addClass("error");
        return false;
    }

    return true;

}

