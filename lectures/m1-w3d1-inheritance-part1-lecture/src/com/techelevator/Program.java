package com.techelevator;

import java.util.ArrayList;
import java.util.List;

public class Program {

	public static void main(String[] args) {
		
		Auction shoesAuction = new Auction("Joe's Shoes");
		
		//Joe wants $500
		
		Bid theBid = new Bid("Nick", 10);
		shoesAuction.placeBid(theBid);
		printAuctionBidder(shoesAuction);
		
		shoesAuction.placeBid(new Bid("Rob", 35));
		printAuctionBidder(shoesAuction);
		
		shoesAuction.placeBid(new Bid("Nick", 40));
		printAuctionBidder(shoesAuction);

		shoesAuction.placeBid(new Bid("Rob", 37));
		printAuctionBidder(shoesAuction);

		shoesAuction.placeBid(new Bid("Eric", 500));
		printAuctionBidder(shoesAuction);
		
		shoesAuction.endAuction();
		printAuctionBidder(shoesAuction);
		
		// *************************************************
		
		System.out.println("*************************************");
				
		Auction shoesReserveAuction = new ReserveAuction("Joe's expensive shoes", 50);

		shoesReserveAuction.placeBid(new Bid("Tyler", 35));
		shoesReserveAuction.placeBid(new Bid("Matthew", 40));
		shoesReserveAuction.placeBid(new Bid("Matt", 48));

		shoesReserveAuction.endAuction();
		printAuctionBidder(shoesReserveAuction);
		
		
		Auction boAuction = new BuyoutAuction("Eric's Glasses", 5);
		
		boAuction.placeBid(new Bid("Hannah", 1));
		boAuction.placeBid(new Bid("Joe", 2));
		boAuction.placeBid(new Bid("Aaron", 5));
		
		printAuctionBidder(boAuction);
		
		System.out.println("*************************************");
		System.out.println("*************************************");
		System.out.println("*************************************");
		
		
		List<Auction> auctionList = new ArrayList<Auction>();
		for(int i = 0; i < 10; i++) {
			Auction curAuction;
			String itemName = "Item " + i;
			int auctionType = (int)((Math.random() * 3) + 1);
//			System.out.println(auctionType);
			if(auctionType == 1) {
				curAuction = new Auction(itemName);
			} else if(auctionType == 2) {
				curAuction = new ReserveAuction(itemName, 25);
			} else {
				curAuction = new BuyoutAuction(itemName, 12);
			}
			
			for(int j = 0; j < 10; j++) {
				String bidder = "Bidder " + j;
				curAuction.placeBid(new Bid(bidder, 2*j));
			}
			
			auctionList.add(curAuction);
		}

		for(Auction a : auctionList) {
			a.endAuction();
			
			if(a instanceof Auction) {
				System.out.println("A is instance of Auction.");
			}
			if(a instanceof ReserveAuction) {
				ReserveAuction ra = (ReserveAuction)a;
				System.out.println("A is instance of ReserveAuction with a reserve of: " + ra.getReserveAmount());
			}
			if(a instanceof BuyoutAuction) {
				System.out.println("A is instance of BuyoutAuction.");
			}

			
			printAuctionBidder(a);
		}
	}
	
	public static void printAuctionBidder(Auction a) {
		
		String activeString = "Active";
		if(a.isAuctionActive() == false) {
			activeString = "Inactave";
		}
		
		System.out.println("Current High Bid for " 
						   + activeString + " " 
						   + a.getAuctionType() 
						   + " Auction: " + a.getItemForSale() 
						   + ": " 
						   + a.getHighBid().getBidder() 
						   + " / " 
						   + a.getHighBid().getBidAmount());
	 }
	
}
