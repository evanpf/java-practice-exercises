package com.techelevator;

public class BuyoutAuction extends Auction {

	public static String AuctionType = "Buyout";
	private int buyoutPrice;
	
	public BuyoutAuction(String itemForSale, int buyoutPrice) {
		super(itemForSale);
		this.buyoutPrice = buyoutPrice;
	}
	
	@Override
	public String getAuctionType() {
		return "Buyout";
	}

	public int getBuyoutPrice() {
		return this.buyoutPrice;
	}

	@Override
	public boolean placeBid(Bid offeredBid) {

		boolean isCurrentWinningBid = super.placeBid(offeredBid);
		
		if(offeredBid.getBidAmount() >= buyoutPrice) {
			this.isActive = false;
		}
		
		return isCurrentWinningBid;
		
	}

	
	
}
