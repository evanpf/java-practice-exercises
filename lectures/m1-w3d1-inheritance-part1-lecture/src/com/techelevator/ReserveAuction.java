package com.techelevator;

public class ReserveAuction extends Auction {

	public static String AuctionType = "Reserve";
	private int reserveAmount;

	
	public ReserveAuction(String itemForSale, int reserveAmount) {
		super(itemForSale);
		this.reserveAmount = reserveAmount;
	}
	
	@Override
	public String getAuctionType() {
		return "Reserve";
	}
	
	public int getReserveAmount() {
		return this.reserveAmount;
	}

	@Override
	public Bid endAuction() {
		isActive = false;

		if(isReserveMet() == false) { // Alternatively, could use 			if(!isReserveMet()) {...}
			currentHighBid = new Bid("Reserve Not Met", 0);
		}

		return currentHighBid;

	}

	
	
	public boolean isReserveMet() {
		if(currentHighBid.getBidAmount() >= reserveAmount) {
			return true;
		}
		return false;
	}
	
}
