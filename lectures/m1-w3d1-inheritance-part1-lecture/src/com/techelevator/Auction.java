package com.techelevator;

import java.util.ArrayList;
import java.util.List;

public class Auction {

	protected String itemForSale;
	protected Bid currentHighBid = new Bid("", 0);
	protected List<Bid> allBids = new ArrayList<Bid>();
	protected boolean isActive = true;
	
	
	public Auction(String itemForSale) {
		this.itemForSale = itemForSale;
		this.currentHighBid = new Bid("", 0);
		isActive = true;
		allBids = new ArrayList<Bid>();
	}

	public String getAuctionType() {
		return "Auction";
	}
	
	public boolean placeBid(Bid offeredBid) {
		
		if(!isActive) {
			return false;
		}
		
		allBids.add(offeredBid);
		boolean isCurrentWinningBid = false;
		
		if(offeredBid.getBidAmount() > currentHighBid.getBidAmount()) {
			currentHighBid = offeredBid;
			isCurrentWinningBid = true;
		}
		return isCurrentWinningBid;		
	}

	public Bid endAuction() {
		isActive = false;
		return currentHighBid;
	}
	
	public boolean isAuctionActive() {
		return isActive;
	}
	
	public Bid getHighBid() {
		return this.currentHighBid;
	}
	
	public List<Bid> getAllBids() {
		return new ArrayList<Bid>(allBids);
	}
	
	public String getItemForSale() {
		return this.itemForSale;
	}
	
}
