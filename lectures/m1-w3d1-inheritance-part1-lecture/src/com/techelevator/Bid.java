package com.techelevator;

public class Bid {

	private String bidder;
	private int bidAmount;
	
	public Bid(String bidder, int bidAmount) {
		this.bidder = bidder;
		this.bidAmount = bidAmount;
	}
	
	public String getBidder() {
		return this.bidder;
	}
	
	public int getBidAmount() {
		return this.bidAmount;
	}
	
}
