package com.techelevator.paintCalculator;

public class Wall {

	private int length = 0;
	private int height = 0;

	
	
	
	public Wall(int length, int height) {
		setLength(length);
		setHeight(height);
	}

	public Wall(int squareSize) {
		setLength(squareSize);
		setHeight(squareSize);
	}
	
	public int getLength() {
		return length;
	}
	
	public void setLength(int length) {
		if(length < 0) {
			this.length = 0;
		} else {
			this.length = length;
		}
	}
	
	public int getHeight() {
		return height;
	}
	
	public void setHeight(int height) {
		if(height < 0) {
			this.height = 0;
		} else {
			this.height = height;
		}
	}
	
	public int getArea() {
		
		return length * height;
		
	}
	
	public boolean isLargerThan(Wall otherWall) {
		
		int otherArea = otherWall.getArea();
		
		return otherArea < getArea();
	}

	public boolean isLargerThan(int length, int height) {

		Wall otherWall = new Wall(length, height);

		//return otherWall.getArea() < getArea();
		return isLargerThan(otherWall);
		
		
		
	}
	
	
	public boolean equals(Wall otherWall) {
		
		boolean lengthEqual = length == otherWall.getLength(); 
		boolean heightEqual = height == otherWall.getHeight();

		return lengthEqual && heightEqual;
		
	}
	

	public String toString() {
		return "Length: " + length + "\nHeight: " + height;
	}
	
	
}
