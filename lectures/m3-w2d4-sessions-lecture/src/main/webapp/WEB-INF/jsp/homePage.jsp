<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<title>Model and Session Test</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>
<body>

	<div class="container" style="margin-top: 50px;" id="container">

		<table class="table table-striped" style="background-color: white;">
			<tr>
				<th>Current DateTime:</th>
				<td><%=(new java.util.Date()).toLocaleString()%></td>
			</tr>
			<tr>
				<th>HttpSession Value (Session Scope):</th>
				<td>${sessionScope.headerText}</td>
			</tr>
			<tr>
				<th>ModelMap Value (Request Scope):</th>
				<td>${requestScope.headerText}</td>
			</tr>
			<tr>
				<th>Value without specifying scope:</th>
				<td>${headerText}</td>
			</tr>

			<c:url var="urlBase" value="/" />

 			<tr>
				<th colspan="2"><a href="${urlBase}setModelMap">Set
						ModelMap Value to "Request Scope"</a></th>
			</tr>

 			<tr>
				<th colspan="2"><a href="${urlBase}setHttpSession">Set
						HttpSession Value to "Session Scope"</a></th>
			</tr>
 
  			<tr>
				<th colspan="2"><a href="${urlBase}clearHttpSession">Clear
						HttpSession Value</a></th>
			</tr>
   			<tr>
				<th colspan="2"><a href="${urlBase}invalidateHttpSession">Invalidate
						Session</a></th>
			</tr>
   			<tr>
				<th colspan="2"><a href="${urlBase}setRequestScopeWithRedirect">Set Model attribute with redirect</a></th>
			</tr> 
 			<tr>
				<th colspan="2"><a href="${urlBase}setFlashScopeWithRedirect">Set Flash Scope attribute with redirect</a></th>
			</tr>


 			<tr>
				<th colspan="2"><a href="${urlBase}homePage">Go to home page</a></th>
			</tr>

		</table>

	</div>
	<script>
		//This just sets the background color to a random color so it's easy to see when the page is reloaded.
		function random_bg_color() {
			var x = Math.floor(Math.random() * 256);
			var y = Math.floor(Math.random() * 256);
			var z = Math.floor(Math.random() * 256);
			var bgColor = "rgb(" + x + "," + y + "," + z + ")";
			console.log(bgColor);

			document.getElementById("container").style.background = bgColor;
		}

		random_bg_color();
	</script>

</body>
</html>