<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Validation Page</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">

</head>
<body>

	<div class="container" style="margin-top: 50px;">

		<h1>Form submission complete!</h1>

			<table class="table">
				<tr>
					<th>First Name:</th>
					<td>${firstName}</td>
				</tr>
				<tr>
					<th>Last Name:</th>
					<td>${lastName}</td>
				</tr>
				<tr>
					<th>Phone Number:</th>
					<td>${phone}</td>
				</tr>
				<tr>
					<th>Email Address:</th>
					<td>${email}</td>
				</tr>
				<tr>
					<th>Message:</th>
					<td>${message}</td>
				</tr>

			</table>

	</div>

</body>
</html>