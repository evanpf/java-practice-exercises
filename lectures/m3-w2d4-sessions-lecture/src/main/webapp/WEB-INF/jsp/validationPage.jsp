<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Validation Page</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">

</head>
<body>

	<div class="container" style="margin-top: 50px;">


		<h1>Validation Page</h1>

		<c:url var="formUrl" value="/postForm" />
		<form method="POST" action="${formUrl}">

			<table class="table">

				<c:if test="${not empty errorMessages}">
					<tr>
						<c:forEach var="message" items="${errorMessages}">
						<div class="alert alert-danger" role="alert">
							${message}
						</div>
						</c:forEach>
					</tr>
				</c:if>

				<tr>
					<th>First Name:</th>
					<td><input type="text" name="firstName" value="${firstName}" class="form-control" /></td>
				</tr>
				<tr>
					<th>Last Name:</th>
					<td><input type="text" name="lastName" value="${lastName}" class="form-control" /></td>
				</tr>
				<tr>
					<th>Phone Number:</th>
					<td><input type="text" name="phone" value="${phone}" class="form-control" /></td>
				</tr>
				<tr>
					<th>Email Address:</th>
					<td><input type="text" name="email" value="${email}" class="form-control" /></td>
				</tr>
				<tr>
					<th>Message:</th>
					<td><textarea name="message" class="form-control">${message}</textarea></td>
				</tr>
				<tr>
					<th colspan="2"><input type="submit"
						class="btn btn-primary btn-block" /></th>
			</table>




		</form>







	</div>

</body>
</html>