package com.techelevator.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class ValidationController {

	@RequestMapping("/")
	public String showForm() {
		return "validationPage";
	}
	
	@RequestMapping(path="/postForm", method=RequestMethod.POST)
	public String postForm(
							RedirectAttributes redirAttribs,
							@RequestParam String firstName,
							@RequestParam String lastName,
							@RequestParam String phone,
							@RequestParam String email,
							@RequestParam String message			
						) {
		
		//Validate the input...	
		boolean isValid = true;
		List<String> errorMessages = new ArrayList<String>();
		
		//First Name is required
		if(firstName.isEmpty()) {
			//No first name!  ERROR!
			isValid = false;
			errorMessages.add("First Name is required.");
		}
		
		
		
		//Last Name can only use these characters: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-' "
		String validChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-' ";
		if(!usesValidChars(lastName, validChars)) {
			isValid = false;
			errorMessages.add("Last name contains invalid characters.  Only letters, dash, apostrophe, and space are allowed.");

		}
		
		//Check Phone Number only uses "() 123456789-"
		if(!usesValidChars(phone, "() 123456789-")) {
			isValid = false;
			errorMessages.add("Phone should be in the format: (234) 555-1234");
		}
		
		//Store values in RedirectAttributes so they're accessible after the redirect
		redirAttribs.addFlashAttribute("firstName", firstName);
		redirAttribs.addFlashAttribute("lastName", lastName);
		redirAttribs.addFlashAttribute("phone", phone);
		redirAttribs.addFlashAttribute("email", email);
		redirAttribs.addFlashAttribute("message", message);	
		
		if(!isValid) {
			redirAttribs.addFlashAttribute("errorMessages", errorMessages);
			return "redirect:/";
		}
		
		return "redirect:/postResult";
	}

	private boolean usesValidChars(String value, String validChars) {
		boolean hasInvalidChars = false;
		for(int i = 0; i < value.length() && !hasInvalidChars; i++) {
			if(validChars.indexOf(value.substring(i, i + 1)) < 0) {
				hasInvalidChars = true;
			}
		}
		return !hasInvalidChars;
	}
	
	@RequestMapping("/postResult")
	public String postResult() {
		
		
		
		return "validationResult";
	}
	

	
	
}
