package com.techelevator.controller;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class HomeController {

	@RequestMapping("/homePage")
	public String displayHomePage() {
		return "homePage";
	}

	@RequestMapping("/setModelMap")
	public String setModelValue(ModelMap map) {
		map.put("headerText", "Request Scope");
		return "homePage";
	}

	// By injecting HttpSession into our method, we can store session variables
	@RequestMapping("/setHttpSession")
	public String setSession(HttpSession session) {

		// Use .setAttribute on the session to save a value to the session
		session.setAttribute("headerText", "Session Scope");
		return "homePage";

	}

	// By injecting HttpSession into our method, we can access and manipulate
	// Session Variables
	@RequestMapping("/clearHttpSession")
	public String clearSession(HttpSession session) {

		// This removes the headerText value from the session variable map
		session.removeAttribute("headerText");
		return "homePage";

	}

	// By injecting HttpSession into our method, we can access and manipulate
	// Session
	@RequestMapping("/invalidateHttpSession")
	public String invalidateSession(HttpSession session) {
		// Use .invalidate() to remove all data from a session and assign a new
		// JSESSIONID
		session.invalidate();
		return "homePage";
	}

	//Inject ModelMap to set Request Scope variables
	@RequestMapping("/setRequestScopeWithRedirect")
	public String clearSession(ModelMap map) {
		// THIS DOESN'T WORK! - When redirecting, a new request is made, so the ModelMap no longer has the request-scope variables that were set
		map.put("headerText", "Request Scope (redirect)");
		return "redirect:/";
	}

	//Injecting RedirectAttributes allows us to set values into the requestScope after the redirect
	@RequestMapping("/setFlashScopeWithRedirect")
	public String clearSession(RedirectAttributes redirAttr) {
		//Use .addFlashAttribute(...) to set a value you want to access after a redirect
		redirAttr.addFlashAttribute("headerText", "Request Scope (redirect)");
		return "redirect:/";
	}

}
