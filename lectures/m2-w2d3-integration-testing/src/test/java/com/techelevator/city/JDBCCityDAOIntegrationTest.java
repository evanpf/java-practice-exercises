package com.techelevator.city;

import static org.junit.Assert.*;

import java.sql.SQLException;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;

public class JDBCCityDAOIntegrationTest {

	private static final String TEST_COUNTRY = "XYZ";
	
	/* Using this particular implementation of DataSource so that
	 * every database interaction is part of the same database
	 * session and hence the same database transaction */
	//TODO 1. Create shared variables needed to do the tests, such as the data source and DAO
	private static SingleConnectionDataSource dataSource;
	private JDBCCityDAO dao;



	/* Before any tests are run, this method initializes the datasource for testing. */
	@BeforeClass
	public static void setupDataSource() {

		//TODO 2. Set up our data source that will be used for the integration test

		dataSource = new SingleConnectionDataSource();
		dataSource.setUrl("jdbc:postgresql://localhost:5432/world");
		dataSource.setUsername("postgres");
		dataSource.setPassword("postgres1");
		
		/* The following line disables autocommit for connections 
		 * returned by this DataSource. This allows us to rollback
		 * any changes after each test */
		dataSource.setAutoCommit(false);
	}

	
	/* After all tests have finished running, this method will close the DataSource */
	@AfterClass
	public static void closeDataSource() {
		//TODO 3. Close our connection
		dataSource.destroy();
	}
	
	
	@Before
	public void setup() {
		//TODO 4. Set up our DAO
		this.dao = new JDBCCityDAO(dataSource);
		
		//TODO 5. Execute any SQL needed to set up each test
		String sqlInsertCountry = "INSERT INTO country (code, name, continent, region, surfacearea, indepyear, population, lifeexpectancy, gnp, gnpold, localname, governmentform, headofstate, capital, code2) VALUES (?, 'Afghanistan', 'Asia', 'Southern and Central Asia', 652090, 1919, 22720000, 45.9000015, 5976.00, NULL, 'Afganistan/Afqanestan', 'Islamic Emirate', 'Mohammad Omar', 1, 'AF')";
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		jdbcTemplate.update(sqlInsertCountry, TEST_COUNTRY);

	}
	
	
	@After
	public void teardown() throws SQLException {
		//TODO 6. Rollback our transaction after each test completes.
		dataSource.getConnection().rollback();
	}
	
	
	
	//TODO 7. Do the tests.
	@Test
	public void saveNewCityAndReadItBack() throws SQLException {
		//TODO I1. Save test
		
		//TODO I2. Create the object to be saved
				//Extracted several lines to new getCity private method to reduce duplicated code
		City theCity = getCity("SQL Station", "South Dakota", TEST_COUNTRY, 65535);

		//	Once we used this code twice, we decided to move it to a private method
		//		City theCity = new City();
		//		theCity.setName("SQL Station");
		//		theCity.setDistrict("South Dakota");
		//		theCity.setCountryCode(TEST_COUNTRY);
		//		theCity.setPopulation(65535);
		
		//TODO I3. Use the DAO to save the city
		dao.save(theCity);
		
		//TODO I4. Use the DAO to get the city based on the ID that was created in step I3
		City savedCity = dao.findCityById(theCity.getId());
		
		//TODO I5. Assert that things are what we expect
		assertNotEquals("ID Check", null, theCity.getId());

			//Extracted several lines of code to new assertCitiesAreEqual() private method to reduce duplicated code
		assertCitiesAreEqual(theCity, savedCity);
	
	}

	@Test
	public void returnsCitiesByCountryCode() {
		
		//TODO S1. Add the data we want to test our select with
		City theCity = getCity("SQL Station", "South Dakota", TEST_COUNTRY, 65535);
		dao.save(theCity);
		
		//TODO S2. Test our READ method
		List<City> results = dao.findCityByCountryCode(TEST_COUNTRY);
		
		//TODO S3. Assert that things are what we expect
		assertNotNull("Checking Results exist", results);
		assertEquals("Checking result set size", 1, results.size());
		
		City savedCity = results.get(0);
				//Extracted several lines of code to new assertCitiesAreEqual() private method to reduce duplicated code
		assertCitiesAreEqual(theCity, savedCity);
		
	}
	
	@Test
	public void returnMultipleCitiesByCountryCode() {
		
		//TODO A1. Add the data we want to test our select with
		dao.save(getCity("SQL Station", "South Dakota", TEST_COUNTRY, 65535));
		dao.save(getCity("Postgres Point", "North Dakota", TEST_COUNTRY, 65535));
		
		//TODO A2. Test our READ method
		List<City> results = dao.findCityByCountryCode(TEST_COUNTRY);
		
		//TODO A3. Assert that things are what we expect
		assertNotNull(results);
		assertEquals(2, results.size());
	}

	@Test
	public void returnCitiesByDistrict() {
		
		//TODO B1. Add the data we want to test our select with
		String testDistrict = "Tech Elevator";
		City theCity = getCity("SQL Station", testDistrict, TEST_COUNTRY, 65535);
		dao.save(theCity);

		//TODO B2. Test our READ method
		List<City> results = dao.findCityByDistrict(testDistrict);
		
		//TODO B3. Assert that thing are what we expect
		assertNotNull(results);
		assertEquals(1, results.size());
		City savedCity = results.get(0);
		assertCitiesAreEqual(theCity, savedCity);

		
	}
	
	


	private void assertCitiesAreEqual(City expected, City actual) {
		assertEquals("ID Match", expected.getId(), actual.getId());
		assertEquals("Name Match", expected.getName(), actual.getName());
		assertEquals("District Match", expected.getDistrict(), actual.getDistrict());
		assertEquals("Country Code Match", expected.getCountryCode(), actual.getCountryCode());
		assertEquals("Population Match", expected.getPopulation(), actual.getPopulation());
	}

	
	
	
	

	private City getCity(String name, String district, String countryCode, int population) {
		City theCity = new City();
		theCity.setName(name);
		theCity.setDistrict(district);
		theCity.setCountryCode(countryCode);
		theCity.setPopulation(population);
		return theCity;
	}
	
	
	
	
	
	
	
	
	
}
