﻿/* ##################################################
Attaching Event Handlers via Javascript
################################################## */

//Blur
        //Could use $("input[type='text']") to add this same handler to all <input type="text"> elements
$("#fname").on("blur", function(event) {
    const uCaseVal = $(this).val().toUpperCase();
    $(this).val(uCaseVal);
})

//Change
$("#activity").on("change", function(event) {
    console.log(event);
    const uCaseVal = $(this).val().toUpperCase();
    $(this).val(uCaseVal);

    // event.preventDefault();

});

//Change
$("#programmingLanguage").on("change", function(event) {
    const lang = $(this).val();
    $("#languageMessage").html("You selected " + lang);
})

//Focus
// $("#username").on("focus", function(event) {
//     $(this).css("background-color", "green");
//     $(this).css("color", "white");
// })
// $("#username").on("blur", function(event) {
//     $(this).css("background-color", "white");
//     $(this).css("color", "black");
// })

$("#username").on("focus", function(event) {
    $(this).addClass("active");
})
//The changes are not automatically un-done when the field is blurred,
//  so we have to manually change it back
$("#username").on("blur", function(event) {
    $(this).removeClass("active");
})



//Select
var messageField = $("#message");
messageField.on("select", function (event) {
    $("#selectedMessage").html("You selected the textbox");
});


//keyup
var birthDateField = $("#birthDate");
birthDateField.on("keyup", function (event) {
    $("#lastkey").html("The last key was " + event.key);
    $("#birthdateMessage").html($(this).val());
});

//MouseOver & MouseOut
var mouseOverBox = $("#mouseover");
mouseOverBox.on("mouseover", function (event) {
    $(this).css("background", "#860036");
});
mouseOverBox.on("mouseout", function (event) {
    $(this).css("background", "#F0b212");
});

//MouseDown & MouseUp
var mouseDownBox = $("#mousedown");
mouseDownBox.on("mousedown", function (event) {
    $(this).css("background", "#023264");
});
mouseDownBox.on("mouseup", function (event) {
    $(this).css("background", "#CC0000");
});

//Click
var clickBox = $("#click");
clickBox.on("click", function (event) {
    $(this).css("background", "blue");
});

//Double Click
var dblclickBox = $("#dblclick");
dblclickBox.on("dblclick", function (event) {
    $(this).css("background", "green");
});

//Context Menu
var rightclickBox = $("#rightclick");
rightclickBox.on("contextmenu", function (event) {
    event.preventDefault();
    $(this).css("background", "violet");
    return false;
});




// Sample of preventing event propagation...

// $(".example").on("click", function(event) {
//     alert("Clicked example div.");
// })

// $("input").on("click", function(event) {
//     event.stopPropagation();
//     alert("Clicked input tag");

// })