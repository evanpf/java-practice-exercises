/* #########################################
Anonymous Functions
######################################### */

//We've seen function declarations that allow us to write reusable code like this: 

function speak(numberOfTimes) {
    for(var i = 0; i < numberOfTimes; i++) {
        console.log("WOOF");   
    }
  }
  
  speak(5);

  
/* We can assign a function to a variable. This is called a "function expression". 
When using this approach, we don't need to provide a function name (unless we want 
to recursively call it). */

// Note that we are assigning an anonymous function to a variable named `fly`.
var fly = function(num) {
    for(var i = 0; i < num; i++) {
        console.log("I'm flying!!");   
    }   
};

fly(3); // here we use the `fly` variable to invoke the function.

let makeDogSound = speak;

makeDogSound(10);

let thinkHappyThoughts = fly;

thinkHappyThoughts(20);


function filter(things, matcher) {
    var results = [];
    for(var i = 0; i < things.length; i++) {
        if(matcher(things[i])) {
            results.push(things[i]);
        }
    }
    return results;
}


const input = [1,2,3,4,3,3,3,5,6,7,8,9];


const threesOnlyMatcher = function(thing) {
    if(thing === 3) return true;
    return false;
}

function evenNumbers(num) {
    return num % 2 == 0;
}

function oddNumbers(num) {
    return num % 2 != 0;
}

let nonOddNumbers = evenNumbers;




console.log("threesOnlyMatcher", filter(input, threesOnlyMatcher));
console.log("evenNumbers", filter(input, evenNumbers));
console.log("oddNumbers", filter(input, oddNumbers));

console.log("foursOnlyMatcher", filter(input, function(num) {
    return num === 4;
}));




