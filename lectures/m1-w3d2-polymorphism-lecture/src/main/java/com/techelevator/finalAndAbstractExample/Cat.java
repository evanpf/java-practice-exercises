package com.techelevator.finalAndAbstractExample;

public class Cat extends FarmAnimal {

	public Cat() {
		super("Cat", "meow");
	}
	
//	@Override
//	public String getSound() {
//		return this.sound;
//	}
	
	@Override
	public String eat() {
		return "purr";
	}
	
}
