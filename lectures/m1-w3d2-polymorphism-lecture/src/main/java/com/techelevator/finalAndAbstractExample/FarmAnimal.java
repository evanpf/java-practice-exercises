package com.techelevator.finalAndAbstractExample;

import com.techelevator.farm.Singable;

public abstract class FarmAnimal implements Singable{

	protected String sound;
	protected String name;
	protected boolean sleeping;

	public FarmAnimal(String name, String sound) {
		this.name = name;
		this.sound = sound;
		this.sleeping = false;
	}
	
	public final String getSound() {
		if(sleeping) {
			return "zzzzzzzz.....";
		} else {
			return this.sound;
		}
	}
	
	public String getName() {
		return this.name;
	}
	
	public boolean isSleeping() {
		return this.sleeping;
	}
	
	public void sleep() {
		this.sleeping = true;
	}
	
	public void wakeUp() {
		this.sleeping = false;
	}
	
	public abstract String eat();
	
	
}
