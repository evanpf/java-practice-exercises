package com.techelevator.finalAndAbstractExample;

public class Cow extends Livestock {

	public Cow() {
		super("Cow", "moo");
	}

	@Override
	public String eat() {
		return "nom nom nom";
	}
	
}
