package com.techelevator.finalAndAbstractExample;

import com.techelevator.farm.Sellable;

public abstract class Livestock extends FarmAnimal {

	public Livestock(String name, String sound) {
		super(name, sound);
	}	
	
}
