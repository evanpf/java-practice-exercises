package com.techelevator.finalAndAbstractExample;

public class Program {

	public static void main(String[] args) {
		
		
		FarmAnimal[] animals = { new Cow(), new Cat() };
		
		//This doesn't work because FarmAnimal is abstract.
//		FarmAnimal testAnimal = new FarmAnimal("Dog", "Bark");
		
		for(FarmAnimal animal : animals) {
			animal.sleep();
			System.out.println("the " + animal.getName() + " says " + animal.eat() + " when it eats.");
		}
		
	}
	
}
