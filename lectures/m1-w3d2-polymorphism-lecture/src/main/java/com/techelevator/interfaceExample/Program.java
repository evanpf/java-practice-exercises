package com.techelevator.interfaceExample;

public class Program {

	public static void main (String[] args) {
		
		SoundMaker[] soundMakers = { new Car(), new Goat(), new Car(), new Car(), new Goat() };
		
		for(SoundMaker noisyThing : soundMakers) {
			System.out.println("The noisy thing says: " + noisyThing.getSound());
			noisyThing.makeSound();
			System.out.println();
		}
		
		
	}
	
}
