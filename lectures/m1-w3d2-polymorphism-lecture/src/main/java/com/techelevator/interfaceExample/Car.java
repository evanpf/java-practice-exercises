package com.techelevator.interfaceExample;

public class Car implements SoundMaker {

	private final String sound = "Beep beep";
	
	@Override
	public String getSound() {
		return this.sound;
	}

	@Override
	public void makeSound() {
		System.out.println(this.sound);
	}

}
