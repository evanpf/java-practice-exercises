package com.techelevator.interfaceExample;

public class Goat implements SoundMaker {

	private String noise = "Maa";
	
	@Override
	public String getSound() {
		return this.noise;
	}

	@Override
	public void makeSound() {
		System.out.println(noise + " " + noise + " " + noise);
	}
	
}
