package com.techelevator.interfaceExample;

public interface SoundMaker {

	public String getSound();
	public void makeSound();
	
}
