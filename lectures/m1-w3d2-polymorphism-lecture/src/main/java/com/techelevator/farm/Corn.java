package com.techelevator.farm;

public class Corn implements Sellable {

	private String name;
	private double price;
	
	public Corn() {
		this.name = "Corn";
		this.price = 200.0;
	}
	
	@Override
	public String getName() {
		return this.name;
	}
	
	@Override
	public double getPrice() {
		return this.price;
	}

}
