package com.techelevator.farm;

public class Cat extends FarmAnimal implements Singable {
	
	public Cat() {
		super("Cat", "meow!");
	}

//	Because FarmAnimal has getSound() marked as `final`, we can't override it.
//	@Override
//	public String getSound() {
//		return this.sound;
//	}

}
