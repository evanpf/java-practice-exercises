package com.techelevator.farm;

public class Cow extends FarmAnimal implements Singable {

	public Cow() {
		super("Cow", "moo!");
	}

//	Because FarmAnimal has getSound() marked as `final`, we can't override it.
//	@Override
//	public String getSound() {
//		return this.sound + this.sound;
//	}
	
}