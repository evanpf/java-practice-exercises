package com.techelevator.farm;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class OldMacdonald {
	public static void main(String[] args) {
		
		Object[] items = new Object[] { new Cat(), new Cow(), new Object(), new Chicken(), new Goat(), new Sheep(), new Tractor(), new Corn()};
		
		for(Object item : items) {

			if(item instanceof FarmAnimal) {
//				((FarmAnimal) item).sleep();
			}

			if(item instanceof Singable) {

				Singable singableItem = (Singable)item;
				
				String name = singableItem.getName();
				String sound = singableItem.getSound();

				log("Old MacDonald had a farm, ee, ay, ee, ay, oh!");
				log("And on his farm he had a "+name+", ee, ay, ee, ay, oh!");
				log("With a "+sound+" "+sound+" here");
				log("And a "+sound+" "+sound+" there");
				log("Here a "+sound+" there a "+sound+" everywhere a "+sound+" "+sound);
				log("Old MacDonald had a farm, ee, ay, ee, ay, oh!");
			
			}
			
			
			
			
			if(item instanceof Sellable) {
				Sellable sellableItem = (Sellable)item;
				String name = sellableItem.getName();
				BigDecimal price = new BigDecimal(sellableItem.getPrice());
				price = price.setScale(2, RoundingMode.HALF_UP);

				System.out.println("(Buy a " + name + " for $" + price + ")");
			}
			
			System.out.println();
			
		}
		

		
	}
	
	public static void log(String message) {
		System.out.println(message);
	}
	
}