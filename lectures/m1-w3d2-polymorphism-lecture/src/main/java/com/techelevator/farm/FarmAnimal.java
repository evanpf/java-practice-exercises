package com.techelevator.farm;

public class FarmAnimal {
	private String name;
	protected String sound;
	protected boolean sleeping;
	
	
	public FarmAnimal() {
		this.name = "default";
		this.sound = "AHHHHHHHHHHHH!";
	}
	
	public FarmAnimal(String name, String sound) {
		this.name = name;
		this.sound = sound;
	}
	
	public String getName( ) {
		return name;
	}

	//Marking as `final` will keep child classes from overriding this method.
	public final String getSound( ) {
		if(sleeping) {
			return "zzzzzz";
		} else {
			return sound;
		}
	}
	
	public boolean isSleeping() { 
		return this.sleeping;
	}
	
	public void sleep() {
		this.sleeping = true;
	}
	
	public void wakeUp() {
		this.sleeping = false;
	}
	
}