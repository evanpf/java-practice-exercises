package com.techelevator.farm;

public class Chicken extends FarmAnimal implements Sellable, Singable {
	
	private double price;
	
	public Chicken() {
		super("Chicken", "cluck!");
		price = 25.0;
	}
	
	public void layEgg() {
		System.out.println("Chicken laid an egg!");
	}
	
	public double getPrice() {
		return this.price;
	}

}