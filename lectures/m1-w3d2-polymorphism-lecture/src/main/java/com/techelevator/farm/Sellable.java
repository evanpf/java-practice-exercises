package com.techelevator.farm;

public interface Sellable {

	public double getPrice();
	public String getName();
	
}
