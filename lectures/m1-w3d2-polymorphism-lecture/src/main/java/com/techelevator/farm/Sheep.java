package com.techelevator.farm;

public class Sheep extends FarmAnimal implements Sellable, Singable {

	private double price;
	
	public Sheep() {
		super("Sheep", "baa!");
		price = 10.0;
	}

	@Override
	public double getPrice() {
		return this.price;
	}
	
	
	
}
