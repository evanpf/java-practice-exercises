package com.techelevator.staticExample;

import java.util.Arrays;
import java.util.List;

public class Program {

	public static void main(String[] args) {
		
		String name = "George Jones";
		
		String firstName = StringUtils.getFirstName(name);
		
		String[] strings = {"A", "B", "C", "D", "E"}; 
		List<String> stringList = Arrays.asList(strings);
		
		
		System.out.println("First Name: " + firstName);
		
	}
	
	
}
