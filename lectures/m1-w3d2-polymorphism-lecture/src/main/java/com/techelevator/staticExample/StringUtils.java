package com.techelevator.staticExample;

public class StringUtils {

	public static String getFirstName(String name) {
		
		String[] nameParts = name.split(" ");
		
		return nameParts[0];
	}
	
}
