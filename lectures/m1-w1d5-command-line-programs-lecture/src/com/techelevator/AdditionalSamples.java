package com.techelevator;

import java.util.Scanner;

public class AdditionalSamples {

	public static void main(String[] args) {
		
		//Set up Scanner to read input
		Scanner inputReader = new Scanner(System.in);
		
		//Prompt the user for input, and then get it.
		System.out.print("Enter multiple values here, separated by spaces: ");
		String inputLine = inputReader.nextLine();
		
		//Separate the items inputted on the space character, and place them in an array
		String[] inputItems = inputLine.split(" ");
		
		//Loop through the items in the array to display them
		for(int i = 0; i < inputItems.length; i++) {
			System.out.println("Item " + i + ": " + inputItems[i]);
		}
		
		
		
	}
	
}
