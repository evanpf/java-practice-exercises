package com.techelevator;

import java.util.Scanner;


public class Lecture {

	
	public static void main(String[] args) {

		boolean done = false;		//Whether the user is done doing conversions
		String tempString;			//The temperature string entered by the user
		double temp;					//The parsed temperature
		String tempScaleString;		//The temperature scale entered by the user ("C" or "F")
		double convertedTemp = 0;		//The converted temperature
		
		//Set up the Scanner for user input
		Scanner inputReader = new Scanner(System.in);
		
		while(!done) {
		
			//Ask user for temperature
			System.out.print("Enter the temperature: ");
			tempString = inputReader.nextLine();
			
			//Ask current scale (F or C)
			System.out.print("Enter the scale of the above temperature as (C) for Celsius or (F) for Fahrenheit: ");
			tempScaleString = inputReader.nextLine();
			tempScaleString = tempScaleString.toLowerCase();
			
			//Parse input to number (double?)
			temp = Double.parseDouble(tempString);
			
			boolean scaleIsValid = false;
			//Determine what to calculate
			if(tempScaleString.equals("c")) {
				//Calculate Celsius to Fahrenheit
				convertedTemp = CalculateFahrenheit(temp);
				scaleIsValid = true;
			} else if(tempScaleString.equals("f")){
				convertedTemp = CalculateCelsius(temp);
				scaleIsValid = true;
			} else {
				System.out.println("Invalid temperature scale.  Please enter C or F");
			}
			
			//Display the result

			if(scaleIsValid) {
				System.out.printf("%.2f => %.2f", temp, convertedTemp);
			}
			System.out.println();
			System.out.println();

			System.out.print("Convert another temperature?  Enter Y or N: ");
			String doAnother = inputReader.nextLine();
			
			if(doAnother.toLowerCase().equals("n")) {
				done = true;
			}
			
		}
		
		
		inputReader.close();
		
		
	}
	
	
	
	public static double CalculateCelsius(double fahrenheitTemp) {

//		[°C] = ([°F] − 32) × ​5⁄9 					https://en.wikipedia.org/wiki/Temperature
		return (fahrenheitTemp - 32.0) * (5.0 / 9.0);
	}
	
	public static double CalculateFahrenheit(double celsiusTemp) {
//		[°F] = [°C] × ​9⁄5 + 32						https://en.wikipedia.org/wiki/Temperature
		return (celsiusTemp * (9.0 / 5.0)) + 32.0;	
	}
	
	

	
	
	
//	public static void main(String[] args) {
//		
//		Scanner inputReader = new Scanner(System.in);
//		
//		System.out.print("Enter your name: ");
//		String name = inputReader.nextLine();
//
//		System.out.print("Enter the hour: ");
//		String hourString = inputReader.nextLine();
//		
//		int hour = Integer.parseInt(hourString);
////		long hourLong = Long.parseLong(hourString);
////		double hourDouble = Double.parseDouble(hourString);
////		float hourFloat = Float.parseFloat(hourString);
//		
//		int hoursToMidnight = 24 - hour;
//
////		Date dt;
//		
//		System.out.println();
//		System.out.println("Hello, " + name);
//		System.out.println("The current hour is: " + hour);
//		System.out.println("There are " + hoursToMidnight + " hours until midnight.");
////		System.out.println("The current hour is: " + hourLong);
////		System.out.println("The current hour is: " + hourDouble);
////		System.out.println("The current hour is: " + hourFloat);
//		
//		
//		
//		
//	}

	
}
