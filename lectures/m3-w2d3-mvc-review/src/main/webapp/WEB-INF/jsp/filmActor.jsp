<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>

	<c:choose>
		<c:when test="${empty film}">
			<h1>Film not found!</h1>
		</c:when>
		<c:otherwise>
			<h1>Actor List</h1>
			
			<h3>${film.title}</h3>
			<p>${film.description}</p>
			<h4>Actors</h4>
			<ul>
				<c:forEach var="actor" items="${actors}">
					<li>${actor.firstName} ${actor.lastName}</li>
				</c:forEach>
			</ul>

		</c:otherwise>
	</c:choose>


</body>
</html>