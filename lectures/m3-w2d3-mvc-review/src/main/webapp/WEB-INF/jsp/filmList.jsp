<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
	
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Insert title here</title>
	<style>
		td, th {
			border: none;
			text-align: left;
		}
	</style>
</head>
<body>
	<h1>Film List</h1>

	<table style="width: 50%; text-align: center;">
		<tr>
			<th>Title</th>
			<th>Release Year</th>
			<th>Length</th>
			<th>Rating</th>
		</tr>

		<c:forEach var="film" items="${films}">
			<tr>
				<td>
					<c:url var="filmUrl" value="/filmActor" />
					<a href="${filmUrl}?filmId=${film.id}">
						${film.title}
					</a>
				</td>
				<td>${film.releaseYear}</td>
				<td>${film.length}</td>
				<td>${film.rating}</td>
			</tr>
		</c:forEach>

	</table>

</body>
</html>