package com.techelevator;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.techelevator.model.Film;
import com.techelevator.model.Actor;
import com.techelevator.model.dao.FilmDAO;

@Controller
public class FilmController {

	@Autowired
	private FilmDAO filmDao;
	
	@RequestMapping("/")
	public String showFilmList(ModelMap map) {
		
		List<Film> allFilms = filmDao.getAllFilms(); 
		
		map.put("films", allFilms);
		
		return "filmList";
	}
	
	@RequestMapping(path="/filmActor", method=RequestMethod.GET)
	public String showFilmActors(ModelMap map, @RequestParam Long filmId) {
		
		Film film = filmDao.getFilm(filmId);
		List<Actor> actors = new ArrayList<Actor>();
		
		if(film != null) {
			actors = filmDao.getActorForFilm(filmId);
		}
		
		map.put("film",  film);
		map.put("actors", actors);
		
		return "filmActor";
	}
	
	
}
