package com.techelevator.model.jdbc;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;

import com.techelevator.model.Actor;
import com.techelevator.model.Film;
import com.techelevator.model.dao.FilmDAO;

@Component
public class JdbcFilmDao implements FilmDAO {

	
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	public JdbcFilmDao(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	
	
	@Override
	public List<Film> getAllFilms() {

		List<Film> allFilms = new ArrayList<Film>();
		String sqlSelectAllFilms = "SELECT * FROM film";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlSelectAllFilms);
		while(results.next()) {
			Film film = new Film(results.getLong("film_id"));
			film.setTitle(results.getString("title"));
			film.setDescription(results.getString("description"));
			film.setReleaseYear(results.getInt("release_year"));
			film.setLength(results.getInt("length"));
			film.setRating(results.getString("rating"));
			allFilms.add(film);
		}
		return allFilms;
	}

	@Override
	public List<Actor> getActorForFilm(Long filmId) {
		List<Actor> actors = new ArrayList<Actor>();
		String sqlSelectActors = "SELECT * FROM actor JOIN film_actor ON actor.actor_id = film_actor.actor_id WHERE film_actor.film_id = ?";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlSelectActors, filmId);

		while(results.next()) {
			actors.add(new Actor(results.getLong("film_id"), results.getString("first_name"), results.getString("last_name")));
		}
		return actors;
	}



	@Override
	public Film getFilm(Long filmId) {
		String sqlSelectAllFilms = "SELECT * FROM film WHERE film_id = ?";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlSelectAllFilms, filmId);
		if(results.next()) {
			Film film = new Film(results.getLong("film_id"));
			film.setTitle(results.getString("title"));
			film.setDescription(results.getString("description"));
			film.setReleaseYear(results.getInt("release_year"));
			film.setLength(results.getInt("length"));
			film.setRating(results.getString("rating"));
			return film;
		}
		return null;
	}

}
