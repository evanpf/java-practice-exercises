package com.techelevator.model;

public class Actor {

	private Long id;
	private String firstName;
	private String lastName;
	
	//Constructors
	public Actor(Long id) {
		this.id = id;
	}

	public Actor(Long id, String firstName, String lastName) {
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
	}

	
	//Getters and Setters	
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Long getId() {
		return id;
	}
	
	
	
}
