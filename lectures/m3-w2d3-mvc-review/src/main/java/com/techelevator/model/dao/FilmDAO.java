package com.techelevator.model.dao;

import java.util.List;

import com.techelevator.model.Actor;
import com.techelevator.model.Film;

public interface FilmDAO {

	public List<Film> getAllFilms();
	public Film getFilm(Long filmId);
	public List<Actor> getActorForFilm(Long filmId);
	
	
}
