package com.techelevator.model;

public class Category {

	private Long id;
	private String name;
	
	//Constructors
	public Category(Long id) {
		this.id = id;
	}
	
	public Category(Long id, String name) {
		this.id = id;
		this.name = name;
	}
	
	
	//Getters and Setters
	public String getName() {
		return this.name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public Long getId() {
		return this.id;
	}
	
}
