package com.techelevator;

public class House {

	public int Rooms;				//Does not include bathrooms
	public int SquareFootage;
	public float Bathrooms;
	public float Storeys;
	public String Color;
	public boolean HasBasement;
	
	
	public float getTotalNumberOfRooms() {
		
		return Rooms + Bathrooms;
		
	}


	public House(int rooms, int squareFootage, float bathrooms, float storeys, String color, boolean hasBasement) {
		super();
		Rooms = rooms;
		SquareFootage = squareFootage;
		Bathrooms = bathrooms;
		Storeys = storeys;
		Color = color;
		HasBasement = hasBasement;
	}
	
	
	
	
	
}
