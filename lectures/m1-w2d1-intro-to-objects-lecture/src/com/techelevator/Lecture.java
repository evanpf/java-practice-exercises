package com.techelevator;

public class Lecture {

	public static void main(String[] args) {
		
		
		House myHouse = new House(7, 1500, 3.5f, 2f, "Purple", true);
		
		System.out.println("House Square Footage: " + myHouse.SquareFootage);
		System.out.println("Total rooms in the house: " + myHouse.getTotalNumberOfRooms());
		
		// -----------------
		
		System.out.println();
		System.out.println("************************************");
		System.out.println("****** MAKING A STRING OBJECT ******");
		System.out.println("************************************");
		System.out.println();
		
	
		/* The String class gets special treatment in the Java language.  One 
		 * example of this is that there is a literal representation of a 
		 * String (i.e. characters appearing between two double quotes).  This
		 * is not the case for most classes */
		
		char[] nameCharacters = {'G', 'e', 'o', 'r', 'g', 'e'}; 
		String nameFromCharacters = new String(nameCharacters);
		
		System.out.println("Hello, " + nameFromCharacters);
		
		
		System.out.println();
		System.out.println("******************************");
		System.out.println("****** MEMBER METHODS ******");
		System.out.println("******************************");
		System.out.println();
		
		/* Below are examples of calling various String methods */

		/*
		 * .charAt()
		 * .length()
		 * .substring()
		 * .indexOf()
		 * .contains()
		 * .startsWith() / .endsWith()
		 * .replace()
		 * .toUpperCase() / .toLowerCase()
		 * .equals() / .equalsIgnoreCase()
		 * .split()
		 * .join()
		 * 
		 */
		
		
		
		System.out.println("***** charAt *****");
		
		String name1 = "George";
		
		char first = name1.charAt(0);
		char last = name1.charAt(name1.length() - 1);
		
		System.out.println(name1);
		System.out.println(first);
		System.out.println(last);
		
		System.out.println("***** length *****");
		
		String lengthName = "George Frederick Frederickson III";
		System.out.println("The length of the name is : " + lengthName.length());
		
		System.out.println("***** substring *****");
		
		String substringName = "George Frederick Frederickson III";
		String firstName1 = substringName.substring(0, 6);
		String middleName1 = substringName.substring(7, 16);
		String lastName1 = substringName.substring(17);
		System.out.println("First Name(1): " + firstName1);
		System.out.println("Middle Name(1): " + middleName1);
		System.out.println("Last Name(1): " + lastName1);
		
		System.out.println("***** indexOf *****");
		
		String indexOfName = "Frederick Freddy Frederickson III";
		int locationOfFirstSpace = indexOfName.indexOf(" ");
		System.out.println("Location of first space: " + locationOfFirstSpace);
		System.out.println("Location of 'x': " + indexOfName.indexOf("x"));
		String indexOfFirstName = indexOfName.substring(0, locationOfFirstSpace);
		System.out.println("First Name: |" + indexOfFirstName + "|");
		
		
		
//		You can string together stuff, if you want.  Make sure your code is still readable, though!
//		System.out.println(indexOfName.substring(0, indexOfName.indexOf(" ")).toUpperCase().charAt(3));
		
		
		System.out.println("***** contains *****");

		String containsName = "Frederick Freddy Frederickson III";
		boolean hasX = containsName.contains("X");
		boolean hasfreddy = containsName.contains("freddy");
		boolean hasFreddy = containsName.contains("Freddy");
		
		boolean hasFreddyCaseInsensitive = containsName.toUpperCase().contains("FREDDY");
		
		
		System.out.println("Contains X?: " + hasX);
		System.out.println("Contains freddy?: " + hasfreddy);
		System.out.println("Contains Freddy?: " + hasFreddy);
		System.out.println("Contains Freddy (CI)?: " + hasFreddyCaseInsensitive);

		
		System.out.println("***** startsWith / endsWith *****");
		
		String swewName = "Frederick Freddy Frederickson III";
		
		System.out.println("Starts with Fred: " + swewName.startsWith("Fred"));
		System.out.println("Ends with II: " + swewName.endsWith("II"));
		
		System.out.println("***** replace *****");
		String replaceName = "Frederick Freddy Frederickson III";
		
		System.out.println("Replace Freddy with George: " + replaceName.replace("Freddy",  "George"));
		
		System.out.println("***** equals / equalsIgnoreCase *****");
		
		String eqName = "Freddy";
		
		System.out.println("Equals Freddy: " + eqName.equals("Freddy"));
		System.out.println("Equals Freddy (ignore case): " + eqName.equalsIgnoreCase("fREDDY"));
		
		System.out.println("***** split / join *****");
		
		String sjName = "George F. Frederickson";
		
		String[] names = sjName.split(" ");
		
		System.out.println("First Name: " + names[0]);
		System.out.println("Middle Name: " + names[1]);
		System.out.println("Last Name: " + names[2]);
		
		String joinedName = String.join(" ", names);
		System.out.println("Joined Name: " + joinedName);
		
		System.out.println("***** trim *****");
		
		String trimName = "    \t   George       \n \n     ";
		
		System.out.println("UnTrimmed name: " + trimName);
		System.out.println("Trimmed name: " + trimName.trim());
		System.out.println("Next line...");
		
		
		/*
		 * 
		 * SAMPLE METHOD CALL
		 * 
		 * 
		 */
		
		String badCharString = "Thi5s ha$s *&#unwanted &^%$#characters1254876325\\=/[][].";
		String removed = stripUnwantedCharacters(badCharString, "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ ");
		System.out.println("Remove unwanted characters: " + removed);
		
		
		
	}
	
	public static String stripUnwantedCharacters(String str, String validCharacters) {
		
		//Set up what we're going to return
		String outputString = "";
		
		//Look at each item in our collection/list/string/etc.
		for(int i = 0; i < str.length(); i++) {
			
			char currentCharacter = str.charAt(i);
			
			if(validCharacters.indexOf(currentCharacter) >= 0) {
				//Character is valid!
				outputString += currentCharacter;
			}
			
			
		}
		
		
		
		return outputString;
		
	}
	
	
}

