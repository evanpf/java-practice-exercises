-- TRANSACTIONS

-- 1. Try deleting all of the rows from the country language table and roll it back.

-- BEGIN TRANSACTION;

-- select count(*), 'Before' from countrylanguage;

-- delete from countrylanguage;

-- select count(*), 'After' from countrylanguage;

-- ROLLBACK TRANSACTION;

-- select count(*), 'After Rollback' from countrylanguage;

-- -- 2. Try updating all of the cities to be in the USA and roll it back

-- BEGIN TRANSACTION;

-- select count(*), 'Before' from city where countrycode = 'USA';

-- update city set countrycode = 'USA';

-- select count(*), 'After' from city where countrycode = 'USA';

-- ROLLBACK TRANSACTION;

-- select count(*), 'After Rollback' from city where countrycode = 'USA';


-- Try a delete that fails

-- BEGIN TRANSACTION;

-- select count(*), 'Before' from countrylanguage where countrycode = 'NIU';
-- delete from countrylanguage where countrycode = 'NIU';
-- select count(*), 'After' from countrylanguage where countrycode = 'NIU';

-- delete from country where code = 'NIU'; --This fails due to fk constraint, so transaction is rolled back.

-- COMMIT TRANSACTION;

-- select count(*), 'After Rollback' from countrylanguage where countrycode = 'NIU';


BEGIN TRANSACTION;

select count(*), 'Before' from countrylanguage where countrycode = 'NIU';
delete from countrylanguage where countrycode = 'NIU';
select count(*), 'After' from countrylanguage where countrycode = 'NIU';

UPDATE country SET capital = null WHERE code = 'NIU';
delete from city where countrycode = 'NIU';

delete from country where code = 'NIU'; --This should succeed this time.

COMMIT TRANSACTION;

select count(*), 'After Rollback' from countrylanguage where countrycode = 'NIU';

-- select * from country where code = 'NIU';
-- select * from countrylanguage where countrycode = 'NIU';



