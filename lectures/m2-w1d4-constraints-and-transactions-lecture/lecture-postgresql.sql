-- INSERT

-- 1. Add Klingon as a spoken language in the USA
-- 2. Add Klingon as a spoken language in Great Britain

--Find out what columns are in the countrylanguage table
select * from countrylanguage where countrycode in ('USA', 'GBR');

-- Get the country codes for USA and GBR
select * from country where name IN ('United Kingdom', 'United States');

--  Column Names: countrycode, language, isofficial, percentage
INSERT INTO countrylanguage 
    (countrycode, language, isofficial, percentage)
    VALUES
    ('GBR', 'Klingon', false, 0.0);

INSERT INTO countrylanguage
    (countrycode, language, isofficial, percentage)
    VALUES
    ('USA', 'Klingon', false, 0);

-- INSERT INTO countrylanguage 
--     (countrycode, language, isofficial, percentage)
--     VALUES
--     ('GBR', 'Romulan', false, 0.0),
--     ('USA', 'Romulan', false, 0);


-- UPDATE

-- 1. Update the capital of the USA to Houston

select * from country where code = 'USA';
select * from city where name = 'Houston';  --Houston ID: 3796

UPDATE country
   SET capital = 3796
 WHERE code = 'USA';

SELECT country.name, country.code, city.name, city.id FROM country 
JOIN city ON country.capital = city.id
WHERE code = 'USA';


-- 2. Update the capital of the USA to Washington DC and the head of state
select * from country where code = 'USA';

select * from city where name like 'Washington%';  --Washingtod DC Id: 3813

UPDATE country
   SET capital = 3813,
       headofstate = 'Donald J. Trump'
 WHERE code = 'USA';

SELECT country.name, country.headofstate, country.code, city.name, city.id FROM country 
JOIN city ON country.capital = city.id
WHERE code = 'USA';


UPDATE country SET indepyear = indepyear + 10 where indepyear is not null;
UPDATE country SET indepyear = indepyear - 10 where indepyear is not null;


-- DELETE

-- 1. Delete English as a spoken language in the USA

--	USA English	true	86.2

select * from countrylanguage where countrycode in ('USA');


DELETE FROM countrylanguage
      WHERE countrycode = 'USA'
        AND language = 'English';

-- DELETE FROM countrylanguage
--       WHERE countrycode in (SELECT code from country where name = 'United States')
--         AND language = 'English';

-- 2. Delete all occurrences of the Klingon language 
DELETE FROM countrylanguage
      WHERE language = 'Klingon';



-- REFERENTIAL INTEGRITY

-- 1. Try just adding Elvish to the country language table.

INSERT INTO countrylanguage (language) values ('Elvish');   --Fails! due to not-null constraints


-- 2. Try inserting English as a spoken language in the country ZZZ. What happens?
INSERT INTO countrylanguage (countrycode, language, isofficial, percentage) 
values ('ZZZ', 'English', false, 0);    --FAILS! due to foreign key constraint

-- 3. Try deleting the country Nieu (NIU). What happens?

delete from country where code = 'NIU';

select * from country where code = 'NIU';
select * from countrylanguage where countrycode = 'NIU';

delete from countrylanguage where countrycode = 'NIU';
UPDATE country SET capital = null WHERE code = 'NIU';
delete from city where countrycode = 'NIU';
delete from country where code = 'NIU';



-- CONSTRAINTS

-- 1. Try inserting English as a spoken language in the USA
select * from countrylanguage where countrycode = 'USA';

INSERT INTO countrylanguage (countrycode, language, isofficial, percentage)
    VALUES ('USA', 'English', true, 86.2);

-- 2. Try again. What happens?
INSERT INTO countrylanguage (countrycode, language, isofficial, percentage)
    VALUES ('USA', 'English', true, 86.2);

-- 3. Let's relocate the USA to the continent - 'Outer Space'
UPDATE country SET continent = 'Outer Space' WHERE code = 'USA';


-- How to view all of the constraints

SELECT * FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS;
SELECT * FROM INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE;
SELECT * FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS;


