<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
	<head>
		<title>Hello Spring MVC</title>
	</head>
	<body>
		<%-- You can access modelMap variables here.  They are in requestScope. --%>
		<h1><c:out value="${greeting}" /></h1>
	</body>
</html>