package com.techelevator;


import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/* The @Controller annotation will make the Spring container aware 
 * of this controller as a managed bean. Controllers in Spring Web 
 * MVC are just plain old Java objects that are annotated to indicate 
 * that they are an MVC controller 
 * 
 * The @Controller annotation indicates to the Spring framework 
 * that this class should be treated as a controller
 * 
 * The <component-scan> element in springmvc-servlet.xml causes 
 * Spring to look at all of the classes in the package specified 
 * and look for classes with this annotation. This controller will
 * be found during the Spring component scan during application
 * startup.
 * 
 * Controllers (and pretty much everything else in Spring) can also be 
 * configured using XML.  However, annotations have become the 
 * suggested way of wiring together a Spring application. */
@Controller										//TODO 2. Add @Controller to your controller class
public class HelloController {					//TODO 1. Create Controller class

	/* The @RequestMapping annotation is used to map resource paths to 
	 * method calls.
	 * 
	 * This particular mapping says that any requests for the root 
	 * should invoke this method.
	 * 
	 * All controller request handling methods should return a String 
	 * that represents the "logical view name" */
	@RequestMapping("/")							//TODO a3. Add @RequestMapping for URL to process
	public String displayHomePage() {			//TODO a4. Create method that returns a String for handling the URL request
		return "homePage";						//TODO a5. Return the LOGICAL NAME of the JSP ("/WEB-INF/jsp/" will be added to the beginning, and ".jsp" will be added to the end
	}
	
	
	
	/* Adding ModelMap will give you access to a Request-Scope Map so you can pass values to the View (JSP)
	 * 
	 * Adding @RequestParam to an input will search the Query String for a key with the variable's name and 
	 * parse it into the given type.
	 */
	@RequestMapping("/greeting")					//TODO b3. Add @RequestMapping for URL to process
	public String displayGreeting(				//TODO b4. Create method that returns a String for handling the URL request
			ModelMap modelMap,					//TODO b5. add ModelMap to your parameters so you can send values to the JSP
			@RequestParam String name) {			//TODO b6. Add @RequestParam to a parameter to look in the QueryString of the URL for a matching variable name
		
		Greeter greeter = new Greeter(name);					//TODO b7. Do stuff with the values you got
		modelMap.put("greeting", greeter.getGreeting());		//TODO b8. Set values needed in the JSP as elements in the modelMap
		
		/* Controller methods can return a variety of types of values, 
		 * but we will be using `String`.  The value of the returned 
		 * String is the *logical view name* to render.  The default 
		 * implementation is to map the logical view name directly to 
		 * a file, however we configured a different View Resolver 
		 * that maps logical view names to JSP files under WEB-INF/jsp 
		 * See springmvc-servlet.xml for details. */
		return "greeting";						//TODO b9. Return the LOGICAL NAME of the JSP ("/WEB-INF/jsp/" will be added to the beginning, and ".jsp" will be added to the end
	}
	
	
	

}
