package com.techelevator.fbn.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.techelevator.fbn.model.MortgageLoanEstimate;


@Controller										//TODO 2. Add @Controller annotation 
public class PaymentCalculatorController {		//TODO 1. Create Controller class

	@RequestMapping("/mortgageCalculatorInput")			//TODO 3. Add @RequestMapping for the expected URL
	public String displayMortgageCalculatorInput() {		//TODO 4. Create the method to handle the mapped URL
		return "mortgageCalculatorInput";				//TODO 5. Return the logical view name
	}

	
	@RequestMapping("/mortgageCalculatorResult")			//TODO 6. Add @RequestMapping to handle the submitted form
	public String displayMortgageCalculatorResults(		//TODO 7. Add method to handle submitted form data
			ModelMap map,								//TODO 8. Add ModelMap to params so we can talk to the JSP
			@RequestParam int loanAmount,				//TODO 9. Add parameters to get from the QueryString.  Use @RequestParam to tell Spring to populate with values from the QueryString.
			@RequestParam int loanTerm,
			@RequestParam double rate
	) {	
		
		//TODO 11. Do stuff with the Model
		MortgageLoanEstimate estimate = new MortgageLoanEstimate(loanAmount, loanTerm, rate);
		
		//TODO 12. Set output values for JSP
		map.put("estimate", estimate);
		
		return "mortgageCalculatorResult";				//TODO 10. Return the logical view name
	}

	
	


}
