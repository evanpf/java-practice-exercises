package com.techelevator.fbn.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller								//TODO 2. Add the @Controller annotation to tell Spring MVC that this is a controller
public class HomeController {			//TODO 1. Create the Controller class
	
	@RequestMapping("/")					//TODO 3. Add @RequestMapping and give it the URL you want to use for this method
	public String displayHomePage() {	//TODO 4. Add method to process the request
		return "homePage";				//TODO 5. Return the logical view name to display.
	}




}
