package com.techelevator;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class NameServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		//Get a list of names -- This would likely be gotten from the DB by calling a DAO
		List<String> names = new ArrayList<String>();
		names.add("Caspian");
		names.add("Zephan");
		names.add("Eve");
		names.add("Peregrine");
		names.add("Dulcinea");
		names.add("Georgia");
		names.add("Iolanthe");
		

		/* This line adds a request attribute with the name "nameList" 
		 * that contains our list of names. This will later be 
		 * used by the View (i.e. JSP) to display student names in HTML */
		//Set an attribute that the View has access to that contains the list of names
		req.setAttribute("nameList", names);
		
		//Additional attributes can be set as well.
		req.setAttribute("familyName", "Tyler");
		
		
		
		//Show the view
		/* This line forwards the request to the JSP */
		// File Path for JSP is relative to the webroot (./src/main/webapp)
		this.getServletContext().getRequestDispatcher("/WEB-INF/nameList.jsp").forward(req, resp);

	
	}
}
