package com.techelevator;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class PersonServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		//This would be populated by calling a DAO method
		List<Person> people = new ArrayList<Person>();
		people.add(new Person("Sarah", "Tyler", 21));
		people.add(new Person("Caspian", "Tyler", 15));
		people.add(new Person("Zephan", "Tyler", 13));
		people.add(new Person("Eve", "Tyler", 9));

		//Make the list of people available to the JSP
		req.setAttribute("personList", people);
		
		//Forward the request to the JSP
		this.getServletContext().getRequestDispatcher("/WEB-INF/personList.jsp").forward(req, resp);
		
	}
}
