package com.techelevator;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;
import java.util.Stack;

public class Lecture {

	public static void main(String[] args) {
		System.out.println("####################");
		System.out.println("       LISTS");
		System.out.println("####################");

		List<String> names = new ArrayList<String>();
		
		names.add("Frodo");
		names.add("Sam");
		names.add("Pippin");
		names.add("Merry");
		names.add("Gandalf");
		names.add("Boromir");
		names.add("Gimli");
		names.add("Legolas");
		
		
		System.out.println("####################");
		System.out.println("Lists are ordered");
		System.out.println("####################");
		
		//the elements will be returned in the same order they were added
		for(int i = 0; i < names.size(); i++) {
			System.out.println(names.get(i));
		}
		
		
		System.out.println("####################");
		System.out.println("Lists allow duplicates");
		System.out.println("####################");
		
		names.add("Sam");
		
		for(int i = 0; i < names.size(); i++) {
			System.out.println(names.get(i));
		}
		
	
		
		System.out.println("####################");
		System.out.println("Lists allow elements to be inserted in the middle");
		System.out.println("####################");
		
		names.add(5, "Aragorn");
		
		for(int i = 0; i < names.size(); i++) {
			System.out.println(names.get(i));
		}

		
		
		System.out.println("####################");
		System.out.println("Lists allow elements to be changed");
		System.out.println("####################");
		
		names.set(9, "Samwise");

		for(int i = 0; i < names.size(); i++) {
			System.out.println(names.get(i));
		}

		
		System.out.println("####################");
		System.out.println("Lists allow elements to be removed by index");
		System.out.println("####################");
		

		names.remove(1);
		
		for(int i = 0; i < names.size(); i++) {
			System.out.println(names.get(i));
		}

		
		
		System.out.println("####################");
		System.out.println("Find out if something is already in the List");
		System.out.println("####################");	

		
		boolean inList = names.contains("Samwise");
		System.out.println("Is Samwise in the list of names? " + inList);
		
		inList = names.contains("Sam");
		System.out.println("Is Sam in the list of names? " + inList);

		
		System.out.println("####################");
		System.out.println("Find out where something is in the list");
		System.out.println("####################");	
		
		int location = names.indexOf("Samwise");
		int notFoundLocation = names.indexOf("Sam");
		System.out.println("Samwise is at index: " + location);
		System.out.println("Sam is at index: " + notFoundLocation);
		
		
		System.out.println("####################");
		System.out.println("Lists can be turned into an array");
		System.out.println("####################");
	
		String[] namesArray = names.toArray(new String[names.size()]);
		for(int i = 0; i < namesArray.length; i++) {
			System.out.println(namesArray[i]);
		}

		

		
		
		
		
		
		System.out.println("####################");
		System.out.println(" PRIMITIVE WRAPPERS");
		System.out.println("####################");
		
		/* Every primitive data type has an equivalent "primitive wrapper class" that is an object representation 
		 * of a primitive value */
		Integer employees = new Integer(25);       // here we call a constructor that "wraps" a primitive int value in an object
		Integer piecesOfCake = new Integer("24");  // here we call a constructor that converts a String into an Integer

		if(employees > piecesOfCake) {
			System.out.println("Burn the building down");
		}

		Double accountBalance = null;
		//double newBalance = accountBalance + 100;  // this causes a NullPointerException because we are trying to "unbox" a null value

		System.out.println("####################");
		System.out.println("       FOREACH");
		System.out.println("####################");
		System.out.println();

		// Let's loop through names again, but this time using a for-each loop
//		for(int i = 0; i < names.size(); i++) {
//			String currentName = names.get(i);
//
//			System.out.println(currentName);
//
//		}
		
		// for each name in names
		for(String currentName : names) {
			System.out.println(currentName);
		}
		
		

		
		
		System.out.println("####################");
		System.out.println("       QUEUES");
		System.out.println("####################");
		System.out.println();

		Queue<String> todoItems = new LinkedList<String>();
		
		todoItems.offer("Wash the dishes");
		todoItems.offer("Wipe the counters");
		todoItems.offer("Sweep the Floor");
		todoItems.offer("Scrub the floor");
		
		/////////////////////
		// PROCESSING ITEMS IN A QUEUE
		/////////////////////

		while(todoItems.size() > 0) {
			String nextItem = todoItems.poll();
			System.out.println("Next item to do: " + nextItem + " (" + todoItems.size() + " items left)");
		}
		
		
		System.out.println("####################");
		System.out.println("       STACKS");
		System.out.println("####################");
		System.out.println();

		Stack<String> urlStack = new Stack<String>();

		////////////////////
		// PUSHING ITEMS TO THE STACK
		//////////////////// 
		
		urlStack.push("http://www.google.com");
		urlStack.push("http://www.techelevator.com");
		urlStack.push("http://www.google.com");
		urlStack.push("http://www.stackoverflow.com");
		
		
		////////////////////
		// POPPING THE STACK
		////////////////////

		while(urlStack.size() > 0) {
			String lastPage = urlStack.pop();
			System.out.println("Last Page: " + lastPage + " (" + urlStack.size() + " items left on stack)");
		}
		
		
		
	}
}
