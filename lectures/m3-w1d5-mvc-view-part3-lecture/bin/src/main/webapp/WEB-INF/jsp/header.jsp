<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<c:url value="/css/site.css" var="cssURL" />
<link rel="stylesheet" type="text/css" href="${cssURL}">
<title>Solar System Geek - ${ param.pageTitle }</title>
</head>

<body>

<c:url value="/" var="homePageURL" />
<a href="${homePageURL}"> 
    <c:url value="/img/ssg_logo.png" var="logoURL" />
    <img src="${logoURL}" id="logo" />
</a>

<div id="content">

<c:url value="/aboutUs" var="aboutUsURL" />


<%-- Get the correct URL for the resource at the project's relative URL /forum and place it in a page-scope variable called "forumURL" --%>    
<c:url var="forumURL" value="/forum" />


    <nav>
    		<ul>
    			<li><a href="${homePageURL}">Home</a></li>
    			<li><a href="${aboutUsURL}">About Us</a></li>
    			
    			<%-- Use the URL saved in the forumURL variable (using c:url above) to link to the Forum page --%>
    			<li><a href="${forumURL}">Forum</a></li>
    		</ul>
    </nav>
    
    
    <%-- This references the parameter set in the JSP importing this page (i.e. home.jsp) --%>
    <h1>${ param.pageTitle }</h1>
    
    
    
<%--  PAGE HEADER ENDS HERE --%>
