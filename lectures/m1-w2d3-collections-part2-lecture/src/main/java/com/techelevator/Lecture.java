package com.techelevator;

import static org.junit.Assert.assertArrayEquals;

import java.util.List;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.TreeSet;

public class Lecture {

	public static void main(String[] args) {
		
		System.out.println("####################");
		System.out.println("       MAPS");
		System.out.println("####################");
		System.out.println();

		Map<String, String> nameToZip = new HashMap<String, String>();		// Map is an interface and HashMap is a class that implements Map
		
		/* The "put" method is used to add elements to a Map */
		nameToZip.put("Aaron", "45150");
		nameToZip.put("Nick", "45040");
		nameToZip.put("Joe", "45209");
		
		/* The "get" method is used to retrieve elements from a Map */
		System.out.println("Aaron lives in zip code: " + nameToZip.get("Aaron"));
		System.out.println("Nick lives in zip code: " + nameToZip.get("Nick"));
		System.out.println("Joe lives in zip code: " + nameToZip.get("Joe"));
		System.out.println();
		
		System.out.println("We can also retrieve a Set of keys and iterate over them using a for loop:");
		Set<String> keys = nameToZip.keySet();
		
		for(String keyName : keys) {
			System.out.println(keyName + " lives in " + nameToZip.get(keyName));
		}
		
		
		
		//Value can be modified by doing another .put() for the same key
		System.out.println("\n\nput 12345 for key Aaron\n");
		nameToZip.put("Aaron", "12345");
		
		for(String keyName : keys) {
			System.out.println(keyName + " lives in " + nameToZip.get(keyName));
		}		

		//Clear value:
		//	TYPICALLY, YOU WOULD JUST REMOVE THE KEY WITH ITS VALUE using .remove(key)
		System.out.println("\n\nClearing key 'Aaron'...");
		nameToZip.put("Aaron", null);
		for(String keyName : keys) {
			System.out.println(keyName + " lives in " + nameToZip.get(keyName));
		}

		
		
		//Remove items using .remove(key)
		System.out.println("\n\nRemoving key 'Aaron'...");
		nameToZip.remove("Aaron");
		
		keys = nameToZip.keySet();
		for(String keyName : keys) {
			System.out.println(keyName + " lives in " + nameToZip.get(keyName));
		}
		
		
		System.out.println("####################");
		System.out.println("       SETS");
		System.out.println("####################");
		System.out.println();
		
		Set<String> students = new HashSet<String>();	// change HashSet to TreeSet or LinkedHashSet to see how the behavior changes
		
		
		System.out.println("####################");
		System.out.println("Sets cannot contain duplicates");
		System.out.println("####################");
		System.out.println();

		students.add("Jim");
		System.out.println("Added Jim");
		students.add("Seth");
		System.out.println("Added Seth");
		students.add("Dan");
		System.out.println("Added Dan");
		if(students.add("Jim")) {
			System.out.println("Added Jim");
		} else {
			System.out.println("Unable to Jim");// this value will be ignored because it is duplicate
		}
//		System.out.println("Added Jim again");
		System.out.println();
		
		
		
		
		System.out.println("####################");
		System.out.println("Sets do not guarantee ordering");
		System.out.println("####################");
		System.out.println();
		
		System.out.println("Set of students contains:");
		for(String studentName : students) { // note that the values are returned in a different order than they were added
			System.out.println(studentName);
		}
		System.out.println();
		System.out.println("note that the values may be returned in a different order than they were added");
		System.out.println();


		
		
		
		

	}
	

//####################
//Big O Notation
//####################
	
	//Big O Notation: O(n^2)
	public boolean doesArrayContainDuplicates(int[] array) {
	    boolean duplicate = false;
	    for (int i = 0; i < array.length; i++){
	        for (int j = 0; j < array.length; j++) {
	            if (i == j){
	                continue;
	            }

	            if (array[i] == array[j]){
	                duplicate = true;
	            }
	        }
	    }

	    return duplicate;
	}
	

}
