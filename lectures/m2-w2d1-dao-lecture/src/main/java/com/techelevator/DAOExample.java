package com.techelevator;

import java.util.List;

import org.apache.commons.dbcp2.BasicDataSource;

import com.techelevator.city.City;
import com.techelevator.city.CityDAO;
import com.techelevator.city.JDBCCityDAO;

public class DAOExample {

	public static void main(String[] args) {

		//Create a Data Source to the World database
		BasicDataSource worldDataSource = new BasicDataSource();
		worldDataSource.setUrl("jdbc:postgresql://localhost:5432/world");
		worldDataSource.setUsername("postgres");
		worldDataSource.setPassword("postgres1");
		
		//Create the DAO
		CityDAO dao = new JDBCCityDAO(worldDataSource);
		
		//Create a city to add
		City smallville = new City();
		smallville.setCountryCode("USA");
		smallville.setDistrict("KS");
		smallville.setName("Smallville");
		smallville.setPopulation(428);
		
		//Insert the city into the database
		dao.save(smallville);
		
		//Get a city from the database
		City foundCity = dao.findCityById(smallville.getId());
		
		System.out.println("Found city with name: " + foundCity.getName());

		
		//Get all cities in the USA
		List<City> citiesInUsa = dao.findCityByCountryCode("USA");
		
		System.out.println("Number of cities in the USA: " + citiesInUsa.size());
		
		
		//Remove Smallville
		dao.delete(smallville.getId());

		//Check the count again
		citiesInUsa = dao.findCityByCountryCode("USA");
		System.out.println("Number of cities in the USA: " + citiesInUsa.size());

		
	}

}
