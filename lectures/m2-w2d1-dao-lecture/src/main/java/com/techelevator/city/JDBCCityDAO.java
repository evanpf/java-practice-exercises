package com.techelevator.city;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;

public class JDBCCityDAO implements CityDAO {

	private JdbcTemplate jdbcTemplate;	
	
	
//	public JDBCCityDAO(String dbUrl, String username, String password) {
//		BasicDataSource dataSource = new BasicDataSource();
//		dataSource.setUrl(dbUrl);
//		dataSource.setUsername(username);
//		dataSource.setPassword(password);
//		this.jdbcTemplate = new JdbcTemplate(dataSource);
//	}
	
	public JDBCCityDAO(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	@Override
	public void save(City newCity) {
		
		String sqlInsertCity = "INSERT INTO city(id, name, countrycode, district, population) " +
				   "VALUES(?, ?, ?, ?, ?)";

		newCity.setId(getNextCityId());
		jdbcTemplate.update(sqlInsertCity, newCity.getId(), 
										  newCity.getName(), 
										  newCity.getCountryCode(), 
										  newCity.getDistrict(), 
										  newCity.getPopulation());
		
	}

	private Long getNextCityId() {

		String sqlGetNextId = "SELECT nextval('seq_city_id')";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlGetNextId);
		results.next(); // advances to the first row
		return results.getLong(1); // returns the integer value of the first column (i.e. index 1)

	}

	@Override
	public City findCityById(long id) {

		City theCity = null;
		
		String sql = "SELECT id, name, countrycode, district, population FROM city WHERE id = ?;";
		
		SqlRowSet results = jdbcTemplate.queryForRowSet(sql, id);
		
		if(results.next()) {
			theCity = mapRowToCity(results);
		}
		
		return theCity;
	}

	@Override
	public List<City> findCityByCountryCode(String countryCode) {

		List<City> cities = new ArrayList<City>();
		
		String sqlFindCityByCountryCode = "SELECT id, name, countrycode, district, population "+
				   "FROM city "+
				   "WHERE countrycode = ?";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlFindCityByCountryCode, countryCode);

		while(results.next()) {
			City theCity = mapRowToCity(results);

			cities.add(theCity);
		}
		
		
		return cities;
		
		
	}

	@Override
	public List<City> findCityByDistrict(String district) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void update(City city) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(long id) {
		
		String sql = "DELETE FROM city WHERE id = ?";
		
		jdbcTemplate.update(sql, id);
		
		// TODO Auto-generated method stub
		
	}

	private City mapRowToCity(SqlRowSet results) {
		City theCity = new City();
		theCity.setId(results.getLong("id"));
		theCity.setName(results.getString("name"));
		theCity.setCountryCode(results.getString("countrycode"));
		theCity.setDistrict(results.getString("district"));
		theCity.setPopulation(results.getInt("population"));
		return theCity;
	}

	
	
	
}
