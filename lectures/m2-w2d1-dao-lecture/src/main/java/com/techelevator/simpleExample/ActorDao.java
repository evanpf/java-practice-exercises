package com.techelevator.simpleExample;

import java.util.List;

//TODO 3. Create the DAO interface file

public interface ActorDao {

	//TODO 6. Create method definitions for CRUD operations (Create, Read, Update, Delete)
	
	public void create(Actor actor);				//Create
	public List<Actor> findAll();					//Read
	public List<Actor> findByName(String name);	//Read
	public Actor findById(long id);				//Read
	public void save(Actor actor);				//Update
	public void deleteActor(long id);			//Delete
	
	
}
