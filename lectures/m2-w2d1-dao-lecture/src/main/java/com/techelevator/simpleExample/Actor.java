package com.techelevator.simpleExample;

//TODO 2. Create the POJO (Plain Ol' Java Object) class - This is what the DAO will populate

public class Actor {

	//TODO 5. Populate the Actor with properties
	
	private long id;
	private String firstName;
	private String lastName;
		
	
	public Actor(String firstName, String lastName) {
		this.firstName = firstName;
		this.lastName = lastName;
	}
	
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName.toUpperCase();
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName.toUpperCase();
	}
	
	
	@Override
	public String toString() {
		return this.lastName + " " + this.firstName + " (" + this.id + ")";
	}
	
	
	
}
