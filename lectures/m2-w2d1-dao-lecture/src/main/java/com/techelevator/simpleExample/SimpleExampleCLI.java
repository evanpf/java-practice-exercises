package com.techelevator.simpleExample;

import java.util.List;

import org.apache.commons.dbcp2.BasicDataSource;

public class SimpleExampleCLI {

	public static void main(String[] args) {
		
		//TODO 1. Create the CLI because that's where we're using everything
		
		//TODO 11. Create the BasicDataSource
		BasicDataSource dataSource = new BasicDataSource();
		dataSource.setUrl("jdbc:postgresql://localhost:5432/dvdstore");
		dataSource.setUsername("postgres");
		dataSource.setPassword("postgres1");
		
		//TODO 12. Instantiate a new DAO, passing it the data source
		ActorDao dao = new JdbcActorDao(dataSource);
		
		//TODO 13. Do stuff with the DAO
		
		List<Actor> results = dao.findByName("b");
		
		System.out.println("Actors containing: b");
		for(Actor a : results) {
			System.out.println(a);
		}
		
		
		
		Actor actor159 = dao.findById(159);
		System.out.println("\n\n\nFound actor 159: " + actor159);
		
		
		
		results = dao.findAll();
		
		System.out.println("\n\n\nAll Actors");
		for(Actor a : results) {
			System.out.println(a);
		}
				
		
		
	}
	
}
