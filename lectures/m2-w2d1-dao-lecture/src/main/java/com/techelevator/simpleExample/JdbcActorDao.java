package com.techelevator.simpleExample;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;

//TODO 4. Create the concrete class that will implement the DAO

public class JdbcActorDao implements ActorDao {

	//TODO 8. Add JdbcTemplate private variable (and the appropriate import)
	private JdbcTemplate template;

	
	//TODO 9. Create a constructor that takes a BasicDataSource (and add the import)
	public JdbcActorDao(BasicDataSource dataSource) {
		this.template = new JdbcTemplate(dataSource);
	}
		
	//TODO 7. Add the interface methods
	
	@Override
	public void create(Actor actor) {

		//TODO 10. Implement the methods in whatever order you want
		
		//TODO I1. Insert - Create the SQL needed
		String sql = "INSERT INTO Actor (actor_id, first_name, last_name) VALUES (?, ?, ?);";
		
		//TODO I2. Get the next ID from the actor table, and set it to the actor's ID
		actor.setId(getNextActorId());
		
		//TODO I3. Execute the insert statement.
		this.template.update(sql, actor.getId(), actor.getFirstName(), actor.getLastName());
		
	}

	private long getNextActorId() {
		long val = 0;
		
		String sql = "SELECT nextval('actor_actor_id_seq');";
		
		SqlRowSet result = this.template.queryForRowSet(sql);
		if(result.next()) {
			val = result.getLong(1);
		}		
		
		return val;
	}

	@Override
	public Actor findById(long id) {

		Actor newActor = null;		//Need to define the return variable here so it's in scope outside of the if statement.
		
		//TODO 10. Implement the methods in whatever order you want
		
		//TODO S1. Create the SQL needed
		String sql = "SELECT actor_id, first_name, last_name FROM Actor WHERE actor_id = ?";
		
		//TODO S2. Get the results from the database (pass in SQL and any paramaters to replace ?s); add import; set to variable
		SqlRowSet results = this.template.queryForRowSet(sql, id);

		//TODO S3. Since selecting by ID will always only return zero or one rows, we don't need to loop.
		if(results.next()) {		//Will check to see if a result exists, and if so, advance the result set pointer to the first row
			//TODO S4. Create the new object to place data into, and put the data in it.
			String firstName = results.getString("first_name");
			String lastName = results.getString("last_name");
			long returnedId = results.getLong("actor_id");
			
			newActor = new Actor(firstName, lastName);	//Create a new Actor object and set the DB values to it
			newActor.setId(returnedId);					//Set the ID of the actor, since my constructor doesn't allow an ID to be passed
			
		}
		
		return newActor;
		
	}

	@Override
	public void save(Actor actor) {

		//TODO 10. Implement the methods in whatever order you want

		//TODO U1. UPDATE - Create SQL
		String sql = "UPDATE Actor SET first_name = ?, last_name = ? WHERE actor_id = ?;";
		
		//TODO U2. Execute the SQL
		this.template.update(sql, actor.getFirstName(), actor.getLastName(), actor.getId());
				
	}

	@Override
	public void deleteActor(long id) {

		//TODO 10. Implement the methods in whatever order you want
		
		//TODO D1. DELETE - Create SQL
		String sql = "DELETE FROM actor WHERE actor_id = ?;";
		
		//TODO D2. Execute SQL
		this.template.update(sql, id);
		
	}

	@Override
	public List<Actor> findByName(String name) {
		
		//TODO 10. Implement the methods in whatever order you want
		
		
		//TODO L0. Create the variable that will hold the actors we're returning
		List<Actor> actorList = new ArrayList<Actor>();
		
		//TODO L1. SELECT List - Create SQL
		String sql = "SELECT actor_id, first_name, last_name FROM Actor WHERE " + 
					 "first_name ILIKE ? OR last_name ILIKE ? ";
		
		//TODO L2. Prepare the inputs -- We need to add the percents so the like works (OPTIONAL - Not needed if doing straight comparisons instead of LIKE)
		name = "%" + name + "%";		//May want % on only one side for some queries
		
		//TODO L3. Get the results
		SqlRowSet results = this.template.queryForRowSet(sql, name, name);
		
		//TODO L4. Loop through the results
		while(results.next()) {
			
			//TODO L5. Get data from the current row 
			String firstName = results.getString("first_name");
			String lastName = results.getString("last_name");
			long actorId = results.getLong("actor_id");

			//TODO L6. Create a new Actor object with the row's data
			Actor act = new Actor(firstName, lastName);
			act.setId(actorId);
			
			//TODO L7. Add the actor to the list that we're returning
			actorList.add(act);
			
		}
		
		//TODO L8. Return the now-populated actor list (or an empty list if there were no results)
		return actorList;
	
	
	}

	@Override
	public List<Actor> findAll() {
		//TODO 10. Implement the methods in whatever order you want
		
		
		//TODO A0. Create the variable that will hold the actors we're returning
		List<Actor> actorList = new ArrayList<Actor>();
		
		//TODO A1. SELECT List - Create SQL
		String sql = "SELECT actor_id, first_name, last_name FROM Actor";
		
		//TODO A3. Get the results
		SqlRowSet results = this.template.queryForRowSet(sql);
		
		//TODO A4. Loop through the results
		while(results.next()) {
			
			//TODO A5. Get data from the current row 
			String firstName = results.getString("first_name");
			String lastName = results.getString("last_name");
			long actorId = results.getLong("actor_id");

			//TODO A6. Create a new Actor object with the row's data
			Actor act = new Actor(firstName, lastName);
			act.setId(actorId);
			
			//TODO A7. Add the actor to the list that we're returning
			actorList.add(act);
			
		}
		
		//TODO A8. Return the now-populated actor list (or an empty list if there were no results)
		return actorList;
	

	}

	
	
}
