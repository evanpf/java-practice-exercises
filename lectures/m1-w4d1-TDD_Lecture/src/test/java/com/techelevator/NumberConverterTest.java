package com.techelevator;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class NumberConverterTest {

	@Test
	public void test1() {
		String result = NumberConverter.convertToRomanNumerals(1);
		assertEquals("1 == I", "I", result);
	}
	
	@Test
	public void test2to3() {
		
		String result = NumberConverter.convertToRomanNumerals(2);
		assertEquals("2 == II", "II", result);
		
		result = NumberConverter.convertToRomanNumerals(3);
		assertEquals("3 == III", "III", result);
		
	}

	@Test
	public void test4() {
		String result = NumberConverter.convertToRomanNumerals(4);
		assertEquals("4 == IV", "IV", result);				
	}
	
	@Test
	public void test5() {
		String result = NumberConverter.convertToRomanNumerals(5);
		assertEquals("5 == V", "V", result);				
		
	}
	
	@Test
	public void test6to8() {
		String result = NumberConverter.convertToRomanNumerals(6);
		assertEquals("6 == VI", "VI", result);
		
		result = NumberConverter.convertToRomanNumerals(7);
		assertEquals("7 == VII", "VII", result);
		
		result = NumberConverter.convertToRomanNumerals(8);
		assertEquals("8 == VIII", "VIII", result);
	}

	@Test
	public void test9() {
		String result = NumberConverter.convertToRomanNumerals(9);
		assertEquals("9 == IX", "IX", result);
	}

	@Test
	public void test10() {
		String result = NumberConverter.convertToRomanNumerals(10);
		assertEquals("10 == X", "X", result);
	}
	
	@Test
	public void test11to13() {
		String result = NumberConverter.convertToRomanNumerals(11);
		assertEquals("11 == XI", "XI", result);
		
		result = NumberConverter.convertToRomanNumerals(12);
		assertEquals("12 == XII", "XII", result);
		
		result = NumberConverter.convertToRomanNumerals(13);
		assertEquals("13 == XIII", "XIII", result);
	}
	
	@Test
	public void test11to14() {
		String result = NumberConverter.convertToRomanNumerals(14);
		assertEquals("14 == XIV", "XIV", result);
	}

	@Test
	public void test15to18() {
		String result = NumberConverter.convertToRomanNumerals(15);
		assertEquals("15 == XV", "XV", result);

		result = NumberConverter.convertToRomanNumerals(16);
		assertEquals("16 == XVI", "XVI", result);
		
		result = NumberConverter.convertToRomanNumerals(17);
		assertEquals("17 == XVII", "XVII", result);

		result = NumberConverter.convertToRomanNumerals(18);
		assertEquals("18 == XVIII", "XVIII", result);
	}
	
	@Test
	public void test19() {
		String result = NumberConverter.convertToRomanNumerals(19);
		assertEquals("19 == XIX", "XIX", result);
	}
	
	@Test
	public void test20to60() {
		
		Map<Integer, String> testMap = new HashMap<Integer, String>();
		testMap.put(20, "XX");
		testMap.put(21, "XXI");
		testMap.put(22, "XXII");
		testMap.put(23, "XXIII");
		testMap.put(24, "XXIV");
		testMap.put(25, "XXV");
		testMap.put(26, "XXVI");
		testMap.put(27, "XXVII");
		testMap.put(28, "XXVIII");
		testMap.put(29, "XXIX");
		testMap.put(30, "XXX");
		testMap.put(31, "XXXI");
		testMap.put(32, "XXXII");
		testMap.put(33, "XXXIII");
		testMap.put(34, "XXXIV");
		testMap.put(35, "XXXV");
		testMap.put(36, "XXXVI");
		testMap.put(37, "XXXVII");
		testMap.put(38, "XXXVIII");
		testMap.put(39, "XXXIX");

		testMap.put(40,	"XL");
		testMap.put(41, "XLI");
		testMap.put(42, "XLII");
		testMap.put(43, "XLIII");
		testMap.put(44, "XLIV");
		testMap.put(45, "XLV");
		testMap.put(46, "XLVI");
		testMap.put(47, "XLVII");
		testMap.put(48, "XLVIII");
		testMap.put(49, "XLIX");
		testMap.put(50, "L");    
		testMap.put(51, "LI");   
		testMap.put(52, "LII");  
		testMap.put(53, "LIII"); 
		testMap.put(54, "LIV");  
		testMap.put(55, "LV");   
		testMap.put(56, "LVI");  
		testMap.put(57, "LVII"); 
		testMap.put(58, "LVIII");
		testMap.put(59, "LIX");  
		testMap.put(60, "LX");

		for(Integer key : testMap.keySet()) {
			String result = NumberConverter.convertToRomanNumerals(key);
			assertEquals(key + " == " + testMap.get(key), testMap.get(key), result);
	
		}

	}
	
	@Test
	public void test88to110() {
		
		Map<Integer, String> testMap = new HashMap<Integer, String>();
		testMap.put(88, "LXXXVIII");
		testMap.put(89, "LXXXIX");
		
		testMap.put(90, "XC");
		testMap.put(91, "XCI");    
		testMap.put(92, "XCII");   
		testMap.put(93, "XCIII");  
		testMap.put(94, "XCIV");   
		testMap.put(95, "XCV");    
		testMap.put(96, "XCVI");   
		testMap.put(97, "XCVII");  
		testMap.put(98, "XCVIII"); 
		testMap.put(99, "XCIX");   
		testMap.put(100, "C");   
		testMap.put(101, "CI");  
		testMap.put(102, "CII"); 
		testMap.put(103, "CIII");
		testMap.put(104, "CIV"); 
		testMap.put(105, "CV");  
		testMap.put(106, "CVI"); 
		testMap.put(107, "CVII");
		testMap.put(108, "CVIII");
		testMap.put(109, "CIX"); 

		testMap.put(110,	"CX");

		for(Integer key : testMap.keySet()) {
			String result = NumberConverter.convertToRomanNumerals(key);
			assertEquals(key + " == " + testMap.get(key), testMap.get(key), result);
	
		}

	}

	@Test
	public void TestEdgeCases() {
		

		Map<Integer, String> testMap = new HashMap<Integer, String>();
		testMap.put(3999, "MMMCMXCIX");
		testMap.put(2999, "MMCMXCIX");
		testMap.put(1999, "MCMXCIX");

		for(Integer key : testMap.keySet()) {
			String result = NumberConverter.convertToRomanNumerals(key);
			assertEquals(key + " == " + testMap.get(key), testMap.get(key), result);
	
		}
		
	}
	
	
	
}
