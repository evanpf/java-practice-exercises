package com.techelevator;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class KataExerciseTest {

	@Test
	public void testAddingStartEndSameOdd() {
		
		int result = KataExercise.SumOddsBetweenValues(11, 11);
		assertEquals("(11, 11) -> 11", 11, result);
	}
	
	@Test
	public void testAddingStartEndSameEven() {
		
		int result = KataExercise.SumOddsBetweenValues(10, 10);
		assertEquals("(10, 10) -> 0", 0, result);
		
	}
	
	@Test
	public void testSumStartOddEndEven() {
		
		int[][] tests = {
				{11, 12, 11},
				{11, 13, 24},
				{9,  26, 153}
		};

		for(int[] values : tests) {
			int result = KataExercise.SumOddsBetweenValues(values[0], values[1]);
			assertEquals("(" + values[0] + ", " + values[1] + ") -> " + values[2], values[2], result);
	
		}
	}
	
	

}
