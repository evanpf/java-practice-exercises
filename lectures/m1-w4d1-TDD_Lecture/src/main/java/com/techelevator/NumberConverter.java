package com.techelevator;

import java.util.HashMap;
import java.util.Map;

public class NumberConverter {

	public static String convertToRomanNumerals(int num) {

		//TODO: Check range 0-3999;
		
		final String ONE = "I";
		final String FIVE = "V";
		final String TEN = "X";
		final String FIFTY = "L";
		final String ONEHUNDRED = "C";
		final String FIVEHUNDRED = "D";
		final String ONETHOUSAND = "M";

		Map<Integer, String> numerals = new HashMap<Integer, String>();
		
		numerals.put(1, "I");
		numerals.put(5, "V");
		numerals.put(10, "X");
		numerals.put(50, "L");
		numerals.put(100, "C");
		numerals.put(500, "D");
		numerals.put(1000, "M");
		numerals.put(5000, "M");		//Won't be used because our range is to 3999
		numerals.put(10000, "M");	//Won't be used because our range is to 3999
		
		String result = "";


		int placeNumber; 

		for(int i = 1000; i >= 1; i /= 10) {

			placeNumber = num / i;
			
			result += getRomanNumeralsForPlacing(placeNumber, numerals.get(i * 5), numerals.get(i), numerals.get(i * 10));

			num -= placeNumber * i;

		}
				
		return result;
	}

	private static String getRomanNumeralsForPlacing(int num, String halfLetter, String fullLetter, String nextFullLetter) {
		String result = "";
		
		if(num == 4) {
			result += fullLetter;
		}
		
		if(num >= 4 && num < 9) {
			result += halfLetter;
		} 
		
		int extraOnes = num % 5;
		if(extraOnes == 4) extraOnes = 0;
		
		for (int i = 0; i < extraOnes; i++) {
			result += fullLetter;
		}

		if(num == 9) {
			result += fullLetter + nextFullLetter;
		}
		
		return result;
		
		
	}
	
	
}
