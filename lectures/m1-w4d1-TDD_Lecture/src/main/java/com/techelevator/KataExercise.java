package com.techelevator;

public class KataExercise {

	public static int SumOddsBetweenValues(int start, int end) {


			int result = 0;
			
			for(int i = start; i <= end; i++) {
				if(i % 2 == 1) {
					result += i;
				}
			}
			
			return result;
			

	}

}
