package com.techelevator.scanner;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class RTNValidator {

	private static final int[] CHECKSUM_WEIGHTS = new int[] { 3, 7, 1, 3, 7, 1, 3, 7, 1 };

	public static void main(String[] args) throws FileNotFoundException {

		printApplicationBanner();

		File inputFile = getInputFileFromUser();

		//Open the file for reading
		try (Scanner fileScanner = new Scanner(inputFile)) {

			//While the file still has more lines to read...
			while (fileScanner.hasNextLine()) {
				//...Get the next line
				String line = fileScanner.nextLine();
				
				
				//Do something with the line
				String rtn = line.substring(0, 9);

				if (!checksumIsValid(rtn)) {
					System.out.println(rtn + " is not a valid rtn. ");
				}

			}

		} catch (Exception ex) {
			System.out.println("An exception occurred!");
		}

		System.out.println();
		System.out.println();
		System.out.println("Execution complete.");

	}

	private static File getInputFileFromUser() {
		File inputFile = null;

		Scanner userInput = new Scanner(System.in);
		
		boolean done = false;
		while (!done) {
			done = true;
			System.out.print("Enter the path to the input file: ");
			String path = userInput.nextLine();

			inputFile = new File(path);

			if (inputFile.exists() == false) {
				System.out.println(path + " does not exist!");
				done = false;
			} else if (!inputFile.isFile()) {
				System.out.println(path + " is not a file!");
				done = false;
			}
		}
		
		return inputFile;

	}

	private static void printApplicationBanner() {
		System.out.println("******************");
		System.out.println("RTN Validator 9000");
		System.out.println("******************");
		System.out.println();
	}

	private static boolean checksumIsValid(String routingNumber) {
		int checksum = 0;
		for (int i = 0; i < 9; i++) {
			int digit = Integer.parseInt(routingNumber.substring(i, i + 1));
			checksum += digit * CHECKSUM_WEIGHTS[i];
		}
		return checksum % 10 == 0;
	}

}
